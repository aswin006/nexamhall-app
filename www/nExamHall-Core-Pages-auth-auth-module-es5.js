(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["nExamHall-Core-Pages-auth-auth-module"], {
    /***/
    "B5mF":
    /*!**********************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/auth/auth.module.ts ***!
      \**********************************************************/

    /*! exports provided: AuthPageModule */

    /***/
    function B5mF(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPageModule", function () {
        return AuthPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _auth_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./auth-routing.module */
      "zM4B");
      /* harmony import */


      var _auth_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./auth.page */
      "hqyE");

      var AuthPageModule = function AuthPageModule() {
        _classCallCheck(this, AuthPageModule);
      };

      AuthPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_5__["AuthPageRoutingModule"]],
        declarations: [_auth_page__WEBPACK_IMPORTED_MODULE_6__["AuthPage"]]
      })], AuthPageModule);
      /***/
    },

    /***/
    "U+1O":
    /*!************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Core-Pages/auth/auth.page.html ***!
      \************************************************************************************************/

    /*! exports provided: default */

    /***/
    function U1O(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n \n\n  <!--<div class=\"about-header\">\n    <div class=\"about-image \" ></div>\n  </div>\n\n  <div class=\"about-info\">\n    <h5 class=\"ion-padding-top ion-padding-start\">Enter your institute code</h5>\n    <div class=\"container\">\n      <form class=\"form-signin form-auth-card\" name=\"institutecodeform\"   role=\"form\" novalidate=\"\">\n        <div class=\"form-group\">\n          <input type=\"text\" class=\"form-control ng-pristine ng-valid ng-empty ng-touched\" name=\"institutename\" id=\"institute\" placeholder=\"Institute Code\" ng-model=\"contactdata.institutename\">\n        </div>\n        <div class=\"form-group row justify-content-center\">\n          <div class=\"col-md-12\">\n            <button class=\"btn btn-md btn-blue-gradient btn-block text-capitalize mb-2\" id=\"loginBtn\" type=\"button\" ng-disabled=\"institutecodeform.$invalid\">Next</button>\n          </div>\n         \n        </div>\n      </form>\n    </div>\n   \n\n  </div>-->\n  <div class=\"container d-flex justify-content-center\">\n    <div class=\"d-flex flex-column justify-content-between\">\n        <div class=\"card  mt-0 p-2\">\n            \n            <!-- <div>\n                <p class=\"mb-1\">Explore and Start Preparing </p>\n                <h4 class=\"mb-5 text-white\">Exam with us!</h4>\n            </div>  -->\n            <div class=\"about-header\">\n             <img class=\"mb-5\" src=\"./assets/images/about/about10.png\" >\n            </div>\n        </div>\n        <div class=\"card two bg-white p-4 mb-3\">\n          <form class=\"form-signin form-auth-card mt-5\" name=\"institutecodeform\"   role=\"form\" novalidate=\"\">\n            \n            <div class=\"form-group col-lg-6 mb-4\">\n              <label for=\"institutecode\">Institute Code</label>\n             \n              <input type=\"text\" class=\"form-control\"  \n              [(ngModel)]=\"institutecode\" name=\"institutecode\" id=\"institutecode\" placeholder=\"Enter institute code\" required>\n            </div>\n            <div class=\"form-group col-lg-12 mx-auto mb-0\">\n              <button\n                class=\"btn btn-primary btn-block py-2 text-white\"\n                (click)=\"checkInstCode()\"\n              >\n                <span class=\"font-weight-normal \">Login</span>\n              </button>\n            </div>\n          </form>\n           \n        </div>\n    </div>\n</div>\n\n\n</ion-content>\n";
      /***/
    },

    /***/
    "d6yy":
    /*!**********************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/auth/auth.page.scss ***!
      \**********************************************************/

    /*! exports provided: default */

    /***/
    function d6yy(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".btn-blue-gradient {\n  color: #fff;\n  background-color: #349cff;\n  background-image: linear-gradient(315deg, #045de9 0, #349cff 74%);\n  box-shadow: -1px 1px 4px 0 rgba(117, 138, 172, 0.12);\n  border: none;\n  transition: all 0.3s ease-in-out;\n}\n\n.container {\n  flex-wrap: wrap;\n}\n\n.card {\n  border: none;\n  border-radius: 0px;\n  background-color: #4270C8;\n  width: 100vw;\n  margin-top: -60px;\n}\n\n.about-header img {\n  background-position: top center;\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\np.mb-1 {\n  font-size: 20px;\n  color: #9FB7FD;\n}\n\n.card.two {\n  border-top-right-radius: 60px;\n  border-top-left-radius: 0;\n}\n\n.form-control:not(select) {\n  padding: 1.5rem 0.5rem;\n}\n\n.form-control::-moz-placeholder {\n  color: #ccc;\n  font-weight: bold;\n  font-size: 0.9rem;\n}\n\n.form-control::placeholder {\n  color: #ccc;\n  font-weight: bold;\n  font-size: 0.9rem;\n}\n\n.form-control:focus {\n  box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNvcmUtUGFnZXMvYXV0aC9hdXRoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUF1RkU7RUFDRSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxpRUFBQTtFQUVBLG9EQUFBO0VBQ0EsWUFBQTtFQUdBLGdDQUFBO0FBdEZKOztBQXlGQTtFQUNFLGVBQUE7QUF0RkY7O0FBeUZBO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUF0RkY7O0FBZ0dBO0VBRUUsK0JBQUE7RUFDQSxzQkFBQTtFQUNBLDRCQUFBO0FBOUZGOztBQWlHQTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FBOUZGOztBQW1HQTtFQUNFLDZCQUFBO0VBQ0EseUJBQUE7QUFoR0Y7O0FBc0hBO0VBQ0Usc0JBQUE7QUFuSEY7O0FBcUhBO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUFsSEY7O0FBK0dBO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUFsSEY7O0FBcUhBO0VBQ0UsZ0JBQUE7QUFsSEYiLCJmaWxlIjoic3JjL2FwcC9uRXhhbUhhbGwtQ29yZS1QYWdlcy9hdXRoL2F1dGgucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLXRvb2xiYXIge1xyXG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIFxyXG4vLyAgICAgdG9wOiAwO1xyXG4vLyAgICAgbGVmdDogMDtcclxuLy8gICAgIHJpZ2h0OiAwO1xyXG4gIFxyXG4vLyAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuLy8gICAgIC0tY29sb3I6IHdoaXRlO1xyXG4vLyAgIH1cclxuICBcclxuLy8gICBpb24tdG9vbGJhciBpb24tYnV0dG9uLFxyXG4vLyAgIGlvbi10b29sYmFyIGlvbi1iYWNrLWJ1dHRvbixcclxuLy8gICBpb24tdG9vbGJhciBpb24tbWVudS1idXR0b24ge1xyXG4vLyAgICAgLS1jb2xvcjogd2hpdGU7XHJcbi8vICAgfVxyXG4gIFxyXG4vLyAgIC5hYm91dC1oZWFkZXIge1xyXG4vLyAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIFxyXG4vLyAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICBoZWlnaHQ6IDMwJTtcclxuLy8gICB9XHJcbiAgXHJcbiAgLy8gLmFib3V0LWhlYWRlciAuYWJvdXQtaW1hZ2Uge1xyXG4gIC8vICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIFxyXG4gIC8vICAgdG9wOiAwO1xyXG4gIC8vICAgbGVmdDogMDtcclxuICAvLyAgIGJvdHRvbTogMDtcclxuICAvLyAgIHJpZ2h0OiAwO1xyXG4gIFxyXG4gIC8vICAgYmFja2dyb3VuZC1wb3NpdGlvbjogdG9wIGNlbnRlcjtcclxuICAvLyAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgLy8gICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIFxyXG4gIC8vICAgb3BhY2l0eTogMDtcclxuICBcclxuICAvLyAgIHRyYW5zaXRpb246IG9wYWNpdHkgNTAwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgLy8gfVxyXG4gIFxyXG4gIC8vIC5hYm91dC1oZWFkZXIge1xyXG4gIC8vICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0NS5wbmcpO1xyXG4gIC8vICAgYmFja2dyb3VuZC1wb3NpdGlvbjp0b3AgY2VudGVyO1xyXG4gIC8vICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAvLyAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgLy8gfVxyXG4gIFxyXG4gIFxyXG4vLyAgIC5hYm91dC1pbmZvIHtcclxuLy8gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuLy8gICAgIG1hcmdpbi10b3A6IC0xMHB4O1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuLy8gICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCAjZmZmKTtcclxuLy8gICAgIHdpZHRoOiAxMDB2dztcclxuLy8gICB9XHJcbiAgXHJcbi8vICAgLmFib3V0LWluZm8gaDMge1xyXG4vLyAgICAgbWFyZ2luLXRvcDogMDtcclxuLy8gICB9XHJcbiAgXHJcbi8vICAgLmFib3V0LWluZm8gaW9uLWxpc3Qge1xyXG4vLyAgICAgcGFkZGluZy10b3A6IDA7XHJcbi8vICAgfVxyXG4gIFxyXG4vLyAgIC5hYm91dC1pbmZvIHAge1xyXG4vLyAgICAgbGluZS1oZWlnaHQ6IDEzMCU7XHJcbiAgXHJcbi8vICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xyXG4vLyAgIH1cclxuICBcclxuLy8gICAuYWJvdXQtaW5mbyBpb24taWNvbiB7XHJcbi8vICAgICBtYXJnaW4taW5saW5lLWVuZDogMzJweDtcclxuLy8gICB9XHJcbiAgXHJcbi8vICAgLypcclxuLy8gICAgKiBpT1MgT25seVxyXG4vLyAgICAqL1xyXG4gIFxyXG4vLyAgIC5pb3MgLmFib3V0LWluZm8ge1xyXG4vLyAgICAgLS1pb24tcGFkZGluZzogMTlweDtcclxuLy8gICB9XHJcbiAgXHJcbi8vICAgLmlvcyAuYWJvdXQtaW5mbyBoMyB7XHJcbi8vICAgICBmb250LXdlaWdodDogNzAwO1xyXG4vLyAgIH1cclxuICBcclxuICAuYnRuLWJsdWUtZ3JhZGllbnQge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzQ5Y2ZmO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDMxNWRlZywjMDQ1ZGU5IDAsIzM0OWNmZiA3NCUpO1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAtMXB4IDFweCA0cHggMCByZ2JhKDExNywxMzgsMTcyLC4xMik7XHJcbiAgICBib3gtc2hhZG93OiAtMXB4IDFweCA0cHggMCByZ2JhKDExNywxMzgsMTcyLC4xMik7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuM3MgZWFzZS1pbi1vdXQ7XHJcbiAgICAtby10cmFuc2l0aW9uOiBhbGwgLjNzIGVhc2UtaW4tb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC4zcyBlYXNlLWluLW91dDtcclxufVxyXG5cclxuLmNvbnRhaW5lciB7XHJcbiAgZmxleC13cmFwOiB3cmFwXHJcbn1cclxuXHJcbi5jYXJkIHtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM0MjcwQzg7XHJcbiAgd2lkdGg6IDEwMHZ3O1xyXG4gIG1hcmdpbi10b3A6IC02MHB4O1xyXG4gXHJcbn1cclxuLy8gIC5oZWFkZXJ7XHJcbi8vICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoL2Fzc2V0cy9pbWFnZXMvYWJvdXQvYWJvdXQ1LnBuZyk7XHJcbi8vICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOnRvcCBjZW50ZXI7XHJcbi8vICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4vLyAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgXHJcbi8vIH1cclxuLmFib3V0LWhlYWRlciBpbWd7XHJcbiAgLy8gYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvaW1hZ2VzL2Fib3V0L2Fib3V0NS5wbmcpO1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246dG9wIGNlbnRlcjtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuXHJcbnAubWItMSB7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gIGNvbG9yOiAjOUZCN0ZEXHJcbn1cclxuXHJcblxyXG5cclxuLmNhcmQudHdvIHtcclxuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNjBweDtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwXHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG4vLyAuYnRuLWJsb2NrIHtcclxuLy8gICBib3JkZXI6IG5vbmU7XHJcbi8vICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4vLyAgIGJhY2tncm91bmQtY29sb3I6IzQyNzBDODtcclxuLy8gICBwYWRkaW5nOiAxMHB4IDAgMTJweFxyXG4vLyB9XHJcblxyXG4vLyAuYnRuLWJsb2NrOmZvY3VzIHtcclxuLy8gICBib3gtc2hhZG93OiBub25lXHJcbi8vIH1cclxuXHJcbi8vIC5idG4tYmxvY2sgIHtcclxuLy8gICBmb250LXNpemU6IDE1cHg7XHJcbi8vICAgY29sb3I6ICNEMEU2RkZcclxuLy8gfVxyXG4uZm9ybS1jb250cm9sOm5vdChzZWxlY3QpIHtcclxuICBwYWRkaW5nOiAxLjVyZW0gMC41cmVtO1xyXG59XHJcbi5mb3JtLWNvbnRyb2w6OnBsYWNlaG9sZGVyIHtcclxuICBjb2xvcjogI2NjYztcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDAuOXJlbTtcclxufVxyXG5cclxuLmZvcm0tY29udHJvbDpmb2N1cyB7XHJcbiAgYm94LXNoYWRvdzogbm9uZTtcclxufVxyXG5cclxuXHJcbiJdfQ== */";
      /***/
    },

    /***/
    "hqyE":
    /*!********************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/auth/auth.page.ts ***!
      \********************************************************/

    /*! exports provided: AuthPage */

    /***/
    function hqyE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPage", function () {
        return AuthPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_auth_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./auth.page.html */
      "U+1O");
      /* harmony import */


      var _auth_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth.page.scss */
      "d6yy");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../nExamHall-Services/api.service */
      "xHtS");
      /* harmony import */


      var _nExamHall_Services_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../nExamHall-Services/toast.service */
      "RWSw");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../nExamHall-Services/local-storage.service */
      "2eCX");

      var AuthPage = /*#__PURE__*/function () {
        function AuthPage(apiService, toast, router, localStorage) {
          _classCallCheck(this, AuthPage);

          this.apiService = apiService;
          this.toast = toast;
          this.router = router;
          this.localStorage = localStorage;
        }

        _createClass(AuthPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "checkInstCode",
          value: function checkInstCode() {
            var _this = this;

            var subdomain = {
              "subdomain": this.institutecode
            };
            this.apiService.checkCredentials(subdomain).subscribe(function (res) {
              console.log(res['userdata']);
              var userdata = res['userdata'];

              if (userdata != null) {
                _this.localStorage.setObject('instituteData', userdata).then(function (res) {
                  _this.router.navigateByUrl("/nexamhallcore/auth/login", {
                    replaceUrl: true
                  });
                });
              } else {
                var errormsg = "Please enter valid institutecode";

                _this.toast.showToast(errormsg, 2000, 'top', 'dark');
              }
            });
          }
        }]);

        return AuthPage;
      }();

      AuthPage.ctorParameters = function () {
        return [{
          type: _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]
        }, {
          type: _nExamHall_Services_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }, {
          type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"]
        }];
      };

      AuthPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-auth',
        template: _raw_loader_auth_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_auth_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], AuthPage);
      /***/
    },

    /***/
    "zM4B":
    /*!******************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/auth/auth-routing.module.ts ***!
      \******************************************************************/

    /*! exports provided: AuthPageRoutingModule */

    /***/
    function zM4B(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthPageRoutingModule", function () {
        return AuthPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _auth_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth.page */
      "hqyE"); // const routes: Routes = [
      //   {
      //     path: 'auth',
      //     component: AuthPage,
      //     children: [
      //       {
      //         path: "instituteauth",
      //         children: [
      //           {
      //             path: "",
      //             loadChildren: () =>
      //               import("../login/login.module").then((m) => m.LoginPageModule),
      //           },
      //         ],
      //       },
      //       {
      //         path: "login",
      //         children: [
      //           {
      //             path: "",
      //             loadChildren: () =>
      //               import("../login/login.module").then((m) => m.LoginPageModule),
      //           },
      //         ],
      //       },
      //       {
      //         path: "signup",
      //         children: [
      //           {
      //             path: "",
      //             loadChildren: () =>
      //               import("../signup/signup.module").then((m) => m.SignupPageModule),
      //           },
      //         ],
      //       },
      //       {
      //         path: "forgotpassword",
      //         children: [
      //           {
      //             path: "",
      //             loadChildren: () =>
      //               import("../forgotpassword/forgotpassword.module").then((m) => m.ForgotpasswordPageModule),
      //           },
      //         ],
      //       },
      //       {
      //         path: "resetpassword",
      //         children: [
      //           {
      //             path: "",
      //             loadChildren: () =>
      //               import("../resetpassword/resetpassword.module").then((m) => m.ResetpasswordPageModule),
      //           },
      //         ],
      //       },
      //       {
      //         path: "otp",
      //         children: [
      //           {
      //             path: "",
      //             loadChildren: () =>
      //               import("../otpscreen/otpscreen.module").then((m) => m.OtpscreenPageModule),
      //           },
      //         ],
      //       },
      //       {
      //         path: "",
      //         redirectTo: "/auth",
      //         pathMatch: "full",
      //       },
      //    ]
      //   }
      // ];


      var routes = [{
        path: 'auth',
        children: [{
          path: '',
          component: _auth_page__WEBPACK_IMPORTED_MODULE_3__["AuthPage"]
        }, {
          path: 'login',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | login-login-module */
            "login-login-module").then(__webpack_require__.bind(null,
            /*! ../login/login.module */
            "zoVK")).then(function (m) {
              return m.LoginPageModule;
            });
          }
        }, {
          path: 'signup',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | signup-signup-module */
            "signup-signup-module").then(__webpack_require__.bind(null,
            /*! ../signup/signup.module */
            "hxpP")).then(function (m) {
              return m.SignupPageModule;
            });
          }
        }, {
          path: 'forgot',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | forgotpassword-forgotpassword-module */
            "forgotpassword-forgotpassword-module").then(__webpack_require__.bind(null,
            /*! ../forgotpassword/forgotpassword.module */
            "XxbN")).then(function (m) {
              return m.ForgotpasswordPageModule;
            });
          }
        }, {
          path: 'resetpassword',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | resetpassword-resetpassword-module */
            "resetpassword-resetpassword-module").then(__webpack_require__.bind(null,
            /*! ../resetpassword/resetpassword.module */
            "YwZx")).then(function (m) {
              return m.ResetpasswordPageModule;
            });
          }
        }, {
          path: 'otp',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | otpscreen-otpscreen-module */
            "otpscreen-otpscreen-module").then(__webpack_require__.bind(null,
            /*! ../otpscreen/otpscreen.module */
            "3QgR")).then(function (m) {
              return m.OtpscreenPageModule;
            });
          }
        }]
      }];

      var AuthPageRoutingModule = function AuthPageRoutingModule() {
        _classCallCheck(this, AuthPageRoutingModule);
      };

      AuthPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AuthPageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=nExamHall-Core-Pages-auth-auth-module-es5.js.map