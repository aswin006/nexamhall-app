(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["examinstruction-examinstruction-module"], {
    /***/
    "5Rea":
    /*!**************************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examinstruction/examinstruction.page.scss ***!
      \**************************************************************************************/

    /*! exports provided: default */

    /***/
    function Rea(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".inst1Container {\n  position: relative;\n  height: 62vh;\n  overflow-y: auto;\n}\n\n.headerButton ion-select {\n  font-size: 0.8rem;\n}\n\n.main {\n  padding: 56px 0 0 0;\n}\n\n::ng-deep .swiper-pagination {\n  position: relative;\n  text-align: left;\n  transition: 300ms opacity;\n  transform: translate3d(0, 0, 0);\n  z-index: 10;\n}\n\n::ng-deep .swiper-pagination-bullet {\n  width: 27px;\n  height: 5px;\n  display: inline-block;\n  border-radius: 4px;\n}\n\n::ng-deep ion-slide li {\n  font-size: 0.8rem;\n}\n\n.declaration-box {\n  padding: 0.6rem;\n  height: 16vh;\n  position: relative;\n  overflow-y: auto;\n  font-size: 0.8rem;\n}\n\nion-checkbox {\n  --border-radius: 5px;\n  height: 15px;\n  width: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvZXhhbWluc3RydWN0aW9uL2V4YW1pbnN0cnVjdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUNBO0VBQ0ksaUJBQUE7QUFFSjs7QUFBQTtFQUNJLG1CQUFBO0FBR0o7O0FBRUk7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBR0EseUJBQUE7RUFFQSwrQkFBQTtFQUNBLFdBQUE7QUFDUjs7QUFFSTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtBQUFSOztBQUlRO0VBQ0ksaUJBQUE7QUFGWjs7QUFPQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBSko7O0FBT0E7RUFDSSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBSkoiLCJmaWxlIjoic3JjL2FwcC9uRXhhbUhhbGwtQ2FuZGlkYXRlLU1vZHVsZS9leGFtaW5zdHJ1Y3Rpb24vZXhhbWluc3RydWN0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbnN0MUNvbnRhaW5lciB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDYydmg7XHJcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xyXG59XHJcbi5oZWFkZXJCdXR0b24gaW9uLXNlbGVjdHtcclxuICAgIGZvbnQtc2l6ZTogMC44cmVtO1xyXG59XHJcbi5tYWlue1xyXG4gICAgcGFkZGluZzo1NnB4IDAgMCAwIDtcclxufVxyXG5cclxuXHJcbjo6bmctZGVlcCB7XHJcbiAgICAuc3dpcGVyLXBhZ2luYXRpb24ge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogMzAwbXMgb3BhY2l0eTtcclxuICAgICAgICAtby10cmFuc2l0aW9uOiAzMDBtcyBvcGFjaXR5O1xyXG4gICAgICAgIHRyYW5zaXRpb246IDMwMG1zIG9wYWNpdHk7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIDAsIDApO1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XHJcbiAgICAgICAgei1pbmRleDogMTA7XHJcbiAgICB9XHJcblxyXG4gICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XHJcbiAgICAgICAgd2lkdGg6IDI3cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1cHg7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuXHJcbiAgICB9XHJcbiAgICBpb24tc2xpZGV7XHJcbiAgICAgICAgbGl7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMC44cmVtO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLmRlY2xhcmF0aW9uLWJveCB7XHJcbiAgICBwYWRkaW5nOiAwLjZyZW07XHJcbiAgICBoZWlnaHQ6IDE2dmg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xyXG4gICAgZm9udC1zaXplOiAwLjhyZW07XHJcbn1cclxuXHJcbmlvbi1jaGVja2JveCB7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIGhlaWdodDogMTVweDtcclxuICAgIHdpZHRoOiAxNXB4O1xyXG59Il19 */";
      /***/
    },

    /***/
    "FRMo":
    /*!************************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examinstruction/examinstruction.page.ts ***!
      \************************************************************************************/

    /*! exports provided: ExaminstructionPage */

    /***/
    function FRMo(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExaminstructionPage", function () {
        return ExaminstructionPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_examinstruction_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./examinstruction.page.html */
      "tebI");
      /* harmony import */


      var _examinstruction_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./examinstruction.page.scss */
      "5Rea");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../nExamHall-Services/api.service */
      "xHtS");
      /* harmony import */


      var _nExamHall_Services_alert_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../nExamHall-Services/alert.service */
      "bFVc");
      /* harmony import */


      var _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../nExamHall-Services/common-services.service */
      "FE8S");
      /* harmony import */


      var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../nExamHall-Services/local-storage.service */
      "2eCX");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../nExamHall-Services/loader.service */
      "9mvZ");

      var ExaminstructionPage = /*#__PURE__*/function () {
        function ExaminstructionPage(apiService, alertService, localStorage, commonService, activeRoute, router, loader) {
          _classCallCheck(this, ExaminstructionPage);

          this.apiService = apiService;
          this.alertService = alertService;
          this.localStorage = localStorage;
          this.commonService = commonService;
          this.activeRoute = activeRoute;
          this.router = router;
          this.loader = loader;
          this.defaultHref = "";
          this.instructionData = {};
          this.selectOptions = {
            header: "Select a Language"
          };
        }

        _createClass(ExaminstructionPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this = this;

            debugger;
            this.mockid = this.activeRoute.snapshot.paramMap.get("mockid");
            this.examstatus = this.activeRoute.snapshot.paramMap.get("examstatus");
            this.examseq = this.activeRoute.snapshot.paramMap.get("examseq");
            this.title = "General Instructions";
            this.instructionData = this.commonService.instructionData();
            this.localStorage.getObject("exam_details").then(function (result) {
              if (result != null) {
                _this.retrievedObject = result;
                _this.examid = _this.retrievedObject.examid;
                _this.testseriesid = _this.retrievedObject.testseriesid;
                _this.examname = _this.retrievedObject.examname;
                _this.examDuration = _this.retrievedObject.timeduration;
                _this.totalQues = _this.retrievedObject.questioncount;
                _this.markPerQues = _this.retrievedObject.markPerQues;
                _this.negativeMark = _this.retrievedObject.negativeMark;
                _this.languageSuport = _this.retrievedObject.langsupport;
                _this.examinstruction = _this.retrievedObject.instructions; //console.log("this.exam_details :" + this.retrievedObject);

                for (var i = 0; i < _this.languageSuport.length; i++) {
                  var langCode = _this.languageSuport[i].lngcode; // var index = this.examinstruction.indexOf(langCode);

                  var index = _this.examinstruction.findIndex(function (item) {
                    return item.langcode == langCode;
                  });

                  if (index > -1) {
                    _this.instructionData[langCode].examInst = _this.examinstruction[index].instructions;
                  }
                }

                console.log(_this.instructionData);
                _this.generalInst = _this.instructionData["en"].generalInst;
                _this.examInst = _this.instructionData["en"].examInst;
                _this.Declaration = _this.instructionData["en"].Declaration;
                _this.DeclarationTitle = _this.instructionData["en"].DeclarationTitle;
                _this.currentLanguage = _this.languageSuport[0].lngcode;
                _this.isAccepted = false;
              }
            })["catch"](function (e) {
              console.log("error: " + e);
            });
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            var _this2 = this;

            this.localStorage.get('theme').then(function (res) {
              _this2.theme = res; // const tabBar = this.bottomBar.nativeElement
              // tabBar.style.display = 'none';

              _this2.commonService.hideTabs();

              _this2.defaultHref = "nexamhallcandidate/myexams/mockexamlist/".concat(_this2.examid, "/").concat(_this2.testseriesid);
            });
          }
        }, {
          key: "onSlideChangeStart",
          value: function onSlideChangeStart(event) {}
        }, {
          key: "slideChanged",
          value: function slideChanged(langCode) {
            var _this3 = this;

            // let currentIndex = this.slides.getActiveIndex();
            // console.log("Current index is", currentIndex);
            this.slides.getActiveIndex().then(function (index) {
              console.log(index);

              if (index == 0) {
                if (langCode == "en") {
                  _this3.title = _this3.instructionData[langCode].heading1;
                } else if (langCode == "ta") {
                  _this3.title = _this3.instructionData[langCode].heading1;
                }
              } else if (index == 1) {
                if (langCode == "en") {
                  _this3.title = _this3.instructionData[langCode].heading2;
                } else if (langCode == "ta") {
                  _this3.title = _this3.instructionData[langCode].heading2;
                }
              }
            });
          }
        }, {
          key: "onCancel",
          value: function onCancel() {
            console.log("cancel");
          }
        }, {
          key: "onChange",
          value: function onChange(currentLanguage) {
            this.currentLanguage = currentLanguage;
            this.slideChanged(this.currentLanguage);
            this.generalInst = this.instructionData[currentLanguage].generalInst;
            this.examInst = this.instructionData[currentLanguage].examInst;
            this.Declaration = this.instructionData[currentLanguage].Declaration;
            this.DeclarationTitle = this.instructionData[currentLanguage].DeclarationTitle;
            console.log(currentLanguage);
          }
        }, {
          key: "checked",
          value: function checked() {}
        }, {
          key: "StartTestConfirmation",
          value: function StartTestConfirmation(isAccepted) {
            if (isAccepted) {
              this.router.navigateByUrl("/nexamhallcandidate/myexams/examconductor/" + this.mockid + "/" + this.examstatus + "/" + this.examseq);
            } else {
              var msg = "Please select declaration checkbox";
              this.alertService.presentAlert(msg);
            }
          }
        }]);

        return ExaminstructionPage;
      }();

      ExaminstructionPage.ctorParameters = function () {
        return [{
          type: _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"]
        }, {
          type: _nExamHall_Services_alert_service__WEBPACK_IMPORTED_MODULE_5__["AlertService"]
        }, {
          type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"]
        }, {
          type: _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_6__["CommonServicesService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]
        }, {
          type: _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"]
        }];
      };

      ExaminstructionPage.propDecorators = {
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ["slides", {
            "static": true
          }]
        }]
      };
      ExaminstructionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-examinstruction',
        template: _raw_loader_examinstruction_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_examinstruction_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ExaminstructionPage);
      /***/
    },

    /***/
    "GYYY":
    /*!**********************************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examinstruction/examinstruction-routing.module.ts ***!
      \**********************************************************************************************/

    /*! exports provided: ExaminstructionPageRoutingModule */

    /***/
    function GYYY(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExaminstructionPageRoutingModule", function () {
        return ExaminstructionPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _examinstruction_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./examinstruction.page */
      "FRMo");

      var routes = [{
        path: '',
        component: _examinstruction_page__WEBPACK_IMPORTED_MODULE_3__["ExaminstructionPage"]
      }];

      var ExaminstructionPageRoutingModule = function ExaminstructionPageRoutingModule() {
        _classCallCheck(this, ExaminstructionPageRoutingModule);
      };

      ExaminstructionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ExaminstructionPageRoutingModule);
      /***/
    },

    /***/
    "tebI":
    /*!****************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/examinstruction/examinstruction.page.html ***!
      \****************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function tebI(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n      <ion-back-button [defaultHref]=\"defaultHref\"  ></ion-back-button>\n      \n    </div>\n    <div class=\"pageTitle\">\n      {{title}}\n    </div>\n    <div class=\"right\">\n        <button  class=\"headerButton\">\n          <ion-select\n          [(ngModel)]=\"currentLanguage\"\n          [interfaceOptions]=\"selectOptions\"\n          (ionCancel)=\"onCancel()\"\n          (ionChange)=\"onChange(currentLanguage)\"\n        >\n          <ion-select-option\n            *ngFor=\"let lang of languageSuport\"\n            value=\"{{lang.lngcode}}\"\n            >{{lang.lngname}}</ion-select-option\n          >\n        </ion-select>\n        </button>\n    </div>\n</div>\n<div class=\"main\">\n\n  <div class=\"container-fluid\">\n    <ion-slides\n      #slides\n      pager=\"true\"\n      (ionSlideDidChange)=\"slideChanged(currentLanguage)\"\n    >\n      <ion-slide>\n        <div class=\"inst1Container\" [innerHTML]=\"generalInst\"></div>\n      </ion-slide>\n\n      <ion-slide>\n        <div class=\"inst1Container\" [innerHTML]=\"examInst\"></div>\n      </ion-slide>\n    </ion-slides>\n  </div>\n\n  <div class=\"container\">\n    <div class=\"card declaration-box box-shadow\">\n      <b [innerHTML]=\"DeclarationTitle\"></b>\n      <ion-label>\n        <ion-checkbox\n          [(ngModel)]=\"isAccepted\"\n          (ionChange)=\"checked()\"\n          shape=\"square\"\n        ></ion-checkbox>\n        <span [innerHTML]=\"Declaration\"></span>\n      </ion-label>\n    </div>\n\n    <p class=\"my-2\">\n      <button\n        class=\"btn btn-success text-white btn-md btn-block\"\n        (click)=\"StartTestConfirmation(isAccepted)\"\n      >\n        Start Test\n      </button>\n    </p>\n  </div>\n  </div>\n\n</ion-content>";
      /***/
    },

    /***/
    "zy6S":
    /*!**************************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examinstruction/examinstruction.module.ts ***!
      \**************************************************************************************/

    /*! exports provided: ExaminstructionPageModule */

    /***/
    function zy6S(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExaminstructionPageModule", function () {
        return ExaminstructionPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _examinstruction_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./examinstruction-routing.module */
      "GYYY");
      /* harmony import */


      var _examinstruction_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./examinstruction.page */
      "FRMo");

      var ExaminstructionPageModule = function ExaminstructionPageModule() {
        _classCallCheck(this, ExaminstructionPageModule);
      };

      ExaminstructionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _examinstruction_routing_module__WEBPACK_IMPORTED_MODULE_5__["ExaminstructionPageRoutingModule"]],
        declarations: [_examinstruction_page__WEBPACK_IMPORTED_MODULE_6__["ExaminstructionPage"]]
      })], ExaminstructionPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=examinstruction-examinstruction-module-es5.js.map