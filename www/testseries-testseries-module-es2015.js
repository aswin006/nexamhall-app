(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["testseries-testseries-module"],{

/***/ "AG1y":
/*!****************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/testseries/testseries.module.ts ***!
  \****************************************************************************/
/*! exports provided: TestseriesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestseriesPageModule", function() { return TestseriesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _testseries_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./testseries-routing.module */ "G2Vl");
/* harmony import */ var _testseries_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./testseries.page */ "aDZ5");







let TestseriesPageModule = class TestseriesPageModule {
};
TestseriesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _testseries_routing_module__WEBPACK_IMPORTED_MODULE_5__["TestseriesPageRoutingModule"]
        ],
        declarations: [_testseries_page__WEBPACK_IMPORTED_MODULE_6__["TestseriesPage"]]
    })
], TestseriesPageModule);



/***/ }),

/***/ "G2Vl":
/*!************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/testseries/testseries-routing.module.ts ***!
  \************************************************************************************/
/*! exports provided: TestseriesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestseriesPageRoutingModule", function() { return TestseriesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _testseries_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./testseries.page */ "aDZ5");




const routes = [
    {
        path: '',
        component: _testseries_page__WEBPACK_IMPORTED_MODULE_3__["TestseriesPage"]
    }
];
let TestseriesPageRoutingModule = class TestseriesPageRoutingModule {
};
TestseriesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TestseriesPageRoutingModule);



/***/ }),

/***/ "aDZ5":
/*!**************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/testseries/testseries.page.ts ***!
  \**************************************************************************/
/*! exports provided: TestseriesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestseriesPage", function() { return TestseriesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_testseries_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./testseries.page.html */ "m+XB");
/* harmony import */ var _testseries_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./testseries.page.scss */ "e2Zm");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let TestseriesPage = class TestseriesPage {
    constructor() { }
    ngOnInit() {
    }
};
TestseriesPage.ctorParameters = () => [];
TestseriesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-testseries',
        template: _raw_loader_testseries_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_testseries_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TestseriesPage);



/***/ }),

/***/ "e2Zm":
/*!****************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/testseries/testseries.page.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL3Rlc3RzZXJpZXMvdGVzdHNlcmllcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "m+XB":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/testseries/testseries.page.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>testseries</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=testseries-testseries-module-es2015.js.map