(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["signup-signup-module"], {
    /***/
    "PQuk":
    /*!**************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/signup/signup.page.scss ***!
      \**************************************************************/

    /*! exports provided: default */

    /***/
    function PQuk(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1Db3JlLVBhZ2VzL3NpZ251cC9zaWdudXAucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "Q5v/":
    /*!****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Core-Pages/signup/signup.page.html ***!
      \****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function Q5v(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\" >\n      \n      <ion-back-button [defaultHref]=\"defaultHref\">\n\n      </ion-back-button>\n      \n    </ion-buttons>\n    <ion-buttons slot=\"secondary\">\n      <ion-button>\n        <ion-icon slot=\"icon-only\" [hidden]=\"true\" name=\"star\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"text-center\">Sign Up</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"container\">\n   \n    <form #loginForm=\"ngForm\" novalidate>\n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"username\">Username</label>\n        <input\n          id=\"username\"\n          type=\"text\"\n          [(ngModel)]=\"firstName\"\n          name=\"username\"\n          placeholder=\"Enter your name\"\n          class=\"form-control bg-white  border-md\"\n        />\n      </div>\n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"email\">Email address</label>\n        <input\n          id=\"email\"\n          type=\"text\"\n          [(ngModel)]=\"loginId\"\n          name=\"email\"\n          placeholder=\"Enter your email\"\n          class=\"form-control bg-white  border-md\"\n        />\n      </div>\n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"mobnumber\">Mobile number</label>\n        <input\n          id=\"mobnumber\"\n          type=\"text\"\n          [(ngModel)]=\"phoneno\"\n          name=\"mobnumber\"\n          placeholder=\"Enter your Mobile number\"\n          class=\"form-control bg-white  border-md\"\n        />\n      </div>\n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"password\">password</label>\n        <input\n          id=\"password\"\n          type=\"password\"\n          [(ngModel)]=\"password\"\n          name=\"password\"\n          placeholder=\"Enter your password\"\n          class=\"form-control bg-white  border-md\"\n        />\n      </div>\n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"confrmpassword\">Confirm password</label>\n        <input\n          id=\"confrmpassword\"\n          type=\"password\"\n          [(ngModel)]=\"confirmpassword\"\n          name=\"confrmpassword\"\n          placeholder=\"Enter your confirm password\"\n          class=\"form-control bg-white  border-md\"\n        />\n      </div>\n\n      <!-- Submit Button -->\n      <div class=\"form-group col-lg-12 mx-auto mb-0\">\n        <button\n          class=\"btn btn-primary btn-block py-2 text-white\"\n          (click)=\"onLogin(loginForm)\"\n        >\n          <span class=\"font-weight-normal \">Signup</span>\n        </button>\n      </div>\n\n      <!-- Divider Text -->\n      <div class=\"\" *ngIf=\"publicsupport\">\n        <div class=\"form-group col-lg-12 mx-auto d-flex align-items-center my-4\">\n          <div class=\"border-bottom w-100 ml-5\"></div>\n          <span class=\"px-2 small text-muted font-weight-bold text-muted\"\n            >OR</span\n          >\n          <div class=\"border-bottom w-100 mr-5\"></div>\n        </div>\n  \n        <!-- Social Login -->\n        <div class=\"flex-c p-b-112\" >\n          <button class=\"login100-social-item\">\n            <i class=\"fa fa-facebook-f\"></i>\n          </button>\n  \n          <button  class=\"login100-social-item\">\n            <img src=\"/assets/images/icons/icon-google.png\" alt=\"GOOGLE\">\n          </button>\n        </div>\n       \n        <!-- Already Registered -->\n        <div class=\"text-center w-100\" >\n          <p class=\"text-muted font-weight-bold\">\n            Don't have an account?\n            <span href=\"\" (click)=\"onSignup()\" class=\"text-primary ml-2\"\n              >Sign Up</span\n            >\n          </p>\n        </div>\n      </div>\n     \n    </form>\n  </div>\n</ion-content>\n ";
      /***/
    },

    /***/
    "Qf9E":
    /*!**********************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/signup/signup-routing.module.ts ***!
      \**********************************************************************/

    /*! exports provided: SignupPageRoutingModule */

    /***/
    function Qf9E(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignupPageRoutingModule", function () {
        return SignupPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _signup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./signup.page */
      "a4qp");

      var routes = [{
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_3__["SignupPage"]
      }];

      var SignupPageRoutingModule = function SignupPageRoutingModule() {
        _classCallCheck(this, SignupPageRoutingModule);
      };

      SignupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SignupPageRoutingModule);
      /***/
    },

    /***/
    "a4qp":
    /*!************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/signup/signup.page.ts ***!
      \************************************************************/

    /*! exports provided: SignupPage */

    /***/
    function a4qp(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignupPage", function () {
        return SignupPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./signup.page.html */
      "Q5v/");
      /* harmony import */


      var _signup_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./signup.page.scss */
      "PQuk");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var SignupPage = /*#__PURE__*/function () {
        function SignupPage() {
          _classCallCheck(this, SignupPage);

          this.defaultHref = '';
        }

        _createClass(SignupPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.defaultHref = '/nexamhallcore/auth/login';
          }
        }]);

        return SignupPage;
      }();

      SignupPage.ctorParameters = function () {
        return [];
      };

      SignupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-signup',
        template: _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_signup_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SignupPage);
      /***/
    },

    /***/
    "hxpP":
    /*!**************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/signup/signup.module.ts ***!
      \**************************************************************/

    /*! exports provided: SignupPageModule */

    /***/
    function hxpP(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignupPageModule", function () {
        return SignupPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./signup-routing.module */
      "Qf9E");
      /* harmony import */


      var _signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./signup.page */
      "a4qp");

      var SignupPageModule = function SignupPageModule() {
        _classCallCheck(this, SignupPageModule);
      };

      SignupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignupPageRoutingModule"]],
        declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]]
      })], SignupPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=signup-signup-module-es5.js.map