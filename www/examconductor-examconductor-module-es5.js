(function () {
  function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

  function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["examconductor-examconductor-module"], {
    /***/
    "+zj+":
    /*!************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/examconductor/examconductor.page.html ***!
      \************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function zj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <ion-header>\n  <div class=\"exam-header pb-6\" [ngClass]=\"theme\">\n    <div class=\"appHeader  text-light\" >\n      <div class=\"left\">\n        <button class=\"btn \">\n          <ion-icon slot=\"start\" name=\"pause\"></ion-icon>\n        </button>\n        10:35\n      </div>\n      <div class=\"pageTitle\">\n        {{title}}\n      </div>\n      <div class=\"right\">\n          <button  class=\"headerButton\">\n            Submit\n          </button>\n      </div>\n  </div>\n   \n      <div class=\"cross-header-body\">\n        <div class=\"py-1\">\n          <div class=\"card-body user-avatar\">\n            <div class=\"row\">\n             \n              <ion-label>\n                <h2>{{examname}}</h2>\n                <p><span>gfhkdghfdgsfdgsd</span>/<span>fghfdbkgfjkd</span></p>\n              </ion-label>\n            </div>\n          </div>\n        </div>\n      \n      </div>\n    \n  </div>\n</ion-header>\n\n<ion-content>\n\n</ion-content> -->\n<ion-header>\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n      <div class=\"headerButton goBack\">\n        <div class=\"pageTitle\">{{examname}}</div>\n      </div>\n    </div>\n    <div class=\"right\">\n      <!--<button\n        class=\"headerButton\"\n        data-toggle=\"modal\"\n        data-target=\"#exampleModal\"\n      >-->\n      <button class=\"headerButton\" (click)=\"openModalDialog()\">\n        <ion-icon\n          class=\"icon md hydrated\"\n          name=\"menu-outline\"\n          role=\"img\"\n          aria-label=\"menu outline\"\n        ></ion-icon>\n      </button>\n    </div>\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"examconductor-page mt-5\" [ngClass]=\"theme\">\n    <div class=\"container card examconductor-main-card\">\n      <ion-toolbar>\n        <ion-buttons slot=\"start\"> {{qno}} | {{totalquestion}} </ion-buttons>\n        <ion-buttons slot=\"primary\" >\n          <button class=\"pause-button btn-sm\" *ngIf=\"showPauseBtn\">\n            <ion-icon\n              slot=\"icon-only\"\n              ios=\"pause-outline\"\n              md=\"pause-outline\"\n            ></ion-icon>\n          </button>\n        </ion-buttons>\n\n        <ion-title class=\"overall-time text-center\">{{time_duration}}</ion-title>\n\n        <ion-progress-bar value=\"{{progress}}\"></ion-progress-bar>\n      </ion-toolbar>\n      <div class=\"text-center\">\n        <ion-slides\n          #slides\n          (ionSlidePrevEnd)=\"slideChangeBack(qno)\"\n          (ionSlideNextEnd)=\"slideChangeNext(qno)\"\n        >\n          <ion-slide *ngFor=\"let ques of filteredquestion\">\n            <div class=\"card box-shadow examconductor-inner-card\">\n              <!-- <ion-toolbar>\n                <ion-buttons slot=\"start\">\n                  <ion-button> {{time_spent | async}} </ion-button>\n                </ion-buttons>\n                <ion-buttons slot=\"end\">\n                  <ion-button>\n                    <ion-icon\n                      slot=\"icon-only\"\n                      ios=\"bookmark-outline\"\n                      md=\"bookmark-outline\"\n                    ></ion-icon>\n                  </ion-button>\n                </ion-buttons>\n                <ion-title class=\"text-center\">\n                  <ion-chip>\n                    <ion-label>{{qno}}</ion-label>\n                  </ion-chip>\n                </ion-title>\n              </ion-toolbar> -->\n              <div class=\"row header-box justify-content-between\">\n                <div class=\"col-4 time-spent\">\n                   <p>{{time_spent | async}}</p>\n                </div>\n                <div class=\"col-4\">\n                  <div class=\"badge\">\n                    <span class=\"text-center\">{{qno}}</span>\n                  </div>\n                 \n                </div>\n                <div class=\"col-4 mark-review-btn\">\n                  <ion-icon\n                  slot=\"icon-only\"\n                  color=\"warning\"\n                  [name]=\"reviewflag == true ? 'bookmark' : 'bookmark-outline'\"\n                  (click)=\"markreview()\"\n                ></ion-icon>\n                </div>\n              </div>\n              <hr />\n              <div class=\"question-line clearfix\">\n                <ion-label [innerHtml]=\"question_value\"></ion-label>\n              </div>\n              <!--<ion-list radio-group [(ngModel)]=\"selectedOption\">\n                <ion-item *ngFor=\"let key of optionkeys; let i = index;\">\n                  <ion-label [innerHtml]=\"Options[key]\"></ion-label>\n                  <ion-radio\n                    slot=\"start\"\n                    color=\"success\"\n                    value=\"{{key}}\"\n                  ></ion-radio>\n                </ion-item>\n              </ion-list>-->\n              <ion-list class=\"optionlist\">\n                <ion-radio-group>\n                  <ion-item\n                    *ngFor=\" let key of optionkeys\"\n                    (click)=\"storeOption(qno,key)\"\n                  >\n                    <ion-label\n                      [innerHtml]=\"Options[key]\"\n                      *ngIf=\"Options[key]\"\n                    ></ion-label>\n                    <ion-radio\n                      *ngIf=\"Options[key]\"\n                      slot=\"start\"\n                      color=\"success\"\n                      mode=\"md\"\n                      name=\"{{key}}\"\n                      value=\"{{key}}\"\n                    ></ion-radio>\n                  </ion-item>\n                </ion-radio-group>\n              </ion-list>\n            </div>\n          </ion-slide>\n        </ion-slides>\n      </div>\n\n      <!--<div class=\"card box-shadow examconductor-inner-card\">\n        <ion-slides pager=\"false\">\n          <ion-slide *ngFor=\"let ques of [1,2,3,4,5]\">\n            <ion-toolbar>\n              <ion-buttons slot=\"start\">\n                <ion-button> 00:001 </ion-button>\n              </ion-buttons>\n              <ion-buttons slot=\"end\">\n                <ion-button>\n                  <ion-icon\n                    slot=\"icon-only\"\n                    ios=\"bookmark-outline\"\n                    md=\"bookmark-outline\"\n                  ></ion-icon>\n                </ion-button>\n              </ion-buttons>\n              <ion-title>\n                <ion-chip>\n                  <ion-label>{{ques}}</ion-label>\n                </ion-chip>\n              </ion-title>\n            </ion-toolbar>\n          </ion-slide>\n        </ion-slides>\n      </div>-->\n      <ion-toolbar>\n        <ion-button class=\"prevbtn\" (click)=\"prev()\">\n          <ion-icon slot=\"start\" name=\"arrow-back-outline\"></ion-icon>\n          Previous\n        </ion-button>\n\n        <ion-button class=\"nextbtn\" slot=\"end\" (click)=\"next()\">\n          Next\n\n          <ion-icon slot=\"end\" name=\"arrow-forward-outline\"></ion-icon>\n        </ion-button>\n      </ion-toolbar>\n    </div>\n  </div>\n\n  <div\n    class=\"modal fade panelbox panelbox-right\"\n    tabindex=\"-1\"\n    role=\"dialog\"\n    #exampleModal\n    id=\"exampleModal\"\n    aria-modal=\"true\"\n    [ngClass]=\"{'show':isModalShow == true}\"\n    [ngStyle]=\"{'display':modalDisplay}\"\n  >\n    <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header\">\n          <h5 class=\"modal-title\">Summary</h5>\n          <button\n            type=\"button\"\n            class=\"close\"\n            data-dismiss=\"modal\"\n            aria-label=\"Close\"\n            (click)=\"closeModalDialog()\"\n          >\n            <span aria-hidden=\"true\">&times;</span>\n          </button>\n        </div>\n        <div class=\"modal-body\">\n          <div class=\"status_box\">\n            <ul class=\"list-group list-group-flush\">\n              <li\n                class=\"list-group-item d-flex justify-content-between align-items-center\"  >\n                Answered\n                <span class=\"badge badge-success badge-pill custom-badge\"\n                  >14</span\n                >\n              </li>\n              <li\n                class=\"list-group-item d-flex justify-content-between align-items-center\"\n              >\n                Unanswered\n                <span class=\"badge badge-danger badge-pill custom-badge\"\n                  >2</span\n                >\n              </li>\n              \n              <li\n                class=\"list-group-item d-flex justify-content-between align-items-center\"\n              >\n                Mark for review\n                <span class=\"badge badge-warning badge-pill custom-badge\"\n                  >1</span\n                >\n              </li>\n              <li\n                class=\"list-group-item d-flex justify-content-between align-items-center\"\n              >\n                Marked & Answered\n                <span class=\"badge badge-brown badge-pill custom-badge\"\n                  >1</span\n                >\n              </li>\n              <li\n                class=\"list-group-item d-flex justify-content-between align-items-center\"\n              >\n                Unseen\n                <span class=\"badge badge-white badge-pill custom-badge\"\n                  >1</span\n                >\n              </li>\n            </ul>\n          </div>\n          <div class=\"qnh-inner\">\n            <ul class=\"questionList slim-scrollbar\">\n              <li\n                class=\"label btn btn-default ques_nav_btn activeQues\"\n                *ngFor=\"let data of filteredquestion track by index\"\n                [ngClass]=\"{'activeQues':data.questionno == '0', \n'success':data.questionno == '1' ,\n'failed': data.questionno == '2','Mark_for_review':data.questionno == '3',\n'Marked_ans':data.questionno == '4'}\"\n                (click)=\"setActiveQuestion(qno,data.questionno)\"\n              >\n                <ion-icon\n                  class=\"bookmark-badge badge-corner radius-0\"\n                  name=\"bookmark\"\n                  color=\"primary\"\n                ></ion-icon>\n\n                <span class=\"qnol\">{{data.questionno}}</span>\n              </li>\n            </ul>\n            <!--<ul class=\"questionList\">\n              <li *ngFor=\"let data of filteredquestion\">\n                <a href=\"#\" class=\"btn btn-success\">\n                  <span class=\"badge badge-dark badge-corner radius-0\">2</span>\n                  {{data.questionno}}\n                </a>\n              </li>\n            </ul>-->\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button\n            type=\"button\"\n            class=\"btn btn-block btn-md btn-primary mb-0\"\n            data-toggle=\"modal\"\n            data-target=\"#sumbitmodal\"\n            (click)=\"closeModalDialog()\"\n          >\n            Submit\n          </button>\n        </div>\n      </div>\n    </div>\n  </div>\n  \n  <!--summary modal-->\n  <div\n    class=\"modal fade\"\n    id=\"sumbitmodal\"\n    tabindex=\"-1\"\n    role=\"dialog\"\n    aria-labelledby=\"exampleModalLabel\"\n    aria-hidden=\"true\"\n  >\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n        <div class=\"modal-header summary-header text-center\">\n          <h2 class=\"modal-title\" id=\"exampleModalLabel\">Summary</h2>\n        </div>\n        <div class=\"modal-body\">\n          <ul class=\"list-group list-group-flush summary-list\">\n            <li\n              class=\"list-group-item d-flex justify-content-between align-items-center\"\n            >\n              Answered\n              <span class=\"badge badge-primary badge-pill custom-badge\"\n                >14</span\n              >\n            </li>\n            <li\n              class=\"list-group-item d-flex justify-content-between align-items-center\"\n            >\n              Unanswered\n              <span class=\"badge badge-primary badge-pill custom-badge\">2</span>\n            </li>\n            <li\n              class=\"list-group-item d-flex justify-content-between align-items-center\"\n            >\n              Unseen\n              <span class=\"badge badge-primary badge-pill custom-badge\">1</span>\n            </li>\n            <li\n              class=\"list-group-item d-flex justify-content-between align-items-center\"\n            >\n              Mark for review\n              <span class=\"badge badge-primary badge-pill custom-badge\">1</span>\n            </li>\n            <li\n              class=\"list-group-item d-flex justify-content-between align-items-center\"\n            >\n              Marked & Answered\n              <span class=\"badge badge-primary badge-pill custom-badge\">1</span>\n            </li>\n          </ul>\n          <div class=\"text-center\">\n            <h2 class=\"font-weight-600\">\n              Are you sure want to submit the test?\n            </h2>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <ion-grid>\n            <ion-row>\n              <ion-col>\n                <button\n                  type=\"button\"\n                  class=\"btn btn-outline-secondary btn-lg btn-block\"\n                  data-dismiss=\"modal\"\n                >\n                  No\n                </button>\n              </ion-col>\n              <ion-col>\n                <button\n                  type=\"button\"\n                  class=\"btn btn-warning text-white btn-lg btn-block\"\n                  (click)=\"goToExam()\"\n                >\n                  Yes\n                </button>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n";
      /***/
    },

    /***/
    "DYKa":
    /*!********************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examconductor/examconductor.page.ts ***!
      \********************************************************************************/

    /*! exports provided: ExamconductorPage */

    /***/
    function DYKa(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExamconductorPage", function () {
        return ExamconductorPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_examconductor_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./examconductor.page.html */
      "+zj+");
      /* harmony import */


      var _examconductor_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./examconductor.page.scss */
      "fiLJ");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../nExamHall-Services/local-storage.service */
      "2eCX");
      /* harmony import */


      var _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../nExamHall-Services/api.service */
      "xHtS");
      /* harmony import */


      var _nExamHall_Services_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../nExamHall-Services/toast.service */
      "RWSw");
      /* harmony import */


      var _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../nExamHall-Services/common-services.service */
      "FE8S");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! rxjs */
      "qCKp");

      var ExamconductorPage = /*#__PURE__*/function () {
        function ExamconductorPage(localStorage, apiService, route, router, actionSheetController, toast, commonService) {
          _classCallCheck(this, ExamconductorPage);

          this.localStorage = localStorage;
          this.apiService = apiService;
          this.route = route;
          this.router = router;
          this.actionSheetController = actionSheetController;
          this.toast = toast;
          this.commonService = commonService;
          this.progress = 0;
          this.getExamData = {};
          this.hoursS = "00";
          this.minutesS = "00";
          this.secondsS = "00";
          this.option = {};
          this.checkboxvalue = "A";
          this.nextBtnDisabled = false;
          this.prevBtnDisabled = false;
          this.time_spent = new rxjs__WEBPACK_IMPORTED_MODULE_10__["BehaviorSubject"]("00:00:00");
          this.modalDisplay = "none";
          this.reviewflag = false;

          this.getCurrentFilteredQuestion = function (question, qno) {
            var filteredques = this.filteredquestion.filter(function (item) {
              return item.questionno == qno;
            });
            return filteredques; // var filteredques = $filter('filter')(question, function(value, index) {
            //     return value.questionno == qno;
            // })[0];
            // return filteredques;
          };
        }

        _createClass(ExamconductorPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this = this;

            this.mockid = this.route.snapshot.paramMap.get("mockid");
            this.examstatus = this.route.snapshot.paramMap.get("examstatus");
            this.examseq = this.route.snapshot.paramMap.get("examseq");
            this.localStorage.get("currentExamStatus").then(function (result) {
              console.log("1", result);
            })["catch"](function (e) {
              console.log("error: " + e);
            });
            this.localStorage.getObject("candidateUserData").then(function (result) {
              if (result) {
                var userdata = result;
                _this.loginId = userdata.loginId;
                _this.instituteid = userdata.instituteuserid;
              }
            })["catch"](function (e) {
              console.log("error: " + e);
            });
            this.localStorage.getObject("exam_details").then(function (result) {
              if (result) {
                var exam_details = result;
                _this.examid = exam_details.examid;
                _this.languageSuport = exam_details.langsupport;
                _this.testseriesid = exam_details.testseriesid;
                _this.showPauseBtn = exam_details.showpausebtn;
                _this.getExamData = {
                  loginId: _this.loginId,
                  instituteid: _this.instituteid,
                  examid: _this.examid,
                  mockid: _this.mockid,
                  examstatus: _this.examstatus,
                  examseq: _this.examseq,
                  defaultlanguage: _this.defaultLanguage,
                  created: _this.startDate,
                  testseriesid: _this.testseriesid
                };

                _this.loadData(_this.getExamData);
              }
            })["catch"](function (e) {
              console.log("error: " + e);
            });
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            var _this2 = this;

            eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
            this.localStorage.get('theme').then(function (res) {
              _this2.theme = res;

              _this2.commonService.hideTabs();
            });
          }
        }, {
          key: "loadData",
          value: function loadData(getExamData) {
            var _this3 = this;

            debugger;
            this.apiService.startTest(getExamData).subscribe(function (res) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var response;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        response = res["questiondata"];
                        this.question = response[0].questionsblock;
                        this.totalquestion = this.question.length;
                        this.examname = response[0].examname;
                        this.test_data = response[0];
                        this.test_answer = this.question;

                        if (this.examstatus == 0 || this.examstatus == 2) {
                          this.counterdown = this.test_data.examDuration * 60;
                          this.current = 0;
                          this.counter = 0;
                          this.index = 0;
                        } else {
                          this.counterdown = this.test_data.totaltimespent;
                        }

                        this.Startcoundown();
                        this.getfilterquestion("A");
                        console.log(res);

                      case 10:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });
          }
        }, {
          key: "pad",
          value: function pad(n) {
            return (n < 10 ? "0" : "") + n;
          } // starttimer = function () {
          //   this.timer = setInterval(() => {
          //     this.counter = this.counter + 1;
          //     this.hours = Math.floor(this.counter / 60 / 60);
          //     this.minutes = Math.floor((this.counter / 60) % 60);
          //     this.seconds = Math.floor(this.counter % 60);
          //     this.time_spent =
          //       this.pad(this.hours) +
          //       ":" +
          //       this.pad(this.minutes) +
          //       ":" +
          //       this.pad(this.seconds);
          //   }, 1000);
          // };

        }, {
          key: "starttimer",
          value: function starttimer() {
            var _this4 = this;

            clearInterval(this.interval);
            this.updateTime();
            this.interval = setInterval(function () {
              _this4.updateTime();
            }, 1000);
          }
        }, {
          key: "stoptimer",
          value: function stoptimer() {
            clearInterval(this.interval);
          }
        }, {
          key: "updateTime",
          value: function updateTime() {
            this.counter = this.counter + 1;
            this.hours = Math.floor(this.counter / 60 / 60);
            this.minutes = Math.floor(this.counter / 60 % 60);
            this.seconds = Math.floor(this.counter % 60);
            var text = this.pad(this.hours) + ":" + this.pad(this.minutes) + ":" + this.pad(this.seconds);
            this.time_spent.next(text);
          }
        }, {
          key: "Startcoundown",
          value: function Startcoundown() {
            var _this5 = this;

            this.timeup = setInterval(function () {
              _this5.counterdown -= 1;
              _this5.hoursS = Math.floor(_this5.counterdown / 60 / 60);
              _this5.minutesS = Math.floor(_this5.counterdown / 60 % 60);
              _this5.secondsS = Math.floor(_this5.counterdown % 60);
              _this5.time_duration = _this5.pad(_this5.hoursS) + ":" + _this5.pad(_this5.minutesS) + ":" + _this5.pad(_this5.secondsS);

              if (_this5.counterdown < 0) {
                _this5.hoursS = "00";
                _this5.minutesS = "00";
                _this5.secondsS = "00";
                _this5.counterdown = 0;
              }
            }, 1000);
          }
        }, {
          key: "questionrefresh",
          value: function questionrefresh(checkboxvalue) {
            if (checkboxvalue == "A") {
              this.filteredquestion = this.question;
            } else {
              this.filteredquestion = this.filteredquestion.filter(function (item) {
                return item.answerstatus == checkboxvalue;
              });
            }
          }
        }, {
          key: "getfilterquestion",
          value: function getfilterquestion(checkboxvalue) {
            this.questionrefresh(checkboxvalue);
            this.show_question(0);
          } // slideChanged(qno) {
          //   // let currentIndex = this.slides.getActiveIndex();
          //   // console.log("Current index is", currentIndex);
          //   this.stoptimer();
          //   this.show_question(qno);
          //   this.slides.getActiveIndex().then((index) => {
          //     console.log(index);
          //   });
          // }

        }, {
          key: "check_status",
          value: function check_status(reviewflag) {
            debugger;

            if (this.option.selected_option != undefined && this.option.selected_option != "0") {
              if (reviewflag) {
                this.answerstatus = "4";
              } else {
                this.answerstatus = "1";
              }
            } else {
              if (reviewflag) {
                this.answerstatus = "3";
              } else {
                this.answerstatus = "2";
              }
            }
          }
        }, {
          key: "check_status2",
          value: function check_status2(statusupdate, currentstatus) {
            if (currentstatus == "0" || currentstatus == '') {
              this.answerstatus = "2";
            } else {
              this.answerstatus = currentstatus;
            }
          }
        }, {
          key: "status_count",
          value: function status_count(oldstatus, newstatus) {
            //debugger;
            if (oldstatus != newstatus) {
              if (oldstatus != 0) {
                if (oldstatus == "1") {
                  this.answered_count = this.answered_count - 1;
                } else if (oldstatus == "2") {
                  this.unanswered_count = this.unanswered_count - 1;
                } else if (oldstatus == "3") {
                  this.markforreview_count = this.markforreview_count - 1;
                } else if (oldstatus == "4") {
                  this.marked_ans_count = this.marked_ans_count - 1;
                }
              }
              /******************* */


              if (newstatus == "1") {
                this.answered_count = this.answered_count + 1;
              } else if (newstatus == "2") {
                this.unanswered_count = this.unanswered_count + 1;
              } else if (newstatus == "3") {
                this.markforreview_count = this.markforreview_count + 1;
              } else if (newstatus == "4") {
                this.marked_ans_count = this.marked_ans_count + 1;
              }
            }
          }
        }, {
          key: "save_answer",
          value: function save_answer(currentqno, ansupdate) {
            this.index = currentqno;
            this.status_count(this.test_answer[this.index].answerstatus, this.answerstatus);

            if (ansupdate == "Y") {
              this.test_answer[this.index].selectoption = this.selected_option;
            }

            this.test_answer[this.index].seqid = this.test_data._id;
            this.test_answer[this.index].ques_id = this.test_data.questionsblock[this.index].questionid;
            this.test_answer[this.index].answerstatus = this.answerstatus;
            this.test_answer[this.index].time_spent = this.counter;
            this.test_answer[this.index].totaltimespent = this.counterdown;

            if (this.counterdown <= 0) {
              this.test_answer[this.index].examstatus = "2";
            } else {
              this.test_answer[this.index].examstatus = "1";
            }

            this.test_answer[this.index].answered_count = this.answered_count;
            this.test_answer[this.index].unanswered_count = this.unanswered_count;
            this.test_answer[this.index].markforreview_count = this.markforreview_count;
            this.test_answer[this.index].marked_ans_count = this.marked_ans_count;
            this.test_answer[this.index].not_visited = this.totalquestion - (this.answered_count + this.unanswered_count + this.markforreview_count + this.marked_ans_count);
            this.test_answer[this.index].lastquestionvist = currentqno; ///****save local and finaly submit btn clicked send to backend */

            this.localStorage.setObject('testanswer', this.test_answer);
          }
        }, {
          key: "iterate_object",
          value: function iterate_object(o) {
            var keys = Object.keys(o);

            for (var i = 0; i < keys.length; i++) {
              return [keys[i], o[keys[i]]];
            }
          }
        }, {
          key: "findKey",
          value: function findKey(obj, langKeys) {
            // debugger;
            var findkey = null;

            var _iterator = _createForOfIteratorHelper(this.iterate_object(obj)),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var _step$value = _slicedToArray(_step.value, 2),
                    key = _step$value[0],
                    val = _step$value[1];

                if (key == langKeys) {
                  if (obj[key].question) {
                    findkey = true;
                    break;
                  } else {
                    findkey = false;
                    break;
                  }
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            return findkey;
          }
        }, {
          key: "checkquestionSupport",
          value: function checkquestionSupport(qno) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var questions, tamil, english;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      //  debugger;
                      questions = this.getCurrentFilteredQuestion(this.filteredquestion, qno);
                      /**22-05-20-Aswin-mobile-view-sidebar-click-error*/

                      tamil = this.findKey(questions, 'ta');
                      english = this.findKey(questions, 'en'); // if (tamil && english) {
                      //     var tempLang = await this.localStorage.get('defaultLanguage');
                      //     if (tempLang == "undefined" || tempLang == "null" || tempLang == undefined || tempLang == null) {
                      //         this.currentLanguage = this.defaultLanguage;
                      //         //this.setLang(this.currentLanguage);
                      //         this.setlangNgmodel(this.currentLanguage);
                      //     } else {
                      //         this.currentLanguage = tempLang;
                      //         this.languageSuport = getLocalStorageData('languageSupport');
                      //         // this.setLang(this.currentLanguage);
                      //         this.setlangNgmodel(this.currentLanguage);
                      //     }
                      // } else {
                      //     if (tamil) {
                      //         this.languageSuport = this.getDynamicLang('ta');
                      //         this.currentLanguage = this.languageSuport[0].lngcode;
                      //         // this.setLang(this.currentLanguage);
                      //         this.setlangNgmodel(this.currentLanguage);
                      //         //alert('tamil false')
                      //     }
                      //     if (english) {
                      //         this.languageSuport = this.getDynamicLang('en');
                      //         this.currentLanguage = this.languageSuport[0].lngcode;
                      //         // this.setLang(this.currentLanguage);
                      //         this.setlangNgmodel(this.currentLanguage);
                      //         //alert('English false')
                      //     }
                      // }

                    case 3:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "show_question",
          value: function show_question(nextqno) {
            var _this6 = this;

            debugger;
            this.index = nextqno;
            this.qno = this.filteredquestion[this.index].questionno;
            this.subjectName = this.filteredquestion[this.index].subject;
            this.unitName = this.filteredquestion[this.index].unit;
            this.subjectid = this.filteredquestion[this.index].subjectid;
            this.unitid = this.filteredquestion[this.index].unitid;
            this.ques_id = this.filteredquestion[this.index].questionid;
            this.question_value = this.filteredquestion[this.index]["en"].question;
            this.questiontype = this.filteredquestion[this.index].questiontype;
            this.parentid = this.filteredquestion[this.index].parentid;
            this.filteredComp = this.filteredquestion.filter(function (item) {
              return item.parentid == _this6.parentid;
            }); // (Que No. {{(test.sections[currentQuestion.sSNo - 1].questions[currentQuestion.qSNo - 1].startQno + 1) + " - " + (test.sections[currentQuestion.sSNo - 1].questions[currentQuestion.qSNo - 1].endQno + 1)}})

            this.filteredCompLength = this.filteredComp.length || "";
            this.compquesno = this.filteredquestion[this.index].compquesno; //console.log('comp:' + this.filteredCompLength.length)

            this.Options = {};
            this.Options = this.filteredquestion[this.index]["en"].options;
            this.optionkeys = Object.keys(this.Options);
            this.selected_option = this.filteredquestion[this.index].selectoption;
            this.answerstatus = this.filteredquestion[this.index].answerstatus;

            if (this.answerstatus == '4' || this.answerstatus == '3') {
              this.reviewflag = true;
            } else {
              this.reviewflag = false;
            }

            this.counter = this.filteredquestion[this.index].time_spent;
            this.not_visited = this.totalquestion - (this.answered_count + this.answered_count + this.markforreview_count + this.marked_ans_count);
            this.starttimer(); //counter starttimer

            eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
          }
        }, {
          key: "markreview",
          value: function markreview() {
            this.reviewflag = !this.reviewflag;
          }
        }, {
          key: "next_btn",
          value: function next_btn(questionno) {
            debugger;
            this.stoptimer();
            this.check_status(this.reviewflag); //chack answer or unanswer 

            this.qindex = questionno - 1;
            this.save_answer(this.qindex, "Y"); //save answer or insert test answer

            var findex = this.filteredquestion.findIndex(function (x) {
              return x.questionno === questionno;
            });
            this.nextqno = findex < this.filteredquestion.length - 1 ? findex + 1 : findex;

            if (findex == this.filteredquestion.length - 1) {
              this.toast.showToast("you are in last question", 2000, 'top', "dark");
              this.nextBtnDisabled = true;
              this.prevBtnDisabled = false;
            } else {
              this.show_question(this.nextqno);
              this.questionrefresh("A");
            }
          }
        }, {
          key: "prev_btn",
          value: function prev_btn(questionno) {
            this.stoptimer();
            this.qindex = questionno - 1;
            var findex = this.filteredquestion.findIndex(function (x) {
              return x.questionno === questionno;
            });
            this.save_answer(this.qindex, "Y");

            if (findex != 0) {
              this.prevBtnDisabled = false;
              this.nextBtnDisabled = false;
              this.qindex = questionno - 1; //question index this.current = this.index;
              //findex = find index

              var findex = this.filteredquestion.findIndex(function (x) {
                return x.questionno === questionno;
              }); // console.log(findex)
              // this.nextqno = findex < this.filteredquestion.length - 1 ? findex - 1 : findex;

              this.nextqno = findex - 1;

              if (findex == 0) {
                this.checkboxvalue = "A";
                this.getfilterquestion(this.checkboxvalue);
              } else {
                this.show_question(this.nextqno);
                this.questionrefresh(this.checkboxvalue);
              }
            } else {
              //alert('First question')
              this.prevBtnDisabled = true;
              this.nextBtnDisabled = false;
            }
          }
        }, {
          key: "openModalDialog",
          value: function openModalDialog() {
            this.modalDisplay = "block"; //Set block css

            this.isModalShow = true;
          }
        }, {
          key: "closeModalDialog",
          value: function closeModalDialog() {
            debugger;
            this.modalDisplay = "none"; //set none css after close dialog

            this.isModalShow = false;
          }
        }, {
          key: "filterquestion",
          value: function filterquestion(checkboxvalue, currentqno) {
            //debugger;
            this.closeModalDialog();

            if (checkboxvalue == undefined) {
              // var checkboxvalue = "A";aswin summary to filter not working 08-03-19
              this.checkboxvalue = "A";
            } else {
              this.checkboxvalue = checkboxvalue;
            }

            this.stoptimer();
            this.save_answer(currentqno - 1, "N");
            this.getfilterquestion(this.checkboxvalue);
          }
        }, {
          key: "setActiveQuestion",
          value: function setActiveQuestion(currentqno, nextqno) {
            // debugger;
            this.qindex = currentqno - 1;
            this.stoptimer(); // this.check_status2("N", this.answerstatus); //check answer or unanswer

            this.save_answer(this.qindex, "N"); //save answer or insert test answer

            var findex = this.filteredquestion.findIndex(function (x) {
              return x.questionno === nextqno;
            });
            this.nextqno = findex; //  this.questionrefresh(this.checkboxvalue);

            this.slides.slideTo(this.nextqno, 1000, false);
            this.show_question(this.nextqno); //question values show

            this.closeModalDialog();
          }
        }, {
          key: "slideChangeBack",
          value: function slideChangeBack(qno) {
            this.prev_btn(qno);
          }
        }, {
          key: "slideChangeNext",
          value: function slideChangeNext(qno) {
            this.next_btn(qno);
          }
        }, {
          key: "storeOption",
          value: function storeOption(qno, value) {
            if (qno && value) {
              this.filteredquestion[qno - 1].selectoption = value;
            } else {
              this.filteredquestion[qno - 1].selectoption = 0;
            }
          }
        }, {
          key: "next",
          value: function next() {
            //this.next_btn(qno);
            this.slides.slideNext();
          }
        }, {
          key: "prev",
          value: function prev() {
            // this.prev_btn(qno);
            this.slides.slidePrev();
          }
        }, {
          key: "goToExam",
          value: function goToExam() {
            this.router.navigateByUrl("/candidate/myexams");
          }
        }, {
          key: "presentActionSheet",
          value: function presentActionSheet() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var actionSheet;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.actionSheetController.create({
                        header: 'Albums',
                        cssClass: 'my-custom-class',
                        buttons: [{
                          text: 'Delete',
                          role: 'destructive',
                          icon: 'trash',
                          handler: function handler() {
                            console.log('Delete clicked');
                          }
                        }, {
                          text: 'Share',
                          icon: 'share',
                          handler: function handler() {
                            console.log('Share clicked');
                          }
                        }, {
                          text: 'Play (open modal)',
                          icon: 'caret-forward-circle',
                          handler: function handler() {
                            console.log('Play clicked');
                          }
                        }, {
                          text: 'Favorite',
                          icon: 'heart',
                          handler: function handler() {
                            console.log('Favorite clicked');
                          }
                        }, {
                          text: 'Cancel',
                          icon: 'close',
                          role: 'cancel',
                          handler: function handler() {
                            console.log('Cancel clicked');
                          }
                        }]
                      });

                    case 2:
                      actionSheet = _context3.sent;
                      _context3.next = 5;
                      return actionSheet.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "submit",
          value: function submit() {
            debugger;
            this.presentActionSheet();
          }
        }]);

        return ExamconductorPage;
      }();

      ExamconductorPage.ctorParameters = function () {
        return [{
          type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_6__["LocalStorageService"]
        }, {
          type: _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_7__["ApiService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ActionSheetController"]
        }, {
          type: _nExamHall_Services_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]
        }, {
          type: _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_9__["CommonServicesService"]
        }];
      };

      ExamconductorPage.propDecorators = {
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ["slides", {
            "static": true
          }]
        }]
      };
      ExamconductorPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-examconductor',
        template: _raw_loader_examconductor_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_examconductor_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ExamconductorPage);
      /***/
    },

    /***/
    "fiLJ":
    /*!**********************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examconductor/examconductor.page.scss ***!
      \**********************************************************************************/

    /*! exports provided: default */

    /***/
    function fiLJ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".examconductor-page {\n  height: calc(100vh - 56px);\n  position: fixed;\n  width: 100%;\n}\n\n.examconductor-main-card {\n  background: #eaecf7;\n  height: calc(100%);\n  border-top-left-radius: 15px;\n  border-top-right-radius: 15px;\n  margin-top: 10px;\n  position: absolute;\n}\n\n.examconductor-main-card .overall-time {\n  font-size: 0.9rem;\n}\n\n.examconductor-inner-card {\n  background: #fff;\n  border-radius: 10px;\n  margin-top: 10px;\n  height: calc(100vh - 180px);\n  width: 100%;\n}\n\n.examconductor-inner-card .header-box .time-spent p, .examconductor-inner-card .header-box .mark-review-btn ion-icon {\n  color: #393c40;\n  font-size: 0.9rem;\n  margin: 0.7rem auto;\n  font-weight: 600;\n}\n\n.examconductor-inner-card .header-box .mark-review-btn ion-icon {\n  font-size: 1.2rem;\n}\n\n.examconductor-inner-card .header-box .badge {\n  padding: 0.6rem;\n  width: 2.3rem;\n  height: 2.3rem;\n  background: gray;\n  vertical-align: middle;\n  border-radius: 50%;\n}\n\nion-toolbar {\n  --background: transparent;\n}\n\n.pause-button {\n  background: #393c40;\n  /* border-radius: 6px; */\n  padding: 0.1rem;\n  font-size: 0.7rem;\n  text-align: center;\n}\n\n.pause-button ion-icon {\n  color: white;\n}\n\n.prevbtn,\n.nextbtn {\n  color: white;\n  font-weight: 600;\n}\n\n.prevbtn {\n  --background: #828685;\n}\n\n.nextbtn {\n  --background: var(--ion-color-warning) ;\n}\n\n::ng-deep ion-slides {\n  border-radius: 10px;\n}\n\n.card > hr {\n  margin-right: 10px;\n  margin-left: 10px;\n}\n\nhr {\n  height: 1px;\n  border-width: 1px;\n  box-sizing: content-box;\n}\n\nhr {\n  margin-top: 0rem;\n  margin-bottom: 0rem;\n  border: 0;\n  border-top: 1px solid #e9ecef;\n}\n\n.question-line {\n  padding: 10px;\n  font-size: 12px;\n  font-weight: bold;\n  text-align: justify;\n}\n\n.optionlist ion-item {\n  --border-color: transparent;\n}\n\n/****sidemenu***/\n\n.panelbox .modal-dialog {\n  margin: 0;\n  position: fixed;\n  left: 0;\n  top: 0;\n}\n\n.panelbox .modal-dialog .modal-content {\n  border: 0;\n  border-top-left-radius: 10px;\n  border-top-right-radius: 0;\n  border-bottom-left-radius: 10px;\n  border-bottom-right-radius: 0;\n  width: 300px;\n  height: 97vh;\n  overflow: auto;\n  margin-top: 15px;\n}\n\n.panelbox .modal-dialog .modal-content .modal-header {\n  border-bottom: 0;\n  padding: 10px 16px;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  min-height: 56px;\n}\n\n.panelbox .modal-dialog .modal-content .modal-header .modal-title {\n  color: #6676df;\n  font-weight: 600;\n}\n\n.panelbox .modal-dialog .modal-content .modal-body {\n  padding: 10px 16px;\n}\n\n.panelbox .modal-dialog .modal-content .modal-body .status_box .list-group {\n  font-size: 0.8rem;\n}\n\n.panelbox.show .modal-dialog {\n  transform: translate(0, 0) !important;\n}\n\n.panelbox .modal-title {\n  margin-top: 0px;\n}\n\n.panelbox .list-group-flush > .list-group-item {\n  border-width: 0 0 1px;\n  background: #e9ecef;\n  margin-bottom: 7px;\n  border-radius: 5px;\n}\n\n.panelbox .list-group-flush > .list-group-item {\n  padding: 4px 8px;\n  border: 0;\n  color: #7f8284;\n  font-weight: 600;\n}\n\n.status_box {\n  border-bottom: 1px solid #a2a4a5;\n}\n\n.custom-badge {\n  height: 2rem;\n  width: 2rem;\n  border-radius: 5px;\n  padding: 0.6rem;\n  font-size: 0.7rem;\n}\n\n.badge-success {\n  background-color: #16B45F;\n  color: white;\n}\n\n.badge-danger {\n  background-color: #F2464C;\n  color: white;\n}\n\n.badge-warning {\n  background-color: #FFDE77;\n  color: white;\n}\n\n.badge-brown {\n  background-color: #F49251;\n  color: grey;\n}\n\n.badge-white {\n  background: linear-gradient(45deg, #fff 17%, #fff 70%);\n  color: #5a6872;\n}\n\n.panelbox-left .modal-dialog {\n  transform: translate(-100%, 0) !important;\n}\n\n.panelbox-right .modal-dialog {\n  transform: translate(100%, 0) !important;\n  left: auto;\n  right: 0;\n}\n\n.profileBox {\n  padding: 0 16px;\n  display: flex;\n  align-items: center;\n}\n\n.profileBox .image-wrapper {\n  margin-right: 16px;\n}\n\n.profileBox .in {\n  line-height: 1.4em;\n  padding-right: 25px;\n}\n\n.profileBox .in strong {\n  display: block;\n  font-weight: 600;\n  color: #393c40;\n}\n\n.profileBox .in .text-muted {\n  font-size: 14px;\n  color: gray !important;\n}\n\n.sidebar-balance {\n  padding: 6px 16px;\n  background: #fff;\n}\n\n.sidebar-balance .listview-title {\n  padding-right: 0;\n  padding-left: 0;\n  color: #FFF;\n  opacity: 0.6;\n}\n\n.sidebar-balance .amount {\n  font-weight: 600;\n  letter-spacing: -0.01em;\n  line-height: 1em;\n  color: #FFF;\n  margin-bottom: 6px;\n}\n\n.action-group {\n  display: flex;\n  align-items: flex-start;\n  justify-content: space-between;\n  padding: 0px 16px 10px 16px;\n  background: #fff;\n}\n\n.action-group .action-button {\n  padding: 10px 2px;\n  display: inline-flex;\n  align-items: center;\n  justify-content: center;\n  text-align: center;\n  font-size: 14px;\n  line-height: 1em;\n  color: rgba(255, 255, 255, 0.7);\n}\n\n.action-group .action-button .iconbox {\n  background: rgba(0, 0, 0, 0.3);\n  width: 38px;\n  height: 38px;\n  margin: 0 auto 8px auto;\n  border-radius: 100%;\n  font-size: 18px;\n  color: #FFF;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.action-group .action-button:last-child {\n  border-right: 0;\n}\n\n#sidebarPanel .modal-dialog {\n  position: fixed;\n  left: 0;\n  top: 0;\n  bottom: 0;\n}\n\n#sidebarPanel .modal-body {\n  margin-bottom: 34px;\n  padding-bottom: env(safe-area-inset-bottom) !important;\n  width: 300px;\n}\n\n.panelbox-left .listview > li,\n.panelbox-right .listview > li {\n  padding: 10px 16px;\n}\n\n.panelbox-left .link-listview > li,\n.panelbox-right .link-listview > li {\n  padding: 0;\n}\n\n.panelbox-left .link-listview > li a,\n.panelbox-right .link-listview > li a {\n  padding: 10px 36px 10px 16px;\n}\n\n.panelbox-left .image-listview > li,\n.panelbox-right .image-listview > li {\n  padding: 0;\n}\n\n.panelbox-left .image-listview > li .item,\n.panelbox-right .image-listview > li .item {\n  padding: 10px 16px;\n}\n\n.panelbox-left .image-listview > li a.item,\n.panelbox-right .image-listview > li a.item {\n  padding-right: 36px;\n}\n\n.sidebar-close {\n  position: absolute;\n  right: 10px;\n}\n\n.qnh-inner {\n  padding: 5px;\n  height: 50%;\n  overflow-y: auto;\n  position: absolute;\n}\n\n.questionList {\n  padding: 0px;\n}\n\n.ques_nav_btn {\n  border: 1px solid #393c40;\n  width: 15%;\n  height: 35px;\n  padding: 5px;\n  margin-left: 10px;\n  margin-top: 5px;\n  font-size: 14px;\n  font-weight: 600;\n}\n\n.radius-0 {\n  border-radius: 0 !important;\n}\n\nspan.badge-transparent {\n  background: transparent !important;\n}\n\n.ques_nav_btn ion-icon {\n  position: relative;\n  left: -10px;\n  top: -6px;\n  font-size: 18px;\n}\n\n.questionList .btn {\n  background-image: none;\n  border-radius: 4px;\n  cursor: pointer;\n  display: inline-block;\n  font-size: 14px;\n  font-weight: 400;\n  line-height: 1.42857;\n  margin-bottom: 5px;\n  margin-right: 0px;\n  padding: 4px 0px;\n  text-align: justify;\n  vertical-align: middle;\n  white-space: nowrap;\n  text-decoration: none;\n  position: relative;\n  z-index: 0;\n}\n\n.success {\n  background: #6c757d !important;\n  color: #FFF;\n}\n\n.success ion-icon {\n  opacity: 0;\n}\n\n.failed {\n  background-color: #f6c14a;\n  color: white;\n}\n\n.failed ion-icon {\n  opacity: 0;\n}\n\n.Mark_for_review {\n  border-color: #6c757d;\n  color: #0e0e0e;\n}\n\n.Marked_ans {\n  background: #6c757d;\n  color: white;\n}\n\n.qnol {\n  margin-left: -6px;\n}\n\nbutton.close {\n  padding: 0;\n  background-color: transparent;\n  border: 0;\n  outline: none;\n}\n\n.summary-header {\n  background: #6676df;\n  color: white;\n  padding: 0px 10px;\n  justify-content: center !important;\n}\n\n.summary-header h2 {\n  margin-bottom: 18px;\n  font-weight: 600;\n  line-height: 1.2;\n  font-size: 24px;\n}\n\n.summary-list li {\n  color: #a5a8ab;\n  font-weight: 700;\n  font-size: 18px;\n  border-bottom: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvZXhhbWNvbmR1Y3Rvci9leGFtY29uZHVjdG9yLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFnREE7RUFFSSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBaERKOztBQW1EQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQWhESjs7QUFpREk7RUFDRyxpQkFBQTtBQS9DUDs7QUFtREE7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSwyQkFBQTtFQUNBLFdBQUE7QUFoREo7O0FBa0RRO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQWhEWjs7QUFrRFE7RUFDSSxpQkFBQTtBQWhEWjs7QUFrRFE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUFoRFo7O0FBcURBO0VBQ0kseUJBQUE7QUFsREo7O0FBcURBO0VBQ0ksbUJBQUE7RUFDQSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBbERKOztBQXFEQTtFQUNJLFlBQUE7QUFsREo7O0FBcURBOztFQUVJLFlBQUE7RUFDQSxnQkFBQTtBQWxESjs7QUFxREE7RUFDSSxxQkFBQTtBQWxESjs7QUFzREE7RUFDSSx1Q0FBQTtBQW5ESjs7QUF1REk7RUFDSSxtQkFBQTtBQXBEUjs7QUF3REE7RUFDSSxrQkFBQTtFQUNBLGlCQUFBO0FBckRKOztBQXdEQTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FBckRKOztBQXdEQTtFQUNJLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxTQUFBO0VBQ0EsNkJBQUE7QUFyREo7O0FBd0RBO0VBQ0ksYUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBckRKOztBQXdEQTtFQUNJLDJCQUFBO0FBckRKOztBQXdEQSxnQkFBQTs7QUFFSTtFQUNJLFNBQUE7RUFDQSxlQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7QUF0RFI7O0FBd0RRO0VBQ0ksU0FBQTtFQUNBLDRCQUFBO0VBQ0EsMEJBQUE7RUFDQSwrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUF0RFo7O0FBMERZO0VBQ0ksZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsZ0JBQUE7QUF4RGhCOztBQTBEZ0I7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7QUF4RHBCOztBQTREWTtFQUNJLGtCQUFBO0FBMURoQjs7QUE0RG9CO0VBQ0ksaUJBQUE7QUExRHhCOztBQWlFSTtFQUNJLHFDQUFBO0FBL0RSOztBQW1FQTtFQUNJLGVBQUE7QUFoRUo7O0FBbUVBO0VBQ0kscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUFoRUo7O0FBbUVBO0VBQ0ksZ0JBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBaEVKOztBQW1FQTtFQUNJLGdDQUFBO0FBaEVKOztBQW1FQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUFoRUo7O0FBa0VBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0FBL0RKOztBQWlFQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtBQTlESjs7QUFnRUE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7QUE3REo7O0FBK0RBO0VBQ0kseUJBQUE7RUFDQSxXQUFBO0FBNURKOztBQThEQTtFQUVJLHNEQUFBO0VBQ0EsY0FBQTtBQTNESjs7QUE4REk7RUFDSSx5Q0FBQTtBQTNEUjs7QUFnRUk7RUFDSSx3Q0FBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FBN0RSOztBQWlFQTtFQUNJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUE5REo7O0FBZ0VJO0VBQ0ksa0JBQUE7QUE5RFI7O0FBaUVJO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtBQS9EUjs7QUFpRVE7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBL0RaOztBQWtFUTtFQUNJLGVBQUE7RUFDQSxzQkFBQTtBQWhFWjs7QUFxRUE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FBbEVKOztBQW9FSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBbEVSOztBQXFFSTtFQUNJLGdCQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQW5FUjs7QUF1RUE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSw4QkFBQTtFQUNBLDJCQUFBO0VBQ0EsZ0JBQUE7QUFwRUo7O0FBc0VJO0VBQ0ksaUJBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLCtCQUFBO0FBcEVSOztBQXNFUTtFQUNJLDhCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQXBFWjs7QUF1RVE7RUFDSSxlQUFBO0FBckVaOztBQTJFSTtFQUNJLGVBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtFQUNBLFNBQUE7QUF4RVI7O0FBMkVJO0VBQ0ksbUJBQUE7RUFDQSxzREFBQTtFQUNBLFlBQUE7QUF6RVI7O0FBZ0ZROztFQUNJLGtCQUFBO0FBNUVaOztBQWlGUTs7RUFDSSxVQUFBO0FBOUVaOztBQWdGWTs7RUFDSSw0QkFBQTtBQTdFaEI7O0FBbUZROztFQUNJLFVBQUE7QUFoRlo7O0FBa0ZZOztFQUNJLGtCQUFBO0FBL0VoQjs7QUFrRlk7O0VBQ0ksbUJBQUE7QUEvRWhCOztBQXFGQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtBQWxGSjs7QUFxRkE7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFsRko7O0FBc0ZBO0VBQ0ksWUFBQTtBQW5GSjs7QUFzRkE7RUFDSSx5QkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQW5GSjs7QUF3R0E7RUFDSSwyQkFBQTtBQXJHSjs7QUF3R0E7RUFDSSxrQ0FBQTtBQXJHSjs7QUFnSEE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQTdHSjs7QUFrSEE7RUFDSSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQS9HSjs7QUFtSEE7RUFDSSw4QkFBQTtFQUNBLFdBQUE7QUFoSEo7O0FBa0hJO0VBQ0ksVUFBQTtBQWhIUjs7QUFvSEE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7QUFqSEo7O0FBbUhJO0VBQ0ksVUFBQTtBQWpIUjs7QUFxSEE7RUFDSSxxQkFBQTtFQUNBLGNBQUE7QUFsSEo7O0FBcUhBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0FBbEhKOztBQXFIQTtFQUNJLGlCQUFBO0FBbEhKOztBQXFIQTtFQUNJLFVBQUE7RUFDQSw2QkFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0FBbEhKOztBQXFIQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUVBLGlCQUFBO0VBQ0Esa0NBQUE7QUFuSEo7O0FBc0hBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQW5ISjs7QUFzSEE7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUFuSEoiLCJmaWxlIjoic3JjL2FwcC9uRXhhbUhhbGwtQ2FuZGlkYXRlLU1vZHVsZS9leGFtY29uZHVjdG9yL2V4YW1jb25kdWN0b3IucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gLmV4YW0taGVhZGVye1xyXG4vLyAgICAgaGVpZ2h0OiAzMHZoO1xyXG4vLyAgICAgLmxlZnR7XHJcbi8vICAgICAgICAgYnV0dG9ue1xyXG4vLyAgICAgICAgICAgICBwYWRkaW5nOiAzcHg7XHJcbiAgICAgICAgICBcclxuLy8gICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4vLyAgICAgICAgICAgICB3aWR0aDogMS41cmVtO1xyXG4vLyAgICAgICAgICAgICBoZWlnaHQ6IDEuNXJlbTtcclxuLy8gICAgICAgICAgICAgZm9udC1zaXplOiAuOXJlbTtcclxuLy8gICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAzcHg7XHJcbi8vICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmYmY5Zjk7XHJcbi8vICAgICAgICAgICAgIGJveC1zaGFkb3c6IC0xcHggMXB4IDRweCAwIHJnYmEoMTE3LDEzOCwxNzIsLjEyKTtcclxuLy8gICAgICAgICB9XHJcbiAgICAgIFxyXG4vLyAgICAgfVxyXG4vLyAgICAgLnJpZ2h0e1xyXG4vLyAgICAgICAgIC5oZWFkZXJCdXR0b257XHJcbi8vICAgICAgICAgICAgIGZvbnQtc2l6ZTogMC45cmVtO1xyXG4vLyAgICAgICAgICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcclxuLy8gICAgICAgICAgICAgd2lkdGg6IDVyZW07XHJcbi8vICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDJyZW07XHJcbi8vICAgICAgICAgICAgIGJhY2tncm91bmQ6ICM4QThERUM7XHJcbi8vICAgICAgICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgICAgICAgICAgYm94LXNoYWRvdzogLTFweCAxcHggNHB4IDAgcmdiYSgxMTcsMTM4LDE3MiwuMTIpO1xyXG4vLyAgICAgICAgIH1cclxuICAgICAgIFxyXG4vLyAgICAgfVxyXG4vLyB9XHJcbi8vIC5jcm9zcy1oZWFkZXItYm9keXtcclxuLy8gICAgIHBhZGRpbmc6IDAuOXJlbTtcclxuLy8gICAgIC5sZWZ0e1xyXG4vLyAgICAgICAgIGJ1dHRvbntcclxuLy8gICAgICAgICAgICAgcGFkZGluZzogM3B4O1xyXG4vLyAgICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCB3aGl0ZTtcclxuLy8gICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4vLyAgICAgICAgICAgICB3aWR0aDogMS41cmVtO1xyXG4vLyAgICAgICAgICAgICBoZWlnaHQ6IDEuNXJlbTtcclxuLy8gICAgICAgICAgICAgZm9udC1zaXplOiAuOXJlbTtcclxuLy8gICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAzcHg7XHJcbi8vICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmYmY5Zjk7XHJcbi8vICAgICAgICAgfVxyXG4gICAgICBcclxuLy8gICAgIH1cclxuLy8gfVxyXG5cclxuXHJcblxyXG4uZXhhbWNvbmR1Y3Rvci1wYWdlIHtcclxuICAgLy8gYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gNTZweCk7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmV4YW1jb25kdWN0b3ItbWFpbi1jYXJkIHtcclxuICAgIGJhY2tncm91bmQ6ICNlYWVjZjc7XHJcbiAgICBoZWlnaHQ6IGNhbGMoMTAwJSk7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxNXB4O1xyXG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDE1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgLm92ZXJhbGwtdGltZXtcclxuICAgICAgIGZvbnQtc2l6ZTogMC45cmVtO1xyXG4gICAgfVxyXG59XHJcblxyXG4uZXhhbWNvbmR1Y3Rvci1pbm5lci1jYXJkIHtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIGhlaWdodDogY2FsYygxMDB2aCAtIDE4MHB4KTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgLmhlYWRlci1ib3h7XHJcbiAgICAgICAgLnRpbWUtc3BlbnQgcCwubWFyay1yZXZpZXctYnRuIGlvbi1pY29ue1xyXG4gICAgICAgICAgICBjb2xvcjojMzkzYzQwO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDAuOXJlbTtcclxuICAgICAgICAgICAgbWFyZ2luOiAwLjdyZW0gYXV0bztcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLm1hcmstcmV2aWV3LWJ0biBpb24taWNvbntcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5iYWRnZXtcclxuICAgICAgICAgICAgcGFkZGluZzogMC42cmVtO1xyXG4gICAgICAgICAgICB3aWR0aDogMi4zcmVtO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDIuM3JlbTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogZ3JheTtcclxuICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLnBhdXNlLWJ1dHRvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMzkzYzQwO1xyXG4gICAgLyogYm9yZGVyLXJhZGl1czogNnB4OyAqL1xyXG4gICAgcGFkZGluZzogMC4xcmVtO1xyXG4gICAgZm9udC1zaXplOiAwLjdyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5wYXVzZS1idXR0b24gaW9uLWljb24ge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4ucHJldmJ0bixcclxuLm5leHRidG4ge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG5cclxuLnByZXZidG4ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjODI4Njg1O1xyXG5cclxufVxyXG5cclxuLm5leHRidG4ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itd2FybmluZylcclxufVxyXG5cclxuOjpuZy1kZWVwIHtcclxuICAgIGlvbi1zbGlkZXMge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5jYXJkPmhyIHtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG59XHJcblxyXG5ociB7XHJcbiAgICBoZWlnaHQ6IDFweDtcclxuICAgIGJvcmRlci13aWR0aDogMXB4O1xyXG4gICAgYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbn1cclxuXHJcbmhyIHtcclxuICAgIG1hcmdpbi10b3A6IDByZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcmVtO1xyXG4gICAgYm9yZGVyOiAwO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlOWVjZWY7XHJcbn1cclxuXHJcbi5xdWVzdGlvbi1saW5lIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbn1cclxuXHJcbi5vcHRpb25saXN0IGlvbi1pdGVtIHtcclxuICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG5cclxuLyoqKipzaWRlbWVudSoqKi9cclxuLnBhbmVsYm94IHtcclxuICAgIC5tb2RhbC1kaWFsb2cge1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB0b3A6IDA7XHJcblxyXG4gICAgICAgIC5tb2RhbC1jb250ZW50IHtcclxuICAgICAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgICAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgICAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDA7XHJcbiAgICAgICAgICAgIHdpZHRoOiAzMDBweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiA5N3ZoO1xyXG4gICAgICAgICAgICBvdmVyZmxvdzogYXV0bztcclxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcclxuXHJcblxyXG5cclxuICAgICAgICAgICAgLm1vZGFsLWhlYWRlciB7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAwO1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgICAgICAgICBtaW4taGVpZ2h0OiA1NnB4O1xyXG5cclxuICAgICAgICAgICAgICAgIC5tb2RhbC10aXRsZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM2Njc2ZGY7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLm1vZGFsLWJvZHkge1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xyXG4gICAgICAgICAgICAgICAgLnN0YXR1c19ib3h7XHJcbiAgICAgICAgICAgICAgICAgICAgLmxpc3QtZ3JvdXB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMC44cmVtO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAmLnNob3cgLm1vZGFsLWRpYWxvZyB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMCwgMCkgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnBhbmVsYm94IC5tb2RhbC10aXRsZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbn1cclxuXHJcbi5wYW5lbGJveCAubGlzdC1ncm91cC1mbHVzaD4ubGlzdC1ncm91cC1pdGVtIHtcclxuICAgIGJvcmRlci13aWR0aDogMCAwIDFweDtcclxuICAgIGJhY2tncm91bmQ6ICNlOWVjZWY7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA3cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbi5wYW5lbGJveCAubGlzdC1ncm91cC1mbHVzaD4ubGlzdC1ncm91cC1pdGVtIHtcclxuICAgIHBhZGRpbmc6IDRweCA4cHg7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBjb2xvcjogIzdmODI4NDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbn1cclxuXHJcbi5zdGF0dXNfYm94IHtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYTJhNGE1O1xyXG59XHJcblxyXG4uY3VzdG9tLWJhZGdlIHtcclxuICAgIGhlaWdodDogMnJlbTtcclxuICAgIHdpZHRoOiAycmVtO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgcGFkZGluZzogMC42cmVtO1xyXG4gICAgZm9udC1zaXplOiAwLjdyZW07XHJcbn1cclxuLmJhZGdlLXN1Y2Nlc3Mge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzE2QjQ1RjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4uYmFkZ2UtZGFuZ2VyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMjQ2NEM7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLmJhZGdlLXdhcm5pbmcge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGREU3NztcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4uYmFkZ2UtYnJvd24ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0Y0OTI1MTtcclxuICAgIGNvbG9yOiBncmV5O1xyXG59XHJcbi5iYWRnZS13aGl0ZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiAtby1saW5lYXItZ3JhZGllbnQoNDVkZWcsICNmZmYgMTclLCAjZmZmIDcwJSk7XHJcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNmZmYgMTclLCAjZmZmIDcwJSk7XHJcbiAgICBjb2xvcjogIzVhNjg3MjtcclxufVxyXG4ucGFuZWxib3gtbGVmdCB7XHJcbiAgICAubW9kYWwtZGlhbG9nIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMTAwJSwgMCkgIWltcG9ydGFudDtcclxuICAgIH1cclxufVxyXG5cclxuLnBhbmVsYm94LXJpZ2h0IHtcclxuICAgIC5tb2RhbC1kaWFsb2cge1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDEwMCUsIDApICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgbGVmdDogYXV0bztcclxuICAgICAgICByaWdodDogMDtcclxuICAgIH1cclxufVxyXG5cclxuLnByb2ZpbGVCb3gge1xyXG4gICAgcGFkZGluZzogMCAxNnB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gICAgLmltYWdlLXdyYXBwZXIge1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMTZweDtcclxuICAgIH1cclxuXHJcbiAgICAuaW4ge1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjRlbTtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyNXB4O1xyXG5cclxuICAgICAgICBzdHJvbmcge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICAgICAgY29sb3I6ICMzOTNjNDA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAudGV4dC1tdXRlZCB7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgICAgICAgICAgY29sb3I6IGdyYXkgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5zaWRlYmFyLWJhbGFuY2Uge1xyXG4gICAgcGFkZGluZzogNnB4IDE2cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cclxuICAgIC5saXN0dmlldy10aXRsZSB7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICAgICAgY29sb3I6ICNGRkY7XHJcbiAgICAgICAgb3BhY2l0eTogLjY7XHJcbiAgICB9XHJcblxyXG4gICAgLmFtb3VudCB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICBsZXR0ZXItc3BhY2luZzogLTAuMDFlbTtcclxuICAgICAgICBsaW5lLWhlaWdodDogMWVtO1xyXG4gICAgICAgIGNvbG9yOiAjRkZGO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuICAgIH1cclxufVxyXG5cclxuLmFjdGlvbi1ncm91cCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICBwYWRkaW5nOiAwcHggMTZweCAxMHB4IDE2cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cclxuICAgIC5hY3Rpb24tYnV0dG9uIHtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4IDJweDtcclxuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDFlbTtcclxuICAgICAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAuNyk7XHJcblxyXG4gICAgICAgIC5pY29uYm94IHtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogcmdiYSgjMDAwLCAuMyk7XHJcbiAgICAgICAgICAgIHdpZHRoOiAzOHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDM4cHg7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMCBhdXRvIDhweCBhdXRvO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjRkZGO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgIGJvcmRlci1yaWdodDogMDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbiNzaWRlYmFyUGFuZWwge1xyXG4gICAgLm1vZGFsLWRpYWxvZyB7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgIH1cclxuXHJcbiAgICAubW9kYWwtYm9keSB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMzRweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogZW52KHNhZmUtYXJlYS1pbnNldC1ib3R0b20pICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgfVxyXG59XHJcblxyXG4ucGFuZWxib3gtbGVmdCxcclxuLnBhbmVsYm94LXJpZ2h0IHtcclxuICAgIC5saXN0dmlldyB7XHJcbiAgICAgICAgPmxpIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAubGluay1saXN0dmlldyB7XHJcbiAgICAgICAgPmxpIHtcclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuXHJcbiAgICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTBweCAzNnB4IDEwcHggMTZweDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuaW1hZ2UtbGlzdHZpZXcge1xyXG4gICAgICAgID5saSB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcblxyXG4gICAgICAgICAgICAuaXRlbSB7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxMHB4IDE2cHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGEuaXRlbSB7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAzNnB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uc2lkZWJhci1jbG9zZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMTBweDtcclxufVxyXG5cclxuLnFuaC1pbm5lciB7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDUwJTtcclxuICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcblxyXG59XHJcblxyXG4ucXVlc3Rpb25MaXN0IHtcclxuICAgIHBhZGRpbmc6IDBweDtcclxufVxyXG5cclxuLnF1ZXNfbmF2X2J0biB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMzkzYzQwO1xyXG4gICAgd2lkdGg6IDE1JTtcclxuICAgIGhlaWdodDogMzVweDtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLy8gLnF1ZXNfbmF2X2J0biAuYmFkZ2Uge1xyXG4vLyAgICAgYmFja2dyb3VuZC1jb2xvcjogIzc3NztcclxuLy8gICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbi8vICAgICBjb2xvcjogI2ZmZjtcclxuLy8gICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuLy8gICAgIGZvbnQtc2l6ZTogMTJweDtcclxuLy8gICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbi8vICAgICBsaW5lLWhlaWdodDogMTtcclxuLy8gICAgIG1pbi13aWR0aDogMTBweDtcclxuLy8gICAgIHBhZGRpbmc6IDNweCA3cHg7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4vLyAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuLy8gfVxyXG5cclxuLnJhZGl1cy0wIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuc3Bhbi5iYWRnZS10cmFuc3BhcmVudCB7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vLyAuYmFkZ2UuYmFkZ2UtY29ybmVyIHtcclxuLy8gICAgIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XHJcbi8vICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbi8vICAgICAvKiByaWdodDogODAlICFpbXBvcnRhbnQ7ICovXHJcbi8vICAgICB0b3A6IC03cHggIWltcG9ydGFudDtcclxuLy8gICAgIGxlZnQ6IC03cHg7XHJcbi8vIH1cclxuXHJcbi5xdWVzX25hdl9idG4gaW9uLWljb24ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogLTEwcHg7XHJcbiAgICB0b3A6IC02cHg7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcblxyXG5cclxuLnF1ZXN0aW9uTGlzdCAuYnRuIHtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNDI4NTc7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIHBhZGRpbmc6IDRweCAwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG5cclxufVxyXG5cclxuLnN1Y2Nlc3Mge1xyXG4gICAgYmFja2dyb3VuZDogIzZjNzU3ZCAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6ICNGRkY7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICAgIG9wYWNpdHk6IDA7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5mYWlsZWQge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y2YzE0YTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgICAgb3BhY2l0eTogMDtcclxuICAgIH1cclxufVxyXG5cclxuLk1hcmtfZm9yX3JldmlldyB7XHJcbiAgICBib3JkZXItY29sb3I6ICM2Yzc1N2Q7XHJcbiAgICBjb2xvcjogIzBlMGUwZTtcclxufVxyXG5cclxuLk1hcmtlZF9hbnMge1xyXG4gICAgYmFja2dyb3VuZDogIzZjNzU3ZDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLnFub2wge1xyXG4gICAgbWFyZ2luLWxlZnQ6IC02cHg7XHJcbn1cclxuXHJcbmJ1dHRvbi5jbG9zZSB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcblxyXG4uc3VtbWFyeS1oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZDogIzY2NzZkZjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuXHJcbiAgICBwYWRkaW5nOiAwcHggMTBweDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5zdW1tYXJ5LWhlYWRlciBoMiB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxOHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjI7XHJcbiAgICBmb250LXNpemU6IDI0cHg7XHJcbn1cclxuXHJcbi5zdW1tYXJ5LWxpc3QgbGkge1xyXG4gICAgY29sb3I6ICNhNWE4YWI7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcclxufVxyXG5cclxuLy8gLnF1ZXN0aW9uTGlzdCAuYnRuLXN1Y2Nlc3Mge1xyXG4vLyAgICAgYmFja2dyb3VuZC1jb2xvcjogIzVjYjg1YztcclxuLy8gICAgIGJvcmRlci1jb2xvcjogIzRjYWU0YztcclxuLy8gICAgIGNvbG9yOiAjZmZmO1xyXG4vLyB9XHJcblxyXG4vLyAucXVlc3Rpb25MaXN0IC5iYWRnZSB7XHJcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzc3O1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuLy8gICAgIGNvbG9yOiAjZmZmO1xyXG4vLyAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4vLyAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4vLyAgICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuLy8gICAgIGxpbmUtaGVpZ2h0OiAxO1xyXG4vLyAgICAgbWluLXdpZHRoOiAxMHB4O1xyXG4vLyAgICAgcGFkZGluZzogM3B4IDdweDtcclxuLy8gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuLy8gICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbi8vICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4vLyB9XHJcblxyXG4vLyAucmFkaXVzLTAge1xyXG4vLyAgICAgYm9yZGVyLXJhZGl1czogMCAhaW1wb3J0YW50O1xyXG4vLyB9XHJcblxyXG4vLyAucXVlc3Rpb25MaXN0IHNwYW4uYmFkZ2UtZGFyayB7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiAjNTU1ICFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuXHJcbi8vIC5xdWVzdGlvbkxpc3QgLmJhZGdlLmJhZGdlLWNvcm5lciB7XHJcbi8vICAgICBjb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xyXG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XHJcbi8vICAgICByaWdodDogLTZweCAhaW1wb3J0YW50O1xyXG4vLyAgICAgdG9wOiAtOHB4ICFpbXBvcnRhbnQ7XHJcbi8vIH0iXX0= */";
      /***/
    },

    /***/
    "uipB":
    /*!**********************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examconductor/examconductor.module.ts ***!
      \**********************************************************************************/

    /*! exports provided: ExamconductorPageModule */

    /***/
    function uipB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExamconductorPageModule", function () {
        return ExamconductorPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _examconductor_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./examconductor-routing.module */
      "yvUt");
      /* harmony import */


      var _examconductor_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./examconductor.page */
      "DYKa");

      var ExamconductorPageModule = function ExamconductorPageModule() {
        _classCallCheck(this, ExamconductorPageModule);
      };

      ExamconductorPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _examconductor_routing_module__WEBPACK_IMPORTED_MODULE_5__["ExamconductorPageRoutingModule"]],
        declarations: [_examconductor_page__WEBPACK_IMPORTED_MODULE_6__["ExamconductorPage"]]
      })], ExamconductorPageModule);
      /***/
    },

    /***/
    "yvUt":
    /*!******************************************************************************************!*\
      !*** ./src/app/nExamHall-Candidate-Module/examconductor/examconductor-routing.module.ts ***!
      \******************************************************************************************/

    /*! exports provided: ExamconductorPageRoutingModule */

    /***/
    function yvUt(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ExamconductorPageRoutingModule", function () {
        return ExamconductorPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _examconductor_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./examconductor.page */
      "DYKa");

      var routes = [{
        path: '',
        component: _examconductor_page__WEBPACK_IMPORTED_MODULE_3__["ExamconductorPage"]
      }];

      var ExamconductorPageRoutingModule = function ExamconductorPageRoutingModule() {
        _classCallCheck(this, ExamconductorPageRoutingModule);
      };

      ExamconductorPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ExamconductorPageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=examconductor-examconductor-module-es5.js.map