(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"], {
    /***/
    "6UR5":
    /*!**************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Core-Pages/login/login.page.html ***!
      \**************************************************************************************************/

    /*! exports provided: default */

    /***/
    function UR5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"container login-form\">\n    <ion-toolbar>\n      <ion-buttons slot=\"start\" >\n        \n        <ion-back-button [defaultHref]=\"defaultHref\">\n\n        </ion-back-button>\n        \n      </ion-buttons>\n      <ion-buttons slot=\"secondary\">\n        <ion-button>\n          <ion-icon slot=\"icon-only\" [hidden]=\"true\" name=\"star\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n      <ion-title class=\"text-center\">Login</ion-title>\n    </ion-toolbar>\n\n    <div class=\"login-logo\">\n      <img [src]=\"instituteLogo\" alt=\"Ionic logo\" />\n    </div>\n\n    <form #loginForm=\"ngForm\" novalidate>\n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"loginId\">Email address</label>\n        <input\n        \n          id=\"loginId\"\n          type=\"email\"\n          [(ngModel)]=\"loginData.loginId\"\n          name=\"loginId\"\n          placeholder=\"Enter your email\"\n          class=\"form-control bg-white  border-md\"\n         \n        required/>\n      \n      </div>\n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"password\">password</label>\n        <input\n          id=\"password\"\n          type=\"password\"\n          [(ngModel)]=\"loginData.password\"\n          name=\"password\"\n          placeholder=\"Enter your password\"\n          class=\"form-control bg-white  border-md\"\n          required />\n      </div>\n      <div class=\"form-group col-lg-6 d-flex justify-content-end\">\n        <a (click)=\"goToForgotPwd()\" class=\"text-small forgot-password text-black\">Forgot Password</a>\n      </div>\n      <!-- Submit Button -->\n      <div class=\"form-group col-lg-12 mx-auto mb-0\">\n        <button\n          class=\"btn btn-primary btn-block py-2 text-white\"\n          (click)=\"onLogin(loginForm)\"\n        >\n          <span class=\"font-weight-normal \">Login</span>\n        </button>\n      </div>\n\n      <!-- Divider Text -->\n      <div class=\"\" *ngIf=\"publicsupport\">\n        <div class=\"form-group col-lg-12 mx-auto d-flex align-items-center my-2\">\n          <div class=\"border-bottom w-100 ml-5\"></div>\n          <span class=\"px-2 small text-muted font-weight-bold text-muted\"\n            >OR</span\n          >\n          <div class=\"border-bottom w-100 mr-5\"></div>\n        </div>\n  \n        <!-- Social Login -->\n        <div class=\"flex-c p-b-112\" >\n          <button class=\"login100-social-item\">\n            <i class=\"fa fa-facebook-f\"></i>\n          </button>\n  \n          <button  class=\"login100-social-item\">\n            <img src=\"/assets/images/icons/icon-google.png\" alt=\"GOOGLE\">\n          </button>\n        </div>\n        <!--<div class=\"form-group col-lg-12 mx-auto\" *ngIf=\"publicsupport\">\n          <a href=\"#\" class=\"btn btn-primary btn-block py-2 btn-facebook\">\n            <i class=\"fa fa-facebook-f mr-2\"></i>\n            <span class=\"font-weight-bold\">Login with Facebook</span>\n          </a>\n          <a href=\"#\" class=\"btn btn-primary btn-block py-2 btn-google\">\n            <i class=\"fa fa-google mr-2\"></i>\n            <span class=\"font-weight-bold\">Login with Gmail</span>\n          </a>\n        </div>-->\n  \n        <!-- Already Registered -->\n        <div class=\"text-center w-100 mt-2\" >\n          <p class=\"text-muted font-weight-bold\">\n            Don't have an account?\n            <span href=\"\" (click)=\"onSignup()\" class=\"text-primary ml-2\"\n              >Sign Up</span\n            >\n          </p>\n        </div>\n      </div>\n     \n    </form>\n  </div>\n</ion-content>\n ";
      /***/
    },

    /***/
    "eoOW":
    /*!********************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/login/login-routing.module.ts ***!
      \********************************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function eoOW(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "jqas");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "jqas":
    /*!**********************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/login/login.page.ts ***!
      \**********************************************************/

    /*! exports provided: LoginPage */

    /***/
    function jqas(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./login.page.html */
      "6UR5");
      /* harmony import */


      var _login_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./login.page.scss */
      "pB17");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _nExamHall_Services_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../nExamHall-Services/toast.service */
      "RWSw");
      /* harmony import */


      var _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../nExamHall-Services/api.service */
      "xHtS");
      /* harmony import */


      var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../nExamHall-Services/local-storage.service */
      "2eCX");
      /* harmony import */


      var _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../nExamHall-Services/common-services.service */
      "FE8S");
      /* harmony import */


      var _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../nExamHall-Services/loader.service */
      "9mvZ");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(router, apiService, storage, toast, commonService, loader, platform) {
          _classCallCheck(this, LoginPage);

          this.router = router;
          this.apiService = apiService;
          this.storage = storage;
          this.toast = toast;
          this.commonService = commonService;
          this.loader = loader;
          this.platform = platform;
          this.loginData = {
            instituteuserid: "",
            loginId: "",
            password: "",
            subdomain: ""
          };
          this.submitted = false;
          this.defaultHref = "";
          this.enableOtpScreen = true;

          this.goToForgotPwd = function () {
            this.routeProvider('otp');
          }; // this.platform.backButton.subscribeWithPriority(10, () => {
          //   console.log('Handler was called!');
          // });

        }

        _createClass(LoginPage, [{
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this = this;

            this.loader.showImgLoader();
            this.storage.getObject('instituteData').then(function (data) {
              debugger;
              _this.instituteData = data;
              _this.instituteLogo = _this.commonService.addImageUrl(_this.instituteData.userimage, "", "");
              _this.loginData.instituteuserid = _this.instituteData.instituteuserid;
              _this.loginData.subdomain = _this.instituteData.subdomain;
              _this.publicsupport = _this.instituteData.features.publicsupport;
              console.log('instdata', _this.instituteData);

              if (_this.instituteData.features.mailotp == false && _this.instituteData.features.mobileotp == false) {
                _this.enableOtpScreen = false;
              }

              _this.loader.hideLoader();
            });
          }
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.defaultHref = '/nexamhallcore/auth';
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "routeProvider",
          value: function routeProvider(value) {
            var path = this.commonService.pathService(value);
            this.router.navigateByUrl(path);
          }
        }, {
          key: "onSignup",
          value: function onSignup() {
            this.routeProvider('signup');
          }
        }, {
          key: "directLogin",
          value: function directLogin() {
            var _this2 = this;

            this.storage.getObject('instituteData').then(function (res) {
              var directLoginData = res;

              if (_this2.enableOtpScreen) {
                var otpdata = {
                  loginId: directLoginData.loginId.toLowerCase(),
                  instituteuserid: _this2.instituteData.instituteuserid
                };

                _this2.storage.setObject('otpData', otpdata).then(function (res) {
                  _this2.apiService.sendAuthOTP(otpdata);

                  _this2.routeProvider('otp');
                });
              } else {
                _this2.loginData.loginId = directLoginData.loginId.toLowerCase();
                _this2.loginData.password = directLoginData.password;
                _this2.loginData.instituteuserid = _this2.instituteData.instituteuserid;
                _this2.loginData.subdomain = _this2.instituteData.subdomain;

                _this2.loginAuth();
              }
            });
          }
        }, {
          key: "loginAuth",
          value: function loginAuth() {
            var _this3 = this;

            this.apiService.loginauth(this.loginData).subscribe(function (response) {
              _this3.loader.showImgLoader();

              console.log(response);

              if (response == null || response == undefined) {
                var errormsg = "Connectivity problem, please try later";

                _this3.toast.showToast(errormsg, 2000, 'top', 'danger');
              } else {
                if (response["statuscode"] == "NT-200") {
                  _this3.submitted = true;
                  var responseLogindata = response["userdata"];

                  _this3.storage.set('token', responseLogindata.token);

                  if (responseLogindata.status == 1) {
                    switch (responseLogindata.userType) {
                      case "1" || false:
                        _this3.toast.showToast('Available  candidates only', 2000, 'top', 'danger');

                        break;

                      case "2":
                        _this3.toast.showToast(response["message"], 2000, 'top', 'success');

                        _this3.storage.setObject('candidateUserdata', responseLogindata);

                        _this3.storage.set('token', responseLogindata["token"]);

                        _this3.routeProvider('candidatedashboard'); //this.router.navigateByUrl("/nexamhallcandidate/dashboard");


                        break;
                      // case "3":
                      //   this.toast.showToast('Available  candidates only', 2000, 'top', 'danger')
                      //   break;

                      default:
                    }
                  }
                } else {
                  var statuscode = response["statuscode"];

                  switch (statuscode) {
                    case 'NT-202':
                      _this3.errormsg = "Email Id not registered";

                      _this3.toast.showToast(_this3.errormsg, 2000, 'top', 'danger');

                      break;

                    case 'NT-404':
                      _this3.errormsg = "Password Incorrect";

                      _this3.toast.showToast(_this3.errormsg, 2000, 'top', 'danger');

                      break;

                    case 'NT-203':
                      _this3.errormsg = "Please Check your Mail to Activate your Account";

                      _this3.toast.showToast(_this3.errormsg, 2000, 'top', 'danger'); // $scope.directLogin();


                      break;

                    case 'NT-208':
                      _this3.errormsg = response["message"];

                      _this3.toast.showToast(_this3.errormsg, 2000, 'top', 'danger');

                      break;

                    case 'NT-212':
                      _this3.errormsg = response["message"];

                      _this3.toast.showToast(_this3.errormsg, 2000, 'top', 'danger');

                      break;

                    default:
                  }
                }
              }

              _this3.loader.hideLoader(); //this.router.navigateByUrl("/app/tabs/home");

            });
          }
        }, {
          key: "onLogin",
          value: function onLogin(form) {
            if (form.valid) {
              if (!this.commonService.checkEmail(this.loginData.loginId)) {
                var errormsg = "Please enter valid email";
                this.toast.showToast(errormsg, 2000, 'top', 'danger');
              } else {
                this.loginAuth();
              }
            } else {
              alert('error');
            }
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"]
        }, {
          type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"]
        }, {
          type: _nExamHall_Services_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"]
        }, {
          type: _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_8__["CommonServicesService"]
        }, {
          type: _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["Platform"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], LoginPage);
      /***/
    },

    /***/
    "pB17":
    /*!************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/login/login.page.scss ***!
      \************************************************************/

    /*! exports provided: default */

    /***/
    function pB17(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".login-form p {\n  font-size: 80%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNvcmUtUGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksY0FBQTtBQUFSIiwiZmlsZSI6InNyYy9hcHAvbkV4YW1IYWxsLUNvcmUtUGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luLWZvcm17XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogODAlO1xyXG4gICAgfVxyXG59Il19 */";
      /***/
    },

    /***/
    "zoVK":
    /*!************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/login/login.module.ts ***!
      \************************************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function zoVK(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "eoOW");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "jqas");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-login-module-es5.js.map