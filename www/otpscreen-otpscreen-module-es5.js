(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["otpscreen-otpscreen-module"], {
    /***/
    "3QgR":
    /*!********************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/otpscreen/otpscreen.module.ts ***!
      \********************************************************************/

    /*! exports provided: OtpscreenPageModule */

    /***/
    function QgR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OtpscreenPageModule", function () {
        return OtpscreenPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _otpscreen_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./otpscreen-routing.module */
      "fLfs");
      /* harmony import */


      var _otpscreen_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./otpscreen.page */
      "CnTH");

      var OtpscreenPageModule = function OtpscreenPageModule() {
        _classCallCheck(this, OtpscreenPageModule);
      };

      OtpscreenPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _otpscreen_routing_module__WEBPACK_IMPORTED_MODULE_5__["OtpscreenPageRoutingModule"]],
        declarations: [_otpscreen_page__WEBPACK_IMPORTED_MODULE_6__["OtpscreenPage"]]
      })], OtpscreenPageModule);
      /***/
    },

    /***/
    "CnTH":
    /*!******************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/otpscreen/otpscreen.page.ts ***!
      \******************************************************************/

    /*! exports provided: OtpscreenPage */

    /***/
    function CnTH(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OtpscreenPage", function () {
        return OtpscreenPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_otpscreen_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./otpscreen.page.html */
      "Id9P");
      /* harmony import */


      var _otpscreen_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./otpscreen.page.scss */
      "rCNz");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var OtpscreenPage = /*#__PURE__*/function () {
        function OtpscreenPage() {
          _classCallCheck(this, OtpscreenPage);

          this.defaultHref = '';
          this.Pin = "";
          this.ShowPin = false;
        }

        _createClass(OtpscreenPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.defaultHref = '/nexamhallcore/auth/login';
          }
        }, {
          key: "eventCapture",
          value: function eventCapture(event) {
            this.ShowPin = false;
            this.Pin = event;
          }
        }, {
          key: "showPin",
          value: function showPin() {
            this.ShowPin = !this.ShowPin;
          }
        }]);

        return OtpscreenPage;
      }();

      OtpscreenPage.ctorParameters = function () {
        return [];
      };

      OtpscreenPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-otpscreen',
        template: _raw_loader_otpscreen_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_otpscreen_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], OtpscreenPage);
      /***/
    },

    /***/
    "Id9P":
    /*!**********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Core-Pages/otpscreen/otpscreen.page.html ***!
      \**********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function Id9P(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\" >\n      \n      <ion-back-button [defaultHref]=\"defaultHref\">\n\n      </ion-back-button>\n      \n    </ion-buttons>\n    <ion-buttons slot=\"secondary\">\n      <ion-button>\n        <ion-icon slot=\"icon-only\" [hidden]=\"true\" name=\"star\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"text-center\">OTP Verification</ion-title>\n  </ion-toolbar>\n</ion-header>\n<!-- <ion-content>\n  <div class=\"container\">\n    <img\n    src=\"assets/images/otpscreen.png\"\n    class=\"slide-image\"\n  />\n  <p>\n    simple and effective. Students get results instantly and compare with others.\n  </p>\n    <form #forgotForm=\"ngForm\" novalidate>\n      \n      <div class=\"form-group col-lg-6 mb-3\">\n        <label for=\"email\">Email address</label>\n        <input\n          id=\"email\"\n          type=\"text\"\n          [(ngModel)]=\"loginId\"\n          name=\"email\"\n          placeholder=\"Enter your email\"\n          class=\"form-control bg-white  border-md\"\n        />\n      </div>\n    \n      <div class=\"form-group col-lg-12 mx-auto mb-0\">\n        <button\n          class=\"btn btn-primary btn-block py-2 text-white\"\n          (click)=\"onLogin(loginForm)\"\n        >\n          <span class=\"font-weight-normal \">Reset password</span>\n        </button>\n      </div>\n\n    \n     \n    </form>\n  </div>\n</ion-content> -->\n<ion-content padding>\n  <div text-center *ngIf=\"!ShowPin\">\n     <button ion-button clear large (click)=\"showPin()\">Enter Pin</button>\n     <br>\n     <label *ngIf=\"Pin\">Pin entered is {{Pin}}</label>\n  </div>\n \n<app-otp-pin-screen  pagetitle=\"Enter The Pin\" (change)=\"eventCapture($event)\"></app-otp-pin-screen>\n</ion-content>\n ";
      /***/
    },

    /***/
    "fLfs":
    /*!****************************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/otpscreen/otpscreen-routing.module.ts ***!
      \****************************************************************************/

    /*! exports provided: OtpscreenPageRoutingModule */

    /***/
    function fLfs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OtpscreenPageRoutingModule", function () {
        return OtpscreenPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _otpscreen_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./otpscreen.page */
      "CnTH");

      var routes = [{
        path: '',
        component: _otpscreen_page__WEBPACK_IMPORTED_MODULE_3__["OtpscreenPage"]
      }];

      var OtpscreenPageRoutingModule = function OtpscreenPageRoutingModule() {
        _classCallCheck(this, OtpscreenPageRoutingModule);
      };

      OtpscreenPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OtpscreenPageRoutingModule);
      /***/
    },

    /***/
    "rCNz":
    /*!********************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/otpscreen/otpscreen.page.scss ***!
      \********************************************************************/

    /*! exports provided: default */

    /***/
    function rCNz(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1Db3JlLVBhZ2VzL290cHNjcmVlbi9vdHBzY3JlZW4ucGFnZS5zY3NzIn0= */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=otpscreen-otpscreen-module-es5.js.map