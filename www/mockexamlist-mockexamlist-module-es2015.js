(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["mockexamlist-mockexamlist-module"],{

/***/ "FtVU":
/*!****************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/mockexamlist/mockexamlist-routing.module.ts ***!
  \****************************************************************************************/
/*! exports provided: MockexamlistPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MockexamlistPageRoutingModule", function() { return MockexamlistPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _mockexamlist_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mockexamlist.page */ "zAbB");




const routes = [
    {
        path: '',
        component: _mockexamlist_page__WEBPACK_IMPORTED_MODULE_3__["MockexamlistPage"]
    }
];
let MockexamlistPageRoutingModule = class MockexamlistPageRoutingModule {
};
MockexamlistPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MockexamlistPageRoutingModule);



/***/ }),

/***/ "L8Rz":
/*!**********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/mockexamlist/mockexamlist.page.html ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-header class=\"ion-no-border\">\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n    \n      <ion-back-button [defaultHref]=\"defaultHref\"  ></ion-back-button>\n    </div>\n    <div class=\"pageTitle\">\n      {{examname}}\n    </div>\n    <div class=\"right\">\n        <button  class=\"headerButton\" (click)=\"presentFilter()\">\n            <!-- <ion-icon  name=\"notifications-outline\"></ion-icon> -->\n            <ion-icon  class=\"icon\" name=\"funnel-outline\"></ion-icon>\n            <span class=\"badge badge-danger\"></span>\n        </button>\n    </div>\n</div>\n</ion-header>\n\n<ion-content>\n  \n<div class=\"main\">\n  <div class=\"section wallet-card-section pt-1\">\n    <div class=\"section-header\">\n      <ion-searchbar\n        mode=\"ios\"\n        [(ngModel)]=\"queryText\"\n        (ionChange)=\"onSearchTerm($event)\"\n        placeholder=\"Search Exam\"\n      ></ion-searchbar>\n    </div>\n  </div>\n  <ion-grid class=\"my-3\" fixed>\n    <ion-row *ngIf=\"mockexamdetails.length > 0; else noItems\">\n      <ion-col size=\"6\" size-md=\"6\" *ngFor=\"let mock of mockexamdetails\">\n        <div\n          class=\"card box-shadow mock-card\"\n          (click)=\"intructpage(mock._id,mock.examstatus,mock.examname,mock.examseq,mock.langsupport)\"\n        >\n          <span\n            class=\"ui-card__badge badge badge-pill badge-white badge-trending text-{{mock.buttoncolor}}\"\n            >{{mock.buttonText}}</span\n          >\n          <div class=\"card-body\">\n            <h5 class=\"card-title\">{{mock.mockname}}</h5>\n            <p class=\"card-text\">Expires on {{mock.endDate | date}}</p>\n          </div>\n\n          <div class=\"card-body custom-card-body\">\n           \n\n            <ul class=\"list-group\">\n              <li\n                class=\"list-group-item d-flex justify-content-between align-items-center\"\n              >\n                <span class=\"badge badge-primary badge-pill\"\n                  >{{mock.questioncount}}</span\n                >\n                Questions\n              </li>\n              <li\n                class=\"list-group-item d-flex justify-content-between align-items-center\"\n              >\n                <span class=\"badge badge-primary badge-pill\"\n                  >{{mock.timeduration}}</span\n                >\n                Minutes\n              </li>\n            </ul>\n         \n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ng-template #noItems>\n      <ion-item> No items found. </ion-item>\n    </ng-template>\n  </ion-grid>\n \n</div>\n\n</ion-content>\n");

/***/ }),

/***/ "Q4vv":
/*!********************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/mockexamlist/mockexamlist.page.scss ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-col:not(:last-of-type) .mock-card {\n  margin-bottom: 0;\n}\n\n.ios ion-list {\n  margin-bottom: 10px;\n}\n\n.md ion-list {\n  border-top: 1px solid var(--ion-color-step-150, #d7d8da);\n  padding: 0;\n}\n\n.mock-card {\n  border-radius: 10px;\n}\n\n.mock-card ion-card-header {\n  padding: 0;\n}\n\n.mock-card ion-card-content {\n  flex: 1 1 auto;\n  padding: 0;\n}\n\n.mock-card .card-body {\n  padding: 4px 10px 10px 10px;\n}\n\n.mock-card .card-body .card-title {\n  font-size: 0.9rem;\n  color: #4c4a4a;\n  font-weight: 600;\n}\n\n.mock-card .card-body .card-text {\n  font-size: 0.7rem;\n  color: #a3a4ab;\n}\n\n.mock-card .custom-card-body {\n  border-radius: 10px;\n  background: #eef0fb;\n  padding: 4px 10px 10px 10px;\n}\n\n.mock-card .custom-card-body .list-group-item {\n  padding: 0.3rem;\n  background: transparent;\n  border: none;\n}\n\n.mock-card .custom-card-body .list-group-item .badge-primary {\n  height: 40px;\n  width: 40px;\n  padding: 14px;\n  background: #3b5065;\n}\n\n.mock-card .mock-card-item {\n  --min-height: 85px;\n}\n\n.mock-card .mock-card-item h2 {\n  font-size: 18px;\n  font-weight: 500;\n  letter-spacing: 0.02em;\n}\n\n.mock-card .mock-card-item p {\n  font-size: 13px;\n  letter-spacing: 0.02em;\n}\n\n.badge-trending {\n  position: relative;\n  padding: 5px 0px 0 0px;\n  color: #5a6872;\n  border-radius: 5px;\n  display: inline-block;\n  height: 20px;\n  font-size: 1rem;\n  font-weight: 600;\n}\n\nion-searchbar .searchbar-input.sc-ion-searchbar-ios {\n  background: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvbW9ja2V4YW1saXN0L21vY2tleGFtbGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKOztBQUlBO0VBQ0ksbUJBQUE7QUFESjs7QUFJQTtFQUNJLHdEQUFBO0VBRUEsVUFBQTtBQUZKOztBQWFBO0VBQ0ksbUJBQUE7QUFWSjs7QUFXSTtFQUNJLFVBQUE7QUFUUjs7QUFXSTtFQUNJLGNBQUE7RUFFQSxVQUFBO0FBVlI7O0FBWUk7RUFDSSwyQkFBQTtBQVZSOztBQVdRO0VBQ0ksaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUFUWjs7QUFXUTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtBQVRaOztBQWFJO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0FBWFI7O0FBYVE7RUFDSSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0FBWFo7O0FBWVk7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtBQVZoQjs7QUErQkk7RUFDSSxrQkFBQTtBQTdCUjs7QUE4QlE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQTVCWjs7QUE4QlE7RUFDSSxlQUFBO0VBQ0Esc0JBQUE7QUE1Qlo7O0FBcUNBO0VBQ0ksa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQWxDSjs7QUF1Q1E7RUFDSSxrQ0FBQTtBQXBDWiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL21vY2tleGFtbGlzdC9tb2NrZXhhbWxpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbDpub3QoOmxhc3Qtb2YtdHlwZSkgLm1vY2stY2FyZCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xyXG59XHJcblxyXG5cclxuXHJcbi5pb3MgaW9uLWxpc3Qge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuLm1kIGlvbi1saXN0IHtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3Itc3RlcC0xNTAsICNkN2Q4ZGEpO1xyXG5cclxuICAgIHBhZGRpbmc6IDA7XHJcbn1cclxuXHJcblxyXG5cclxuLy8gLm1vY2stY2FyZCB7XHJcbi8vICAgICAvLyBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4vLyAgICAgLy8gYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XHJcbiAgXHJcbi8vIH1cclxuXHJcbi5tb2NrLWNhcmQge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIGlvbi1jYXJkLWhlYWRlciB7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICAgIGlvbi1jYXJkLWNvbnRlbnQge1xyXG4gICAgICAgIGZsZXg6IDEgMSBhdXRvO1xyXG4gICAgXHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgIH1cclxuICAgIC5jYXJkLWJvZHl7XHJcbiAgICAgICAgcGFkZGluZzogNHB4IDEwcHggMTBweCAxMHB4O1xyXG4gICAgICAgIC5jYXJkLXRpdGxlIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAwLjlyZW07XHJcbiAgICAgICAgICAgIGNvbG9yOiAjNGM0YTRhO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY2FyZC10ZXh0IHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAwLjdyZW07XHJcbiAgICAgICAgICAgIGNvbG9yOiAjYTNhNGFiO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgXHJcbiAgICAuY3VzdG9tLWNhcmQtYm9keXtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNlZWYwZmI7XHJcbiAgICAgICAgcGFkZGluZzogNHB4IDEwcHggMTBweCAxMHB4O1xyXG4gICAgICAgIFxyXG4gICAgICAgIC5saXN0LWdyb3VwLWl0ZW17XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDAuM3JlbTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgLmJhZGdlLXByaW1hcnkge1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDQwcHg7XHJcbiAgICAgICAgICAgICAgICBwYWRkaW5nOiAxNHB4O1xyXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzNiNTA2NTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIC5jdXN0b20tY2FyZC1ib2R5IHtcclxuICAgIC8vICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgLy8gICAgIGJhY2tncm91bmQ6ICNlZWYwZmI7XHJcbiAgICAvLyAgIC8vICBwYWRkaW5nOiA1cHg7XHJcbiAgICAvLyB9XHJcbiAgICBcclxuICAgIC8vIC5jdXN0b20tY2FyZC1ib2R5IC5saXN0LWdyb3VwLWl0ZW0ge1xyXG4gICAgLy8gICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLy8gICAgIGJvcmRlcjogbm9uZTtcclxuICAgIC8vIH1cclxuICAgIFxyXG4gICAgLy8gLmN1c3RvbS1jYXJkLWJvZHkgLmxpc3QtZ3JvdXAtaXRlbSAuYmFkZ2UtcHJpbWFyeSB7XHJcbiAgICAvLyAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgLy8gICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgLy8gICAgIHBhZGRpbmc6IDE0cHg7XHJcbiAgICAvLyAgICAgYmFja2dyb3VuZDogIzNiNTA2NTtcclxuICAgIC8vIH1cclxuICAgIC5tb2NrLWNhcmQtaXRlbSB7XHJcbiAgICAgICAgLS1taW4taGVpZ2h0OiA4NXB4O1xyXG4gICAgICAgIGgyIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4wMmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBwIHtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC4wMmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgIH1cclxuICAgIFxyXG59XHJcblxyXG5cclxuXHJcbi5iYWRnZS10cmVuZGluZyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBwYWRkaW5nOiA1cHggMHB4IDAgMHB4O1xyXG4gICAgY29sb3I6ICM1YTY4NzI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG5cclxufVxyXG5cclxuICAgIGlvbi1zZWFyY2hiYXIge1xyXG4gICAgICAgIC5zZWFyY2hiYXItaW5wdXQuc2MtaW9uLXNlYXJjaGJhci1pb3N7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQ6dHJhbnNwYXJlbnQgIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcbiAgICB9Il19 */");

/***/ }),

/***/ "xZir":
/*!********************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/mockexamlist/mockexamlist.module.ts ***!
  \********************************************************************************/
/*! exports provided: MockexamlistPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MockexamlistPageModule", function() { return MockexamlistPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _mockexamlist_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mockexamlist-routing.module */ "FtVU");
/* harmony import */ var _mockexamlist_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mockexamlist.page */ "zAbB");







let MockexamlistPageModule = class MockexamlistPageModule {
};
MockexamlistPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _mockexamlist_routing_module__WEBPACK_IMPORTED_MODULE_5__["MockexamlistPageRoutingModule"]
        ],
        declarations: [_mockexamlist_page__WEBPACK_IMPORTED_MODULE_6__["MockexamlistPage"]]
    })
], MockexamlistPageModule);



/***/ }),

/***/ "zAbB":
/*!******************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/mockexamlist/mockexamlist.page.ts ***!
  \******************************************************************************/
/*! exports provided: MockexamlistPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MockexamlistPage", function() { return MockexamlistPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_mockexamlist_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./mockexamlist.page.html */ "L8Rz");
/* harmony import */ var _mockexamlist_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mockexamlist.page.scss */ "Q4vv");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../nExamHall-Services/api.service */ "xHtS");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../nExamHall-Services/local-storage.service */ "2eCX");
/* harmony import */ var _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../nExamHall-Services/loader.service */ "9mvZ");
/* harmony import */ var _nExamHall_Custom_Components_mockfilter_modal_mockfilter_modal_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../nExamHall-Custom-Components/mockfilter-modal/mockfilter-modal.component */ "5IGy");
/* harmony import */ var _nExamHall_Services_alert_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../nExamHall-Services/alert.service */ "bFVc");
/* harmony import */ var _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../nExamHall-Services/common-services.service */ "FE8S");












let MockexamlistPage = class MockexamlistPage {
    constructor(loader, modalCtrl, router, routerOutlet, route, apiService, localStorage, nav, alertService, commonService) {
        this.loader = loader;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.routerOutlet = routerOutlet;
        this.route = route;
        this.apiService = apiService;
        this.localStorage = localStorage;
        this.nav = nav;
        this.alertService = alertService;
        this.commonService = commonService;
        this.defaultHref = "";
        this.mockexamdetails = [];
        this.allmockExams = [];
        this.excludedFilter = [];
    }
    ngOnInit() {
    }
    constructMockList(data) {
        debugger;
        this.student_map = data[0];
        this.mockexams = this.student_map.mockexams;
        // console.log(this.student_map)
        var l = this.mockexams.length;
        this.mockexamdetails = [];
        for (let i = 0; i < l; i++) {
            this.mockExams = {
                _id: this.mockexams[i]._id,
                s_no: i + 1,
                examstatus: this.mockexams[i].examstatus,
                mockname: this.mockexams[i].mockname,
                examid: this.student_map._id,
                examseq: this.mockexams[i].examseq,
                exam_retake_limit: this.mockexams[i].examsretake,
                markPerQues: this.mockexams[i].markperquestions,
                negativeMark: this.mockexams[i].Negativemarks,
                examname: this.student_map.examname,
                timeduration: this.mockexams[i].timeduration,
                examshedule_date: this.student_map.examshedule_date,
                questioncount: this.mockexams[i].questioncount,
                buttonText: "",
                fa_icon: "",
                buttoncolor: "",
                langsupport: this.mockexams[i].langsupport,
                themesupport: this.student_map.themesupport,
                instructions: this.student_map.instructions,
                showviewsolution: this.mockexams[i].config.viewsolution,
                showpausebtn: this.mockexams[i].config.pause,
                showpdfdownload: this.mockexams[i].config.pdfviewsolution,
                testseriesid: this.testseriesid,
                testSeriesPaid: this.student_map.paid,
                startDate: this.mockexams[i].startDate,
                endDate: this.mockexams[i].endDate,
            };
            if (this.mockexams[i].examstatus == "0") {
                if (this.mockexams[i].freepaid == "1") {
                    debugger;
                    if (this.testseriesid != "0") {
                        if (this.student_map.paid) {
                            this.mockExams["buttonText"] = "Start Test";
                            this.mockExams["fa_icon"] = "fa fa-hand-o-right";
                            this.mockExams["buttoncolor"] = "success";
                            this.color = "success";
                        }
                        else {
                            this.mockExams["examstatus"] = "4";
                            this.mockExams["buttonText"] = "Unlock Test";
                            this.mockExams["fa_icon"] = "fa fa-lock";
                            this.mockExams["buttoncolor"] = "success";
                            this.color = "success";
                        }
                    }
                    else {
                        if (!this.userExpired) {
                            this.mockExams["buttonText"] = "Start Test";
                            this.mockExams["fa_icon"] = "fa fa-hand-o-right";
                            this.mockExams["buttoncolor"] = "success";
                            this.color = "success";
                        }
                        else {
                            this.mockExams["examstatus"] = "4";
                            this.mockExams["buttonText"] = "Unlock Test";
                            this.mockExams["fa_icon"] = "fa fa-lock";
                            this.mockExams["buttoncolor"] = "success";
                            this.color = "success";
                        }
                    }
                }
                else {
                    this.mockExams["buttonText"] = "Start Test";
                    this.mockExams["fa_icon"] = "fa fa-hand-o-right";
                    this.mockExams["buttoncolor"] = "success";
                    this.color = "success";
                }
                this.mockexamdetails.push(this.mockExams);
            }
            else if (this.mockexams[i].examstatus == "1") {
                this.mockExams["buttonText"] = "Resume";
                this.mockExams["fa_icon"] = "fa fa-pause-circle-o";
                this.mockExams["buttoncolor"] = "info";
                this.color = "info";
                this.mockexamdetails.push(this.mockExams);
            }
            else if (this.mockexams[i].examstatus == "2") {
                this.mockExams["buttonText"] = "Re-Take";
                this.mockExams["fa_icon"] = "fa fa-undo";
                this.mockExams["buttoncolor"] = "warning";
                this.color = "warning";
                this.mockexamdetails.push(this.mockExams);
            }
        }
        console.log(this.mockexamdetails);
        this.allmockExams = this.mockexamdetails;
        this.loader.hideLoader();
    }
    ;
    ionViewWillEnter() {
        debugger;
        this.loader.showImgLoader();
        this.examid = this.route.snapshot.paramMap.get("examid");
        this.testseriesid = this.route.snapshot.paramMap.get("testseriesid");
        this.localStorage
            .getObject("candidateUserdata")
            .then((result) => {
            if (result != null) {
                this.userdata = result;
                this.loginId = this.userdata.loginId;
                this.instituteuserid = this.userdata.instituteuserid;
                if (this.testseriesid != "0") {
                    console.log(this.testseriesid);
                }
                else {
                    this.apiService
                        .getTestmaps(this.loginId, this.instituteuserid, this.examid)
                        .subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.getMockexams = res;
                        this.examname = this.getMockexams[0].examname;
                        this.constructMockList(this.getMockexams);
                        console.log(res);
                    }));
                }
            }
        })
            .catch((e) => {
            console.log("error: " + e);
        });
        console.log("enter");
    }
    ionViewDidEnter() {
        this.localStorage.get('theme').then((res) => {
            this.theme = res;
            this.commonService.showTabs();
        });
        this.defaultHref = 'nexamhallcandidate/myexams';
    }
    ;
    onSearchTerm(ev) {
        this.mockexamdetails = this.allmockExams;
        const val = ev.detail.value;
        if (val && val.trim() !== "") {
            this.mockexamdetails = this.mockexamdetails.filter((term) => {
                return (term.mockname.toLowerCase().indexOf(val.trim().toLowerCase()) > -1);
            });
        }
    }
    ;
    presentFilter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _nExamHall_Custom_Components_mockfilter_modal_mockfilter_modal_component__WEBPACK_IMPORTED_MODULE_9__["MockfilterModalComponent"],
                swipeToClose: true,
                cssClass: 'half-modal',
                presentingElement: this.routerOutlet.nativeEl,
                componentProps: { excludedFilter: this.excludedFilter },
            });
            yield modal.present();
            const { data } = yield modal.onWillDismiss();
            if (data) {
                this.excludedFilter = data;
                this.updateFilter(this.excludedFilter);
            }
        });
    }
    updateFilter(excludedFilter) {
        this.mockexamdetails = this.allmockExams;
        console.log("gghh", excludedFilter);
        if (excludedFilter) {
            for (let i = 0; i < excludedFilter.length; i++) {
                this.mockexamdetails = this.mockexamdetails.filter((item) => item.examstatus !== excludedFilter[i]);
            }
        }
    }
    ;
    intructpage(mockid, examstatus, examname, examseq, language) {
        debugger;
        if (examstatus == "4") {
            if (this.testseriesid != "0") {
            }
            else {
            }
        }
        else {
            var validityCheckData = {
                loginId: this.loginId,
                examid: this.examid,
                mockid: mockid,
                testseriesid: this.testseriesid,
            };
            this.apiService
                .uservalidityCheck(validityCheckData)
                .subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                console.log(res);
                if (res["status"]) {
                    this.filtermockexam = this.mockexamdetails.filter((item) => item._id == mockid);
                    this.mockid = mockid;
                    this.examstatus = examstatus;
                    this.examseq = examseq;
                    this.localStorage.setObject("exam_details", this.filtermockexam[0]);
                    switch (examstatus) {
                        case "0":
                            this.localStorage.set("currentExamStatus", this.examstatus);
                            this.router.navigateByUrl("/nexamhallcandidate/myexams/intructionpage/" +
                                this.mockid +
                                "/" +
                                this.examstatus +
                                "/" +
                                this.examseq);
                            break;
                        case "1":
                            this.localStorage.set("currentExamStatus", this.examstatus);
                            this.router.navigateByUrl("/nexamhallcandidate/myexams/examconductor/" +
                                this.mockid +
                                "/" +
                                this.examstatus +
                                "/" +
                                this.examseq);
                            break;
                        case "2":
                            this.examseq = this.examseq + 1;
                            this.localStorage.set("currentExamStatus", this.examstatus);
                            this.router.navigateByUrl("/nexamhallcandidate/myexams/intructionpage/" +
                                this.mockid +
                                "/" +
                                this.examstatus +
                                "/" +
                                this.examseq);
                            break;
                        case "4":
                            this.router.navigateByUrl("pricing");
                            break;
                    }
                }
                else {
                    var msg = "Sorry, your plan is expired";
                    this.alertService.presentAlert(msg);
                }
            }));
        }
    }
};
MockexamlistPage.ctorParameters = () => [
    { type: _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonRouterOutlet"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"] },
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _nExamHall_Services_alert_service__WEBPACK_IMPORTED_MODULE_10__["AlertService"] },
    { type: _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_11__["CommonServicesService"] }
];
MockexamlistPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-mockexamlist',
        template: _raw_loader_mockexamlist_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_mockexamlist_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MockexamlistPage);



/***/ })

}]);
//# sourceMappingURL=mockexamlist-mockexamlist-module-es2015.js.map