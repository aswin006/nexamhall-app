(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["nExamHall-Core-Pages-onboardingscreen-onboardingscreen-module"], {
    /***/
    "Bz6x":
    /*!******************************************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/onboardingscreen/onboardingscreen-routing.module.ts ***!
      \******************************************************************************************/

    /*! exports provided: OnboardingscreenPageRoutingModule */

    /***/
    function Bz6x(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnboardingscreenPageRoutingModule", function () {
        return OnboardingscreenPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _onboardingscreen_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./onboardingscreen.page */
      "DrGe");

      var routes = [{
        path: '',
        component: _onboardingscreen_page__WEBPACK_IMPORTED_MODULE_3__["OnboardingscreenPage"]
      }];

      var OnboardingscreenPageRoutingModule = function OnboardingscreenPageRoutingModule() {
        _classCallCheck(this, OnboardingscreenPageRoutingModule);
      };

      OnboardingscreenPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OnboardingscreenPageRoutingModule);
      /***/
    },

    /***/
    "DrGe":
    /*!********************************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/onboardingscreen/onboardingscreen.page.ts ***!
      \********************************************************************************/

    /*! exports provided: OnboardingscreenPage */

    /***/
    function DrGe(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnboardingscreenPage", function () {
        return OnboardingscreenPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_onboardingscreen_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./onboardingscreen.page.html */
      "KFIW");
      /* harmony import */


      var _onboardingscreen_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./onboardingscreen.page.scss */
      "ur2E");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/storage */
      "e8h1");

      var OnboardingscreenPage = /*#__PURE__*/function () {
        function OnboardingscreenPage(router, storage) {
          _classCallCheck(this, OnboardingscreenPage);

          this.router = router;
          this.storage = storage;
          this.showSkip = true;
        }

        _createClass(OnboardingscreenPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "startApp",
          value: function startApp() {
            var _this = this;

            this.router.navigateByUrl("/nexamhallcore/auth", {
              replaceUrl: true
            }).then(function () {
              return _this.storage.set("ion_did_onboarding", true);
            });
          }
        }, {
          key: "onSlideChangeStart",
          value: function onSlideChangeStart(event) {
            var _this2 = this;

            event.target.isEnd().then(function (isEnd) {
              _this2.showSkip = !isEnd;
            });
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this3 = this;

            this.storage.get("ion_did_onboarding").then(function (res) {
              if (res === true) {
                _this3.router.navigateByUrl("/nexamhallcore/auth", {
                  replaceUrl: true
                });
              }
            });
          }
        }, {
          key: "ionViewDidLeave",
          value: function ionViewDidLeave() {// enable the root left menu when leaving the tutorial page
          }
        }, {
          key: "nextSlide",
          value: function nextSlide() {
            this.slides.slideNext();
          }
        }]);

        return OnboardingscreenPage;
      }();

      OnboardingscreenPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }];
      };

      OnboardingscreenPage.propDecorators = {
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ["slides", {
            "static": true
          }]
        }]
      };
      OnboardingscreenPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-onboardingscreen',
        template: _raw_loader_onboardingscreen_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_onboardingscreen_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], OnboardingscreenPage);
      var slideOpts = {
        grabCursor: true,
        cubeEffect: {
          shadow: true,
          slideShadows: true,
          shadowOffset: 20,
          shadowScale: 0.94
        },
        on: {
          beforeInit: function beforeInit() {
            var swiper = this;
            swiper.classNames.push("".concat(swiper.params.containerModifierClass, "cube"));
            swiper.classNames.push("".concat(swiper.params.containerModifierClass, "3d"));
            var overwriteParams = {
              slidesPerView: 1,
              slidesPerColumn: 1,
              slidesPerGroup: 1,
              watchSlidesProgress: true,
              resistanceRatio: 0,
              spaceBetween: 0,
              centeredSlides: false,
              virtualTranslate: true
            };
            this.params = Object.assign(this.params, overwriteParams);
            this.originalParams = Object.assign(this.originalParams, overwriteParams);
          },
          setTranslate: function setTranslate() {
            var swiper = this;
            var $el = swiper.$el,
                $wrapperEl = swiper.$wrapperEl,
                slides = swiper.slides,
                swiperWidth = swiper.width,
                swiperHeight = swiper.height,
                rtl = swiper.rtlTranslate,
                swiperSize = swiper.size;
            var params = swiper.params.cubeEffect;
            var isHorizontal = swiper.isHorizontal();
            var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
            var wrapperRotate = 0;
            var $cubeShadowEl;

            if (params.shadow) {
              if (isHorizontal) {
                $cubeShadowEl = $wrapperEl.find('.swiper-cube-shadow');

                if ($cubeShadowEl.length === 0) {
                  $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
                  $wrapperEl.append($cubeShadowEl);
                }

                $cubeShadowEl.css({
                  height: "".concat(swiperWidth, "px")
                });
              } else {
                $cubeShadowEl = $el.find('.swiper-cube-shadow');

                if ($cubeShadowEl.length === 0) {
                  $cubeShadowEl = swiper.$('<div class="swiper-cube-shadow"></div>');
                  $el.append($cubeShadowEl);
                }
              }
            }

            for (var i = 0; i < slides.length; i += 1) {
              var $slideEl = slides.eq(i);
              var slideIndex = i;

              if (isVirtual) {
                slideIndex = parseInt($slideEl.attr('data-swiper-slide-index'), 10);
              }

              var slideAngle = slideIndex * 90;
              var round = Math.floor(slideAngle / 360);

              if (rtl) {
                slideAngle = -slideAngle;
                round = Math.floor(-slideAngle / 360);
              }

              var progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
              var tx = 0;
              var ty = 0;
              var tz = 0;

              if (slideIndex % 4 === 0) {
                tx = -round * 4 * swiperSize;
                tz = 0;
              } else if ((slideIndex - 1) % 4 === 0) {
                tx = 0;
                tz = -round * 4 * swiperSize;
              } else if ((slideIndex - 2) % 4 === 0) {
                tx = swiperSize + round * 4 * swiperSize;
                tz = swiperSize;
              } else if ((slideIndex - 3) % 4 === 0) {
                tx = -swiperSize;
                tz = 3 * swiperSize + swiperSize * 4 * round;
              }

              if (rtl) {
                tx = -tx;
              }

              if (!isHorizontal) {
                ty = tx;
                tx = 0;
              }

              var transform$$1 = "rotateX(".concat(isHorizontal ? 0 : -slideAngle, "deg) rotateY(").concat(isHorizontal ? slideAngle : 0, "deg) translate3d(").concat(tx, "px, ").concat(ty, "px, ").concat(tz, "px)");

              if (progress <= 1 && progress > -1) {
                wrapperRotate = slideIndex * 90 + progress * 90;
                if (rtl) wrapperRotate = -slideIndex * 90 - progress * 90;
              }

              $slideEl.transform(transform$$1);

              if (params.slideShadows) {
                // Set shadows
                var shadowBefore = isHorizontal ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
                var shadowAfter = isHorizontal ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');

                if (shadowBefore.length === 0) {
                  shadowBefore = swiper.$("<div class=\"swiper-slide-shadow-".concat(isHorizontal ? 'left' : 'top', "\"></div>"));
                  $slideEl.append(shadowBefore);
                }

                if (shadowAfter.length === 0) {
                  shadowAfter = swiper.$("<div class=\"swiper-slide-shadow-".concat(isHorizontal ? 'right' : 'bottom', "\"></div>"));
                  $slideEl.append(shadowAfter);
                }

                if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
              }
            }

            $wrapperEl.css({
              '-webkit-transform-origin': "50% 50% -".concat(swiperSize / 2, "px"),
              '-moz-transform-origin': "50% 50% -".concat(swiperSize / 2, "px"),
              '-ms-transform-origin': "50% 50% -".concat(swiperSize / 2, "px"),
              'transform-origin': "50% 50% -".concat(swiperSize / 2, "px")
            });

            if (params.shadow) {
              if (isHorizontal) {
                $cubeShadowEl.transform("translate3d(0px, ".concat(swiperWidth / 2 + params.shadowOffset, "px, ").concat(-swiperWidth / 2, "px) rotateX(90deg) rotateZ(0deg) scale(").concat(params.shadowScale, ")"));
              } else {
                var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
                var multiplier = 1.5 - (Math.sin(shadowAngle * 2 * Math.PI / 360) / 2 + Math.cos(shadowAngle * 2 * Math.PI / 360) / 2);
                var scale1 = params.shadowScale;
                var scale2 = params.shadowScale / multiplier;
                var offset$$1 = params.shadowOffset;
                $cubeShadowEl.transform("scale3d(".concat(scale1, ", 1, ").concat(scale2, ") translate3d(0px, ").concat(swiperHeight / 2 + offset$$1, "px, ").concat(-swiperHeight / 2 / scale2, "px) rotateX(-90deg)"));
              }
            }

            var zFactor = swiper.browser.isSafari || swiper.browser.isUiWebView ? -swiperSize / 2 : 0;
            $wrapperEl.transform("translate3d(0px,0,".concat(zFactor, "px) rotateX(").concat(swiper.isHorizontal() ? 0 : wrapperRotate, "deg) rotateY(").concat(swiper.isHorizontal() ? -wrapperRotate : 0, "deg)"));
          },
          setTransition: function setTransition(duration) {
            var swiper = this;
            var $el = swiper.$el,
                slides = swiper.slides;
            slides.transition(duration).find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left').transition(duration);

            if (swiper.params.cubeEffect.shadow && !swiper.isHorizontal()) {
              $el.find('.swiper-cube-shadow').transition(duration);
            }
          }
        }
      };
      /***/
    },

    /***/
    "KFIW":
    /*!************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Core-Pages/onboardingscreen/onboardingscreen.page.html ***!
      \************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function KFIW(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content fullscreen=\"true\">\n  <div class=\"onboarding-main\">\n    <div class=\"onboarding-slide\">\n      <ion-slides\n      #slides\n      (ionSlideWillChange)=\"onSlideChangeStart($event)\"\n      pager=\"true\"\n      [options]=\"slideOpts\"\n    >\n      <ion-slide>\n        <img src=\"assets/images/onboarding/banner1.png\" class=\"slide-image\" />\n        <h2 class=\"slide-title\">Start Learning in  nExam Hall</h2>\n        <p>\n           As all the competitive exam goes online in India, it is important for learners to get practiced in the exam environment \n        </p>\n      </ion-slide>\n      <ion-slide>\n        <img\n          src=\"assets/images/onboarding/banner2.png\"\n          class=\"slide-image\"\n        />\n        <h2 class=\"slide-title\">Practice Best Questions</h2>\n        <p>\n          Practice thousands of questions created by experts & toppers, review answers with detailed solutions, track your progress with performance analysis\n        </p>\n      </ion-slide>\n  \n      <ion-slide>\n        <img\n          src=\"assets/images/onboarding/banner3.png\"\n          class=\"slide-image\"\n        />\n        <h2 class=\"slide-title\"> Appflow</h2>\n        <p>\n          Online exam through Just Exam uses revolutionary modes to make your whole examination process quick.\n        </p>\n      </ion-slide>\n      <ion-slide>\n        <img\n          src=\"assets/images/onboarding/banner4.png\"\n          class=\"slide-image\"\n        />\n        <h2 class=\"slide-title\">Analysis</h2>\n        <p>\n          simple and effective. Students get results instantly and compare with others.\n        \n        </p>\n      </ion-slide>\n    </ion-slides>\n    </div>\n  \n  </div>\n  <!--<div class=\"container\">-->\n  \n  \n  <!--</div>-->\n \n  <ion-tabs class=\"ion-no-border\" >\n    <ion-tab-bar slot=\"bottom\">\n      <ion-toolbar>\n        <ion-buttons slot=\"start\">\n          <ion-button color=\"dark\" (click)=\"startApp()\" [hidden]=\"!showSkip\"\n          >Skip</ion-button\n        >\n        </ion-buttons>\n        <ion-buttons slot=\"end\">\n          <ion-button class=\"btn btn-primary\"  (click)=\"nextSlide()\" [hidden]=\"!showSkip\">\n            Next\n          </ion-button>\n        </ion-buttons>\n        <button class=\"btn btn-block btn-primary\"   (click)=\"startApp()\"\n        [hidden]=\"showSkip\">\n        Get Started\n          <ion-icon slot=\"end\" name=\"arrow-forward\"></ion-icon>\n        </button>\n      </ion-toolbar>\n    </ion-tab-bar>\n  </ion-tabs>\n</ion-content>\n";
      /***/
    },

    /***/
    "aJvx":
    /*!**********************************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/onboardingscreen/onboardingscreen.module.ts ***!
      \**********************************************************************************/

    /*! exports provided: OnboardingscreenPageModule */

    /***/
    function aJvx(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OnboardingscreenPageModule", function () {
        return OnboardingscreenPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _onboardingscreen_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./onboardingscreen-routing.module */
      "Bz6x");
      /* harmony import */


      var _onboardingscreen_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./onboardingscreen.page */
      "DrGe");

      var OnboardingscreenPageModule = function OnboardingscreenPageModule() {
        _classCallCheck(this, OnboardingscreenPageModule);
      };

      OnboardingscreenPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _onboardingscreen_routing_module__WEBPACK_IMPORTED_MODULE_5__["OnboardingscreenPageRoutingModule"]],
        declarations: [_onboardingscreen_page__WEBPACK_IMPORTED_MODULE_6__["OnboardingscreenPage"]]
      })], OnboardingscreenPageModule);
      /***/
    },

    /***/
    "ur2E":
    /*!**********************************************************************************!*\
      !*** ./src/app/nExamHall-Core-Pages/onboardingscreen/onboardingscreen.page.scss ***!
      \**********************************************************************************/

    /*! exports provided: default */

    /***/
    function ur2E(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  --background: transparent;\n  --border-color: transparent;\n}\n\nion-slide h2 {\n  color: #233558;\n  font-size: 100%;\n}\n\nion-slide p {\n  color: #8193AE;\n}\n\n.swiper-slide {\n  display: block;\n}\n\n.slide-title {\n  margin-top: 16px;\n}\n\n.slide-image {\n  max-height: 40vh;\n  max-width: 100%;\n  margin: 25px 0;\n  pointer-events: none;\n}\n\nb {\n  font-weight: 500;\n}\n\np {\n  padding: 0 40px;\n  font-size: 14px;\n  line-height: 1.5;\n  color: var(--ion-color-step-600, #60646b);\n}\n\np b {\n  color: var(--ion-text-color, #000000);\n}\n\n.btn-block {\n  width: 90%;\n  margin: 0 auto;\n}\n\n.btn {\n  text-transform: capitalize;\n}\n\nion-button {\n  text-transform: capitalize;\n}\n\n.text-dark {\n  color: #475574;\n  font-size: 80%;\n  font-weight: 600;\n}\n\n::ng-deep .swiper-pagination-bullet-active {\n  background: #3879F0;\n  width: 20px;\n  height: 6px;\n  border-radius: 5px;\n}\n\n::ng-deep .swiper-container-horizontal > .swiper-pagination-bullets {\n  bottom: 0px;\n  left: 0;\n  width: 100%;\n}\n\n.onboarding-main {\n  padding: 5%;\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNvcmUtUGFnZXMvb25ib2FyZGluZ3NjcmVlbi9vbmJvYXJkaW5nc2NyZWVuLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHlCQUFBO0VBQ0EsMkJBQUE7QUFDSjs7QUFFTTtFQUNFLGNBQUE7RUFDQSxlQUFBO0FBQ1I7O0FBQ007RUFDSSxjQUFBO0FBQ1Y7O0FBRUU7RUFDRSxjQUFBO0FBQ0o7O0FBRUU7RUFDRSxnQkFBQTtBQUNKOztBQUVFO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUU7RUFDRSxnQkFBQTtBQUNKOztBQUVFO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlDQUFBO0FBQ0o7O0FBQ0k7RUFDRSxxQ0FBQTtBQUNOOztBQU9FO0VBQ0UsVUFBQTtFQUNBLGNBQUE7QUFKSjs7QUFPRTtFQUNFLDBCQUFBO0FBSko7O0FBUUU7RUFDSSwwQkFBQTtBQUxOOztBQVFFO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQUxKOztBQVNJO0VBQ0UsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBTk47O0FBU0k7RUFDRSxXQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7QUFQTjs7QUFXQztFQUNJLFdBQUE7RUFDQSxZQUFBO0FBUkwiLCJmaWxlIjoic3JjL2FwcC9uRXhhbUhhbGwtQ29yZS1QYWdlcy9vbmJvYXJkaW5nc2NyZWVuL29uYm9hcmRpbmdzY3JlZW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICB9XHJcbiAgaW9uLXNsaWRlIHtcclxuICAgICAgaDJ7XHJcbiAgICAgICAgY29sb3I6ICMyMzM1NTg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgICAgIHB7XHJcbiAgICAgICAgICBjb2xvcjojODE5M0FFO1xyXG4gICAgICB9XHJcbiAgfVxyXG4gIC5zd2lwZXItc2xpZGUge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgfVxyXG4gIFxyXG4gIC5zbGlkZS10aXRsZSB7XHJcbiAgICBtYXJnaW4tdG9wOiAxNnB4O1xyXG4gIH1cclxuICBcclxuICAuc2xpZGUtaW1hZ2Uge1xyXG4gICAgbWF4LWhlaWdodDogNDB2aDtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMjVweCAwO1xyXG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIGIge1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB9XHJcbiAgXHJcbiAgcCB7XHJcbiAgICBwYWRkaW5nOiAwIDQwcHg7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zdGVwLTYwMCwgIzYwNjQ2Yik7XHJcbiAgXHJcbiAgICBiIHtcclxuICAgICAgY29sb3I6IHZhcigtLWlvbi10ZXh0LWNvbG9yLCAjMDAwMDAwKTtcclxuICAgIH1cclxuICBcclxuICBcclxuICBcclxuICB9XHJcbiAgXHJcbiAgXHJcbiAgLmJ0bi1ibG9jayB7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgfVxyXG4gIFxyXG4gIC5idG4ge1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgIFxyXG4gIH1cclxuICBcclxuICBpb24tYnV0dG9ue1xyXG4gICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICB9XHJcbiAgXHJcbiAgLnRleHQtZGFyayB7XHJcbiAgICBjb2xvcjogIzQ3NTU3NDtcclxuICAgIGZvbnQtc2l6ZTogODAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICB9XHJcbiAgXHJcbiAgOjpuZy1kZWVwIHtcclxuICAgIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQtYWN0aXZlIHtcclxuICAgICAgYmFja2dyb3VuZDojMzg3OUYwO1xyXG4gICAgICB3aWR0aDogMjBweDtcclxuICAgICAgaGVpZ2h0OiA2cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIH1cclxuICBcclxuICAgIC5zd2lwZXItY29udGFpbmVyLWhvcml6b250YWw+LnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHMge1xyXG4gICAgICBib3R0b206IDBweDtcclxuICAgICAgbGVmdDogMDtcclxuICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIFxyXG4gLm9uYm9hcmRpbmctbWFpbntcclxuICAgICBwYWRkaW5nOiA1JTtcclxuICAgICBoZWlnaHQ6IDEwMCU7XHJcbiB9XHJcbiJdfQ== */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=nExamHall-Core-Pages-onboardingscreen-onboardingscreen-module-es5.js.map