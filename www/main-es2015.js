(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\MobileApp\ionicprojects\nExamHallApp\src\main.ts */"zUnb");


/***/ }),

/***/ "0O0W":
/*!************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/side-menu-bar/side-menu-bar.component.scss ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-menu ion-content {\n  --padding-bottom: 20px;\n  --background: var(--ion-item-background, var(--ion-background-color, #fff));\n}\n\n/* Remove background transitions for switching themes */\n\nion-menu ion-item {\n  --transition: none;\n}\n\nion-item.selected {\n  --color: var(--ion-color-primary);\n}\n\n/*\n * Material Design Menu\n*/\n\nion-menu.md ion-list {\n  padding: 20px 0;\n}\n\nion-menu.md ion-list-header {\n  padding-left: 18px;\n  padding-right: 18px;\n  text-transform: uppercase;\n  letter-spacing: 0.1em;\n  font-weight: 450;\n}\n\nion-menu.md ion-item {\n  --padding-start: 18px;\n  margin-right: 10px;\n  border-radius: 0 50px 50px 0;\n  font-weight: 500;\n}\n\nion-menu.md ion-item.selected {\n  --background: rgba(var(--ion-color-primary-rgb), 1.14);\n}\n\nion-menu.md ion-item.selected ion-icon {\n  color: white;\n}\n\nion-menu.md ion-item.selected ion-label {\n  color: white;\n}\n\nion-menu.md ion-list-header,\nion-menu.md ion-item ion-icon {\n  color: var(--ion-color-step-650, #5f6368);\n}\n\nion-menu.md ion-list:not(:last-of-type) {\n  border-bottom: 1px solid var(--ion-color-step-150, #d7d8da);\n}\n\n/*\n * iOS Menu\n*/\n\nion-menu.ios ion-list-header {\n  padding-left: 16px;\n  padding-right: 16px;\n  margin-bottom: 8px;\n}\n\nion-menu.ios ion-list {\n  padding: 20px 0 0;\n}\n\nion-menu.ios ion-item {\n  --padding-start: 16px;\n  --min-height: 50px;\n}\n\nion-menu.ios ion-item ion-icon {\n  font-size: 24px;\n  color: #73849a;\n}\n\nion-menu.ios ion-item.selected ion-icon {\n  color: var(--ion-color-primary);\n}\n\n.menu-header {\n  height: 125px;\n  width: 90%;\n  box-shadow: 0px 1px 10px rgba(98, 140, 255, 0.5);\n  transform: rotate(0deg);\n  margin: 15px 15px 15px 15px;\n  border-radius: 10px;\n}\n\n.header-content {\n  position: absolute;\n  top: 5%;\n  left: 10%;\n  display: flex;\n  align-items: center;\n}\n\n.header-content img {\n  width: 60px;\n  height: 60px;\n  border-radius: 50%;\n  border: 7px solid #25bbbb;\n  margin-right: 14px;\n}\n\n.header-content h2 {\n  font-weight: 600;\n  color: white;\n  letter-spacing: 0.4px;\n}\n\n.header-content p {\n  font-size: 12px;\n  color: white;\n  font-weight: 400;\n  letter-spacing: 0.4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvbkV4YW1IYWxsLUNvcmUtVXRpbHMvc2lkZS1tZW51LWJhci9zaWRlLW1lbnUtYmFyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksc0JBQUE7RUFFQSwyRUFBQTtBQURKOztBQUlFLHVEQUFBOztBQUNBO0VBQ0Usa0JBQUE7QUFESjs7QUFJRTtFQUNFLGlDQUFBO0FBREo7O0FBSUU7O0NBQUE7O0FBR0E7RUFDRSxlQUFBO0FBREo7O0FBSUU7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBRUEseUJBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0FBRko7O0FBS0U7RUFDRSxxQkFBQTtFQUVBLGtCQUFBO0VBRUEsNEJBQUE7RUFFQSxnQkFBQTtBQUxKOztBQWdCRTtFQUNFLHNEQUFBO0FBYko7O0FBZ0JFO0VBQ0UsWUFBQTtBQWJKOztBQWVFO0VBQ0UsWUFBQTtBQVpKOztBQWVFOztFQUVFLHlDQUFBO0FBWko7O0FBZUU7RUFDRSwyREFBQTtBQVpKOztBQWdCRTs7Q0FBQTs7QUFHQTtFQUNFLGtCQUFBO0VBQ0EsbUJBQUE7RUFFQSxrQkFBQTtBQWRKOztBQWlCRTtFQUNFLGlCQUFBO0FBZEo7O0FBaUJFO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtBQWRKOztBQWlCRTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FBZEo7O0FBaUJFO0VBQ0UsK0JBQUE7QUFkSjs7QUFpQkU7RUFZRSxhQUFBO0VBQ0EsVUFBQTtFQUNBLGdEQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBekJKOztBQTRCRTtFQUNFLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUF6Qko7O0FBMkJJO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUF6Qk47O0FBNEJJO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUExQk47O0FBOEJJO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0FBNUJOIiwiZmlsZSI6InNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvbkV4YW1IYWxsLUNvcmUtVXRpbHMvc2lkZS1tZW51LWJhci9zaWRlLW1lbnUtYmFyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLW1lbnUgaW9uLWNvbnRlbnQge1xyXG4gICAgLy8gLS1wYWRkaW5nLXRvcDogMjBweDtcclxuICAgIC0tcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgXHJcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1pdGVtLWJhY2tncm91bmQsIHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCAjZmZmKSk7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIFJlbW92ZSBiYWNrZ3JvdW5kIHRyYW5zaXRpb25zIGZvciBzd2l0Y2hpbmcgdGhlbWVzICovXHJcbiAgaW9uLW1lbnUgaW9uLWl0ZW0ge1xyXG4gICAgLS10cmFuc2l0aW9uOiBub25lO1xyXG4gIH1cclxuICBcclxuICBpb24taXRlbS5zZWxlY3RlZCB7XHJcbiAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgfVxyXG4gIFxyXG4gIC8qXHJcbiAgICogTWF0ZXJpYWwgRGVzaWduIE1lbnVcclxuICAqL1xyXG4gIGlvbi1tZW51Lm1kIGlvbi1saXN0IHtcclxuICAgIHBhZGRpbmc6IDIwcHggMDtcclxuICB9XHJcbiAgXHJcbiAgaW9uLW1lbnUubWQgaW9uLWxpc3QtaGVhZGVyIHtcclxuICAgIHBhZGRpbmctbGVmdDogMThweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDE4cHg7XHJcbiAgXHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IC4xZW07XHJcbiAgICBmb250LXdlaWdodDogNDUwO1xyXG4gIH1cclxuICBcclxuICBpb24tbWVudS5tZCBpb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDE4cHg7XHJcbiAgXHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgXHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDUwcHggNTBweCAwO1xyXG4gIFxyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICB9XHJcbiAgXHJcbi8vICAgaW9uLW1lbnUubWQgaW9uLWl0ZW0uc2VsZWN0ZWQge1xyXG4vLyAgICAgLS1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXJnYiksIDEuMTQpO1xyXG4vLyAgIH1cclxuICBcclxuLy8gICBpb24tbWVudS5tZCBpb24taXRlbS5zZWxlY3RlZCBpb24taWNvbiB7XHJcbi8vICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4vLyAgIH1cclxuXHJcbiAgaW9uLW1lbnUubWQgaW9uLWl0ZW0uc2VsZWN0ZWQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXJnYiksIDEuMTQpO1xyXG4gIH1cclxuICBcclxuICBpb24tbWVudS5tZCBpb24taXRlbS5zZWxlY3RlZCBpb24taWNvbiB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG4gIGlvbi1tZW51Lm1kIGlvbi1pdGVtLnNlbGVjdGVkIGlvbi1sYWJlbCB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1tZW51Lm1kIGlvbi1saXN0LWhlYWRlcixcclxuICBpb24tbWVudS5tZCBpb24taXRlbSBpb24taWNvbiB7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXN0ZXAtNjUwLCAjNWY2MzY4KTtcclxuICB9XHJcbiAgXHJcbiAgaW9uLW1lbnUubWQgaW9uLWxpc3Q6bm90KDpsYXN0LW9mLXR5cGUpIHtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3Itc3RlcC0xNTAsICNkN2Q4ZGEpO1xyXG4gIH1cclxuICBcclxuICBcclxuICAvKlxyXG4gICAqIGlPUyBNZW51XHJcbiAgKi9cclxuICBpb24tbWVudS5pb3MgaW9uLWxpc3QtaGVhZGVyIHtcclxuICAgIHBhZGRpbmctbGVmdDogMTZweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDE2cHg7XHJcbiAgXHJcbiAgICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbiAgfVxyXG4gIFxyXG4gIGlvbi1tZW51LmlvcyBpb24tbGlzdCB7XHJcbiAgICBwYWRkaW5nOiAyMHB4IDAgMDtcclxuICB9XHJcbiAgXHJcbiAgaW9uLW1lbnUuaW9zIGlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMTZweDtcclxuICAgIC0tbWluLWhlaWdodDogNTBweDtcclxuICB9XHJcbiAgXHJcbiAgaW9uLW1lbnUuaW9zIGlvbi1pdGVtIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgIGNvbG9yOiAjNzM4NDlhO1xyXG4gIH1cclxuICBcclxuICBpb24tbWVudS5pb3MgaW9uLWl0ZW0uc2VsZWN0ZWQgaW9uLWljb24ge1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICB9XHJcbiAgXHJcbiAgLm1lbnUtaGVhZGVyIHtcclxuICAgIC8vIGhlaWdodDogMTI1cHg7XHJcbiAgICAvLyB3aWR0aDogMzUwcHg7XHJcbiAgICAvLyAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMjViYmJiO1xyXG4gICAgLy8gLy8gYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDkwZGVnLCAjOGNmMGYwIDAlLCAjMjViYmJiIDEwMCUpO1xyXG4gICAgLy8gYm94LXNoYWRvdzogMHB4IDFweCAxMHB4IHJnYmEoOTgsIDE0MCwgMjU1LCAwLjUpO1xyXG4gICAgLy8gdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICAvLyAvKiBib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMTBweCAzMHB4OyAqL1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IC0xOHB4O1xyXG4gICAgLy8gbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgLy8gbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICAgIC8vIC8vIG1hcmdpbjogLTIwcHggYXV0bztcclxuICAgIGhlaWdodDogMTI1cHg7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAxMHB4IHJnYmEoOTgsIDE0MCwgMjU1LCAwLjUpO1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XHJcbiAgICBtYXJnaW46IDE1cHggMTVweCAxNXB4IDE1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIH1cclxuICBcclxuICAuaGVhZGVyLWNvbnRlbnQge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1JTtcclxuICAgIGxlZnQ6IDEwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIFxyXG4gICAgaW1nIHtcclxuICAgICAgd2lkdGg6IDYwcHg7XHJcbiAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICBib3JkZXI6IDdweCBzb2xpZCAjMjViYmJiO1xyXG4gICAgICBtYXJnaW4tcmlnaHQ6IDE0cHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuNHB4O1xyXG4gIFxyXG4gICAgfVxyXG4gIFxyXG4gICAgcCB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgICBsZXR0ZXItc3BhY2luZzogMC40cHg7XHJcbiAgXHJcbiAgICB9XHJcbiAgfVxyXG4gICJdfQ== */");

/***/ }),

/***/ "23nY":
/*!********************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/candidate-dashboard/candidate-dashboard.page.ts ***!
  \********************************************************************************************/
/*! exports provided: CandidateDashboardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidateDashboardPage", function() { return CandidateDashboardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_candidate_dashboard_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./candidate-dashboard.page.html */ "FTAm");
/* harmony import */ var _candidate_dashboard_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./candidate-dashboard.page.scss */ "pEEP");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../nExamHall-Services/theme-switcher.service */ "Fql8");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../nExamHall-Services/local-storage.service */ "2eCX");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! chart.js */ "MO+k");
/* harmony import */ var chart_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(chart_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../nExamHall-Services/api.service */ "xHtS");
/* harmony import */ var _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../nExamHall-Services/loader.service */ "9mvZ");
/* harmony import */ var _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../nExamHall-Services/common-services.service */ "FE8S");










let CandidateDashboardPage = class CandidateDashboardPage {
    constructor(themeSwitcherService, localStorage, apiService, loader, commonService) {
        this.themeSwitcherService = themeSwitcherService;
        this.localStorage = localStorage;
        this.apiService = apiService;
        this.loader = loader;
        this.commonService = commonService;
        this.showLess = true;
        this.showMoreBtn = 'Show more';
        this.showMoreBtnIcon = 'down';
        this.chartCardCustomStyle = 'showmore';
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
    }
    ionViewWillEnter() {
        debugger;
        this.loader.showImgLoader();
        this.localStorage.get('theme').then((res) => {
            this.theme = res;
            this.localStorage
                .getObject("candidateUserdata")
                .then((result) => {
                if (result != null) {
                    this.userdata = result;
                    this.loginId = this.userdata.loginId;
                    this.userimage = this.commonService.addImageUrl(this.userdata.userimage, this.userdata.firstName, 60);
                    this.username = this.userdata.firstName;
                    this.lastlogin_date = this.userdata.lastlogin_date;
                    console.log("token :" + result);
                    this.apiService.getDashBoardDetails(this.loginId).subscribe((res) => {
                        // debugger;
                        this.dashboardData = res;
                        this.dashBoardGraphGenerate(this.dashboardData);
                        this.loader.hideLoader();
                        console.log(res);
                    });
                }
            })
                .catch((e) => {
                console.log("error: " + e);
            });
        });
    }
    showMore(value) {
        // debugger;
        this.showLess = !this.showLess;
        this.showMoreBtn = this.showLess == true ? 'Show more' : 'Show less';
        this.showMoreBtnIcon = this.showLess == true ? 'down' : 'up';
        this.chartCardCustomStyle = this.showLess == true ? 'showmore' : 'showless';
        // for(let item of this.pdata){
        // }
    }
    dashBoardGraphGenerate(data) {
        this.pdata = [
            {
                value: data.totalexamsmapped,
                color: '#EA347E',
                highlight: "#EA347E",
                label: 'Exams Mapped',
                isChart: false,
                show: true
            },
            {
                value: data.attended,
                color: '#2AC96D',
                highlight: "#2AC96D",
                label: 'Exams Attended',
                isChart: false,
                show: true
            },
            {
                value: data.incompleteexams,
                color: '#F49251',
                highlight: "#F49251",
                label: 'Exams Incompleted',
                isChart: true,
                show: false
            },
            {
                value: data.passedexams,
                color: '#2AC96D',
                highlight: "#2AC96D",
                label: 'Exam Passed',
                isChart: true,
                show: false
            },
            {
                value: data.failedexams,
                color: '#2AC8C7',
                highlight: "#2AC8C7",
                label: 'Exam Failed',
                isChart: true,
                show: false
            },
            {
                value: data.yettoteaken,
                color: '#FBD12C',
                highlight: "#FBD12C",
                label: 'Exams Yet to be taken',
                isChart: true,
                show: false
            },
        ];
        var doughnutDataValues = [];
        var doughnutBackgroundColor = [];
        var doughnutLabel = [];
        for (let value of this.pdata) {
            if (value.isChart) {
                doughnutDataValues.push(value.value);
                doughnutBackgroundColor.push(value.color);
                doughnutLabel.push(value.label);
            }
        }
        var ctxp = this.doughnutCanvas.nativeElement;
        /***Doughnut Chart */
        var doughnutChart = new chart_js__WEBPACK_IMPORTED_MODULE_6__["Chart"](ctxp, {
            type: 'doughnut',
            data: {
                labels: doughnutLabel,
                datasets: [{
                        data: doughnutDataValues,
                        backgroundColor: doughnutBackgroundColor,
                        hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
                        hoverBorderColor: "rgba(234, 236, 244, 1)",
                    }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 10,
                    yPadding: 10,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                animation: {
                    animateRotate: true
                },
                title: {
                    display: false,
                    text: 'Status Based'
                },
                cutoutPercentage: 60,
            },
        });
    }
};
CandidateDashboardPage.ctorParameters = () => [
    { type: _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_4__["ThemeSwitcherService"] },
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__["LocalStorageService"] },
    { type: _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_7__["ApiService"] },
    { type: _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_8__["LoaderService"] },
    { type: _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_9__["CommonServicesService"] }
];
CandidateDashboardPage.propDecorators = {
    doughnutCanvas: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ["doughnutCanvas",] }]
};
CandidateDashboardPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-candidate-dashboard',
        template: _raw_loader_candidate_dashboard_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_candidate_dashboard_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CandidateDashboardPage);



/***/ }),

/***/ "2LUc":
/*!****************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/bottom-menu-bar/bottom-menu-bar.component.scss ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL25FeGFtSGFsbC1Db3JlLVV0aWxzL2JvdHRvbS1tZW51LWJhci9ib3R0b20tbWVudS1iYXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "2eCX":
/*!*************************************************************!*\
  !*** ./src/app/nExamHall-Services/local-storage.service.ts ***!
  \*************************************************************/
/*! exports provided: LocalStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStorageService", function() { return LocalStorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "e8h1");



let LocalStorageService = class LocalStorageService {
    constructor(storage) {
        this.storage = storage;
    }
    //set a key value
    set(key, value) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const result = yield this.storage.set(key, value);
                console.log("set string in storage : " + result);
                return true;
            }
            catch (reason) {
                console.log(reason);
                return false;
            }
        });
    }
    //to get a key /value pair
    get(key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const result = yield this.storage.get(key);
                console.log("storageGET:" + key + ":" + result);
                if (result != null) {
                    return result;
                }
                return null;
            }
            catch (reason) {
                console.log(reason);
                return null;
            }
        });
    }
    setObject(key, object) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const result = yield this.storage.set(key, JSON.stringify(object));
                console.log("set Object in storage: " + result);
                return true;
            }
            catch (reason) {
                console.log(reason);
                return false;
            }
        });
    }
    // get a key/value object
    getObject(key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                const result = yield this.storage.get(key);
                if (result != null) {
                    return JSON.parse(result);
                }
                return null;
            }
            catch (reason) {
                console.log(reason);
                return null;
            }
        });
    }
    // remove a single key value:
    remove(key) {
        this.storage.remove(key);
    }
    //  delete all data from your application:
    clear() {
        this.storage.clear();
    }
};
LocalStorageService.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] }
];
LocalStorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LocalStorageService);



/***/ }),

/***/ "5IGy":
/*!********************************************************************************************!*\
  !*** ./src/app/nExamHall-Custom-Components/mockfilter-modal/mockfilter-modal.component.ts ***!
  \********************************************************************************************/
/*! exports provided: MockfilterModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MockfilterModalComponent", function() { return MockfilterModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_mockfilter_modal_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./mockfilter-modal.component.html */ "OoTI");
/* harmony import */ var _mockfilter_modal_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mockfilter-modal.component.scss */ "rl9i");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





let MockfilterModalComponent = class MockfilterModalComponent {
    constructor(config, modalCtrl, navParams) {
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.filerLists = [];
        this.filerList = [];
        this.filerLists = [
            {
                name: "Start Test",
                value: "0",
                isChecked: false,
            },
            {
                name: "Resume Test",
                value: "1",
                isChecked: false,
            },
            {
                name: "Retake Test",
                value: "2",
                isChecked: false,
            },
        ];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        debugger;
        const excludedFilter = this.navParams.get("excludedFilter");
        this.filerLists.forEach((item) => {
            this.filerList.push({
                name: item.name,
                value: item.value,
                isChecked: excludedFilter.indexOf(item.value) === -1,
            });
        });
        console.log(this.filerList);
    }
    selectAll(check) {
        this.filerList.forEach((item) => {
            item.isChecked = check;
        });
    }
    applyFilters() {
        const excludedFilter = this.filerList
            .filter((c) => !c.isChecked)
            .map((c) => c.value);
        console.log(excludedFilter);
        this.dismiss(excludedFilter);
    }
    dismiss(data) {
        this.modalCtrl.dismiss(data);
    }
};
MockfilterModalComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Config"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] }
];
MockfilterModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-mockfilter-modal',
        template: _raw_loader_mockfilter_modal_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_mockfilter_modal_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MockfilterModalComponent);



/***/ }),

/***/ "6aOO":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/settings/settings.component.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header class=\"ion-no-border\">\n  <ion-toolbar [color]=\"theme\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"secondary\">\n      <ion-button>\n        <ion-icon name=\"notifications-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"text-center\"> Settings</ion-title>\n  </ion-toolbar>\n</ion-header>-->\n\n<ion-content fullscreen>\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n      <ion-menu-button autoHide=\"false\" class=\"headerButton\"></ion-menu-button>\n      \n    </div>\n    <div class=\"pageTitle\">\n      Settings\n    </div>\n    <div class=\"right\">\n        <button  class=\"headerButton\">\n            <ion-icon class=\"icon\" name=\"notifications-outline\"></ion-icon>\n            <span class=\"badge badge-danger\"></span>\n        </button>\n    </div>\n</div>\n<div class=\"main\">\n  <ion-grid>\n    <ion-list>\n      <ion-list-header>\n        <ion-icon slot=\"start\" name=\"color-palette\"></ion-icon>\n        <p><strong>Themes</strong></p>\n      </ion-list-header>\n      <div class=\"container mt-2 theme-container\" >\n        <p><strong>Choose color</strong></p>\n       \n          <ion-segment\n            scrollable\n            [(ngModel)]=\"selectedcolor\"\n            (ionChange)=\"themeColorSet(selectedcolor)\"\n          >\n            <ion-segment-button\n              *ngFor=\"let t of themes;let i = index\"\n              value=\"{{t.id}}\"\n              id=\"seg-{{i}}\"\n            >\n              <button\n              [ngClass]=\"t.cssclass\"\n                class=\"btn color-btn \"\n              >\n              </button>\n            </ion-segment-button>\n          </ion-segment>\n      \n      </div>\n       \n      \n    </ion-list>\n    <ion-list>\n      <div class=\"container mt-2 theme-container\" >\n        <p><strong>Choose mode</strong></p>\n       \n          <ion-segment\n            scrollable\n            [(ngModel)]=\"selectedMode\"\n            (ionChange)=\"colorMode(selectedMode)\"\n          >\n            <ion-segment-button value=\"day\" id=\"modeseg1\" >\n                <ion-icon name=\"sunny-outline\"></ion-icon>\n                <ion-label>Day</ion-label>\n            </ion-segment-button>\n            <ion-segment-button value=\"night\" id=\"modeseg2\" >\n              <ion-icon name=\"moon\"></ion-icon>\n              <ion-label>Night</ion-label>\n          </ion-segment-button>\n          <ion-segment-button value=\"auto\" id=\"modeseg3\" >\n            <ion-icon name=\"contrast-outline\"></ion-icon>\n            <ion-label>Auto</ion-label>\n        </ion-segment-button>\n          </ion-segment>\n      \n      </div>\n       \n      \n    </ion-list>\n  </ion-grid>\n</div>\n \n\n</ion-content>");

/***/ }),

/***/ "8VBk":
/*!********************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/myexams/myexams.page.ts ***!
  \********************************************************************/
/*! exports provided: MyexamsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyexamsPage", function() { return MyexamsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_myexams_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./myexams.page.html */ "aodY");
/* harmony import */ var _myexams_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./myexams.page.scss */ "mIMV");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../nExamHall-Services/local-storage.service */ "2eCX");
/* harmony import */ var _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../nExamHall-Services/api.service */ "xHtS");
/* harmony import */ var _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../nExamHall-Services/common-services.service */ "FE8S");
/* harmony import */ var _nExamHall_Services_alert_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../nExamHall-Services/alert.service */ "bFVc");
/* harmony import */ var _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../nExamHall-Services/loader.service */ "9mvZ");










let MyexamsPage = class MyexamsPage {
    constructor(localStorage, apiService, commonService, alertService, loader, router) {
        this.localStorage = localStorage;
        this.apiService = apiService;
        this.commonService = commonService;
        this.alertService = alertService;
        this.loader = loader;
        this.router = router;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        debugger;
        this.loader.showImgLoader();
        this.localStorage.get('theme').then((res) => {
            this.theme = res;
            this.localStorage
                .getObject("candidateUserdata")
                .then((result) => {
                if (result != null) {
                    var _userdetails = result;
                    this.loginId = _userdetails.loginId;
                    console.log("token :" + _userdetails);
                    this.apiService
                        .userEnrolled_exams(this.loginId)
                        .subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        //debugger;
                        this.bgcolors = [
                            "linear-gradient(rgba(255, 250, 238, 1) , white)",
                            "linear-gradient(rgba(255, 243, 243, 1) , white)",
                            "linear-gradient(rgba(251, 252, 236, 1) , white)",
                        ];
                        var curr = 0;
                        this.allocate_exams = res;
                        this.selected_Exam = [];
                        // console.log("this.allocate_exams :", this.allocate_exams);
                        for (let i in this.allocate_exams) {
                            this.selected_Exam.push({
                                examid: this.allocate_exams[i]._id,
                                examname: this.allocate_exams[i].examname,
                                examimage: this.commonService.addImageUrl(this.allocate_exams[i].images, this.allocate_exams[i].examname, 60),
                                mockexamslength: this.allocate_exams[i].mockexamscount,
                                testserieslength: this.allocate_exams[i].testseriescount,
                                testseriessupport: this.allocate_exams[i].testseries,
                                livemocktestlength: this.allocate_exams[i].livemockcount,
                                livemocktestsupport: _userdetails.features.livetest,
                                background_color: this.bgcolors[curr],
                            });
                            curr++;
                            if (curr == this.bgcolors.length) {
                                curr = 0;
                            }
                        }
                        console.log(this.selected_Exam);
                    }));
                }
                this.loader.hideLoader();
            })
                .catch((e) => {
                console.log("error: " + e);
                this.loader.hideLoader();
            });
        });
    }
    ;
    gomocklist(examid, mockexamslength) {
        //debugger;
        if (mockexamslength > 0) {
            // $location.path('examlist/' + examid + '/' + 0);
            this.router.navigateByUrl(`/nexamhallcandidate/myexams/mockexamlist/${examid}/0`);
        }
        else {
            var alertMsg = 'This Exam have no more mock tests';
            this.alertService.presentAlert(alertMsg);
        }
    }
    ;
    goTestSeries(examid, testserieslength) {
        if (testserieslength > 0) {
            this.router.navigateByUrl(`/nexamhallcandidate/myexams/testseries/${examid}`);
            // $location.path('testseries/' + examid)
        }
        else {
            var alertMsg = 'This Exam have no more test series';
            this.alertService.presentAlert(alertMsg);
        }
    }
    goLiveTest(examid, livetestlength) {
        if (livetestlength > 0) {
            // setLocalStorage('livetest', _userdetails.features.livetest)
            // $location.path('examlist/' + examid + '/' + 0)
        }
        else {
            var alertMsg = 'This Exam have no more live test';
            this.alertService.presentAlert(alertMsg);
        }
    }
};
MyexamsPage.ctorParameters = () => [
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__["LocalStorageService"] },
    { type: _nExamHall_Services_api_service__WEBPACK_IMPORTED_MODULE_6__["ApiService"] },
    { type: _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_7__["CommonServicesService"] },
    { type: _nExamHall_Services_alert_service__WEBPACK_IMPORTED_MODULE_8__["AlertService"] },
    { type: _nExamHall_Services_loader_service__WEBPACK_IMPORTED_MODULE_9__["LoaderService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
MyexamsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-myexams',
        template: _raw_loader_myexams_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_myexams_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MyexamsPage);



/***/ }),

/***/ "8mUz":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.page.html ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>nExamHall-Candidate-Core-Module</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "9lZg":
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/bottom-menu-bar/bottom-menu-bar.component.html ***!
  \******************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-tabs  >\n  <ion-tab-bar slot=\"bottom\" id=\"bottombar\">\n    <ion-tab-button tab=\"dashboard\">\n      <ion-icon name=\"home\"></ion-icon>\n      <ion-label>Home</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"myexams\">\n      <ion-icon name=\"receipt\"></ion-icon>\n      <ion-label>My Exams</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"analysischart\">\n      <ion-icon name=\"pie-chart\"></ion-icon>\n      <ion-label>Analysis</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"profile\">\n      <ion-icon name=\"information-circle\"></ion-icon>\n      <ion-label>Settings</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n</ion-tabs>\n");

/***/ }),

/***/ "9mvZ":
/*!******************************************************!*\
  !*** ./src/app/nExamHall-Services/loader.service.ts ***!
  \******************************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let LoaderService = class LoaderService {
    constructor(loadingController) {
        this.loadingController = loadingController;
        this.isLoading = false;
    }
    // showImgLoader(){
    //   this.loadingController.create({
    //    // message:'Please wait ...',
    //     mode:'ios',
    //     message: '<div class="text-center"><img src="../assets/images/icons/book.gif"   /> <br />Please wait..</div>',
    //     spinner: null, 
    //     cssClass:'custom-loader-class',
    //   }).then((res) =>{
    //     res.present();
    //   })
    // };
    // showNormalLoader(){
    //   this.loadingController.create({
    //     message:'Please wait ...',
    //     mode:'ios',
    //   }).then((res) =>{
    //     res.present();
    //   })
    // };
    // async hideLoader(){
    //   let topLoader = await this.loadingController.getTop();
    // while (topLoader) {
    //   if (!(await topLoader.dismiss())) {
    //     console.log('Could not dismiss the topmost loader. Aborting...');
    //   }
    //   topLoader = await this.loadingController.getTop();
    // }
    // }
    showNormalLoader() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: 'Please wait ...',
                mode: 'ios',
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    showImgLoader() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                mode: 'ios',
                message: '<div class="text-center"><img src="../assets/images/icons/book.gif"   /> <br />Please wait..</div>',
                spinner: null,
                cssClass: 'custom-loader-class',
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoaderService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var apiurl = 'http://104.218.53.125/';
const environment = {
    production: false,
    apiUrl: apiurl + 'adminmerge/api/v1.0/',
    commonImgUrl: apiurl + 'adminmerge/',
    subdomain: "localhost/atech/"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "FBZy":
/*!**********************************************************************************!*\
  !*** ./src/app/nExamHall-Custom-Components/nExamhall-custom-component.module.ts ***!
  \**********************************************************************************/
/*! exports provided: nExamHallcustomComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "nExamHallcustomComponentsModule", function() { return nExamHallcustomComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _otp_pin_screen_otp_pin_screen_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./otp-pin-screen/otp-pin-screen.component */ "bsB4");




let nExamHallcustomComponentsModule = class nExamHallcustomComponentsModule {
};
nExamHallcustomComponentsModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_otp_pin_screen_otp_pin_screen_component__WEBPACK_IMPORTED_MODULE_3__["OtpPinScreenComponent"]],
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"]],
        exports: [_otp_pin_screen_otp_pin_screen_component__WEBPACK_IMPORTED_MODULE_3__["OtpPinScreenComponent"]]
    })
], nExamHallcustomComponentsModule);



/***/ }),

/***/ "FE8S":
/*!***************************************************************!*\
  !*** ./src/app/nExamHall-Services/common-services.service.ts ***!
  \***************************************************************/
/*! exports provided: CommonServicesService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonServicesService", function() { return CommonServicesService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "AytR");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "TEn/");




let CommonServicesService = class CommonServicesService {
    constructor(alertCtrl) {
        this.alertCtrl = alertCtrl;
        this.commonImgUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].commonImgUrl;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl;
    }
    addImageUrl(img, name, size) {
        debugger;
        var first_part = img.substring(0, 8);
        if (first_part === 'http://' || first_part === 'https://') {
            var image = img;
        }
        else {
            if (img == "" || img == null || img == undefined) {
                var image = this.letterLogo(name, size, 1);
            }
            else {
                var image = this.commonImgUrl + img;
            }
        }
        return image;
    }
    showExitConfirm() {
        this.alertCtrl.create({
            header: 'Exit App',
            message: 'Do you want to close the app?',
            backdropDismiss: false,
            mode: 'ios',
            buttons: [{
                    text: 'Stay',
                    role: 'cancel',
                    handler: () => {
                        console.log('Application exit prevented!');
                    }
                }, {
                    text: 'Exit',
                    handler: () => {
                        navigator['app'].exitApp();
                    }
                }]
        })
            .then(alert => {
            alert.present();
        });
    }
    checkEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(email)) {
            // Invalid Email
            return false;
        }
        else {
            return true;
        }
    }
    pathService(pathname) {
        switch (pathname) {
            case "auth":
                this.returnPath = '/nexamhallcore/auth';
                break;
            case "login":
                this.returnPath = '/nexamhallcore/auth/login';
                break;
            case "signup":
                this.returnPath = '/nexamhallcore/auth/signup';
                break;
            case "forgot":
                this.returnPath = '/nexamhallcore/auth/forgot';
                break;
            case "resetpassword":
                this.returnPath = '/nexamhallcore/auth/resetpassword';
                break;
            case "otp":
                this.returnPath = '/nexamhallcore/auth/otp';
                break;
            case "candidatedashboard":
                this.returnPath = '/nexamhallcandidate/dashboard';
                break;
            default:
        }
        return this.returnPath;
    }
    ;
    letterLogo(name, size, wordcount) {
        name = name || "";
        size = size || 60;
        wordcount = wordcount || 1;
        var colours = [
            "#1abc9c",
            "#2ecc71",
            "#3498db",
            "#9b59b6",
            "#34495e",
            "#16a085",
            "#27ae60",
            "#2980b9",
            "#8e44ad",
            "#2c3e50",
            "#f1c40f",
            "#e67e22",
            "#e74c3c",
            "#ecf0f1",
            "#95a5a6",
            "#f39c12",
            "#d35400",
            "#c0392b",
            "#bdc3c7",
            "#7f8c8d",
        ], nameSplit = String(name).toUpperCase().split(" "), initials, charIndex, colourIndex, canvas, context, dataURI;
        // if (nameSplit.length == 1) {
        //     initials = nameSplit[0] ? nameSplit[0].charAt(0) : '?';
        // } else {
        //     initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
        // }
        if (wordcount == 1) {
            initials = nameSplit[0] ? nameSplit[0].charAt(0) : "?";
        }
        else {
            initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
        }
        if (window.devicePixelRatio) {
            size = size * window.devicePixelRatio;
        }
        charIndex = (initials == "?" ? 72 : initials.charCodeAt(0)) - 64;
        colourIndex = charIndex % 10;
        canvas = document.createElement("canvas");
        canvas.width = size;
        canvas.height = size;
        context = canvas.getContext("2d");
        context.fillStyle = colours[colourIndex - 1];
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.font = Math.round(canvas.width / 2) + "px Arial";
        context.textAlign = "center";
        context.fillStyle = "#FFF";
        context.fillText(initials, size / 2, size / 1.5);
        dataURI = canvas.toDataURL();
        canvas = null;
        return dataURI;
    }
    hideTabs() {
        const tabBar = document.getElementById('bottombar');
        if (tabBar.style.display !== 'none')
            tabBar.style.display = 'none';
    }
    showTabs() {
        const tabBar = document.getElementById('bottombar');
        if (tabBar.style.display !== 'flex')
            tabBar.style.display = 'flex';
    }
    ;
    instructionData() {
        var data = [
            {
                ta: {
                    heading1: "பொது வழிமுறை",
                    heading2: "தேர்வு அறிவுறுத்தல்",
                    generalInst: '<div><ol start="1"><li><p class="text-left">திரையின் மேல் வலது மூலையில் உள்ள கவுண்டவுன் டைமர் நீங்கள் தேர்வை முடிக்க மீதமுள்ள நேரத்தைக் காண்பிக்கும். டைமர் பூஜ்ஜியத்தை அடையும் போது, தேர்வு தானாகவே முடிவடையும். நீங்கள் தேர்வை நிறுத்தவோ அல்லது உங்கள் தாளைச் சமர்ப்பிக்கவோ தேவையில்லை.</p></li></ol><ol start="2"><li><p class="text-left">திரையின் வலது பக்கத்தில் காட்டப்படும் கேள்வி தட்டு பின்வரும் கேள்விகளில் ஒன்றைப் பயன்படுத்தி ஒவ்வொரு கேள்வியின் நிலையையும் காண்பிக்கும்:</p><ul class="que-ans-states text-left"><li><button class="intruct_btn answer"></button> என்ற கேள்விக்கு நீங்கள் பதிலளித்துள்ளீர்கள்..</li><li><button class="intruct_btn notvist"></button> கேள்வியை நீங்கள் இதுவரை பார்வையிடவில்லை.</li><li><button class="intruct_btn not_ans"></button> என்ற கேள்விக்கு நீங்கள் பதிலளிக்கவில்லை.</li><li><button class="intruct_btn Markedans"></button> மதிப்பாய்வுக்கான கேள்வியைக் குறித்துள்ளீர்கள், ஆனால் பதிலளித்தீர்கள்.</li><li><button class="intruct_btn Markfor_review"></button> மதிப்பாய்வுக்கான கேள்வியை நீங்கள் குறித்துள்ளீர்கள், ஆனால் பதிலளிக்கவில்லை.</li></ul></li></ol><ol start="3"><li><p class="text-left">ஒரு கேள்விக்கான மதிப்பாய்வுக்கான நிலை நீங்கள் அந்த கேள்வியை மீண்டும் பார்க்க விரும்புகிறீர்கள் என்பதைக் குறிக்கிறது. ஒரு கேள்விக்கு பதிலளிக்கப்பட்டாலும், மதிப்பாய்வுக்காக குறிக்கப்பட்டால், வேட்பாளரால் அந்தஸ்தை மாற்றியமைக்காவிட்டால் பதில் மதிப்பீட்டிற்கு பரிசீலிக்கப்படும்.</p></li></ol><ol start="4"><li><p class="text-left">பல தேர்வு வகை கேள்விக்கு பதிலளிப்பதற்கான செயல்முறை:</p><ul class="text-left"><li>நீங்கள் தேர்ந்தெடுத்த பதிலைத் தேர்வுநீக்க, தேர்ந்தெடுக்கப்பட்ட விருப்பத்தின் குமிழியை மீண்டும் கிளிக் செய்யவும் அல்லது தெளிவான முடிவு பொத்தானைக் கிளிக் செய்யவும்.</li><li>நீங்கள் தேர்ந்தெடுத்த பதிலை மாற்ற, மற்றொரு விருப்பத்தின் குமிழியைக் கிளிக் செய்க.</li><li>உங்கள் பதிலைச் சேமிக்க, சேமி &amp; அடுத்து என்பதைக் கிளிக் செய்ய வேண்டும்.</li><li>அந்த கேள்விக்கு நேரடியாகச் செல்ல கேள்வி தட்டில் உள்ள கேள்வி எண்ணைக் கிளிக் செய்க.</li><li>4 தேர்வுகளுக்கு முன் வைக்கப்பட்டுள்ள குமிழியைக் கிளிக் செய்வதன் மூலம் பல தேர்வு வகை கேள்விக்கான பதிலைத் தேர்ந்தெடுக்கவும்.</li><li>தற்போதைய கேள்விக்கான உங்கள் பதிலைச் சேமிக்க சேமி &amp; அடுத்து என்பதைக் கிளிக் செய்து அடுத்த கேள்விக்குச் செல்லவும்.</li><li>தற்போதைய கேள்விக்கான உங்கள் பதிலைச் சேமிக்க மார்க் ஃபார் ரிவியூவைக் கிளிக் செய்து மதிப்பாய்வுக்காகக் குறிக்கவும், பின்னர் அடுத்த கேள்விக்குச் செல்லவும்.</li></ul></li></ol><ol start="5"><li><p class="text-left">ஏற்கனவே பதிலளிக்கப்பட்ட கேள்விக்கு உங்கள் பதிலை மாற்ற, முதலில் பதிலளிக்க அந்த கேள்வியைத் தேர்ந்தெடுத்து, பின்னர் அந்த வகை கேள்விக்கு பதிலளிப்பதற்கான நடைமுறையைப் பின்பற்றவும்.</p></li></ol><ol start="6"><li><p class="text-left">முந்தைய கேள்விக்கான பதிலைச் சேமிக்காமல் ஒரு கேள்வி எண்ணைக் கிளிக் செய்வதன் மூலம் நீங்கள் நேரடியாக மற்றொரு கேள்விக்குச் சென்றால், தற்போதைய கேள்விக்கான உங்கள் பதில் சேமிக்கப்படாது.</p></li></ol><ol start="7"><li><p class="text-left">ஒரு கேள்விக்கான மார்க் ஃபார் ரிவியூ நிலையை நீங்கள் மீண்டும் அந்த கேள்வியைப் பார்க்க விரும்புகிறீர்கள் என்பதைக் குறிக்கிறது. ஒரு கேள்விக்கு பதிலளிக்கப்பட்டாலும், மதிப்பாய்வுக்காக குறிக்கப்பட்டால், வேட்பாளரால் அந்தஸ்தை மாற்றியமைக்காவிட்டால் பதில் மதிப்பீட்டிற்கு பரிசீலிக்கப்படும்.</p></li></ol><ol start="8"><li><p class="text-left">பதில்களுக்குப் பிறகு சேமிக்கப்பட்ட அல்லது மதிப்பாய்வு செய்ய குறிக்கப்பட்ட கேள்விகள் மட்டுமே மதிப்பீட்டிற்கு பரிசீலிக்கப்படும் என்பதை நினைவில் கொள்க.</p></li></ol><ol start="9"><li><p class="text-left">ஒரு பிரிவின் கடைசி கேள்விக்கான சேமி &amp; அடுத்து பொத்தானைக் கிளிக் செய்த பிறகு, அடுத்த பிரிவின் முதல் கேள்விக்கு நீங்கள் தானாக அழைத்துச் செல்லப்படுவீர்கள்.</p></li></ol></div>',
                    examInst: "",
                    declareSelect: '<p><label for="language">உங்கள் இயல்புநிலை மொழியைத் தேர்வுசெய்க:</label> <select class="" ng-model="defaultlanguage" ng-change="select_change(defaultlanguage)" placeholder="select"  ng-options="language.lngcode as language.lngname for language in languageSuport" ><option value="" disabled selected hidden>Select</option></select></p>',
                    DeclarationTitle: '<p class="dec_margin text-left"><strong class="ng-binding"> உறுதிமொழி: </strong></p>',
                    Declaration: '<span class="res exam-instructions-declare_box">  எல்லா வழிமுறைகளையும் கவனமாகப் படித்து அவற்றைப் புரிந்துகொண்டேன். இந்த தேர்வில் ஏமாற்று அல்லது நியாயமற்ற வழிகளைப் பயன்படுத்த வேண்டாம் என்று ஒப்புக்கொள்கிறேன். எனது சொந்த அல்லது வேறு ஒருவருக்கு எந்தவொரு நியாயமற்ற வழிகளையும் பயன்படுத்துவது எனது உடனடி தகுதியிழப்புக்கு வழிவகுக்கும் என்பதை நான் புரிந்துகொள்கிறேன். இந்த விஷயங்களில் eParikshaa.com இன் முடிவு இறுதியானது மற்றும் மேல்முறையீடு செய்ய முடியாது. </span>',
                },
                en: {
                    heading1: "General Instructions",
                    heading2: "Exam Instructions",
                    generalInst: '<div><ol start="1"><li><p class="text-left">The clock will be set at the server. The countdown timer at the top right corner of screen will display the remaining time available for you to complete the examination. When the timer reaches zero, the examination will end by itself. You need not terminate the examination or submit your paper.</p></li></ol><ol start="2"><li><p class="text-left">The Question Palette displayed on the right side of screen will show the status of each question using one of the following symbols:</p><ul class="que-ans-states text-left"><li><button class="intruct_btn answer"></button> You have answered the question.</li><li><button class="intruct_btn notvist"></button> You have not visited the question yet.</li><li><button class="intruct_btn not_ans"></button> You have not answered the question.</li><li><button class="intruct_btn Markedans"></button> You have marked the question for review but answered</li><li><button class="intruct_btn Markfor_review"></button> You have marked the question for review but NOT answered.</li></ul></li></ol><ol start="3"><li><p class="text-left">The <strong>Mark For Review </strong> status for a question simply indicates that you would like to look at that question again. If a question is answered, but marked for review, then the answer will be considered for evaluation unless the status is modified by the candidate.</p></li></ol><ol start="4"><li><p class="text-left">Procedure for answering a multiple choice type question:</p><ul class="text-left"><li>To deselect your chosen answer, click on the bubble of the chosen option again or click on the Clear Result button.</li><li>To change your chosen answer, click on the bubble of another option.</li><li>To save your answer, you MUST click on the Save &amp; Next.</li><li>Click on the question number in the Question Palette to go to that question directly.</li><li>Select an answer for a multiple choice type question by clicking on the bubble placed before the 4 choices.</li><li>Click on Save &amp; Next to save your answer for the current question and then go to the next question.</li><li>Click on Mark for Review to save your answer for the current question and also mark it for review, and then go to the next question.</li></ul></li></ol><ol start="5"><li><p class="text-left">To change your answer to a question that has already been answered, first select that question for answering and then follow the procedure for answering that type of question</p></li></ol><ol start="6"><li><p class="text-left">that your answer for the current question will not be saved, if you navigate to another question directly by clicking on a question number without saving the answer to the previous question.</p></li></ol><ol start="7"><li><p class="text-left">Note that Mark For Review status for a question simply indicates that you would like to look at that question again. If a question is answered, but marked for review, then the answer will be considered for evaluation unless the status is modified by the candidate</p></li></ol><ol start="8"><li><p class="text-left">Note that ONLY Questions for which answers are saved or marked for review after answering will be considered for evaluation.</p></li></ol><ol start="9"><li><p class="text-left">After clicking the Save &amp; Next button for the last question in a Section, you will automatically be taken to the first question of the next Section in sequence.</p></li></ol></div>',
                    examInst: "",
                    declareSelect: '<p><label for="language">Choose your default language:</label> <select class="" ng-model="defaultlanguage" ng-change="select_change(defaultlanguage)" placeholder="select"  ng-options="language.lngcode as language.lngname for language in languageSuport" ><option value="" disabled selected hidden>Select</option></select></p>',
                    DeclarationTitle: '<p class="dec_margin text-left"><strong class="ng-binding"> Declaration </strong></p>',
                    Declaration: '<span class="res exam-instructions-declare_box"> I have read all the instructions carefully and have understood them. I agree not to cheat or use unfair means in this examination. I understand that using unfair means of any sort for my own or someone else&rsquo;s advantage will lead to my immediate disqualification. The decision of eParikshaa.com will be final in these matters and cannot be appealed. </span>',
                },
            },
        ];
        return data[0];
    }
};
CommonServicesService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }
];
CommonServicesService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], CommonServicesService);



/***/ }),

/***/ "FTAm":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/candidate-dashboard/candidate-dashboard.page.html ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"secondary\">\n      <ion-button>\n        <ion-icon name=\"notifications-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"text-center\"> n Exam Hall </ion-title>\n  </ion-toolbar>\n</ion-header>-->\n\n<ion-content>\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n      <ion-menu-button autoHide=\"false\" class=\"headerButton\"></ion-menu-button>\n\n    </div>\n    <div class=\"pageTitle\">\n      nExamHall\n    </div>\n    <div class=\"right\">\n      <button class=\"headerButton\">\n        <ion-icon class=\"icon\" name=\"notifications-outline\"></ion-icon>\n        <span class=\"badge badge-danger\"></span>\n      </button>\n    </div>\n  </div>\n  <div class=\"main\">\n    <div class=\"cross-header pb-6\" [ngClass]=\"theme\">\n      <div class=\"container-fluid\">\n        <div class=\"cross-header-body\">\n          <div class=\"py-1\">\n            <div class=\"card-body user-avatar\">\n              <div class=\"row\">\n                <ion-avatar slot=\"start\">\n                  <img [src]=\"userimage\">\n                </ion-avatar>\n                <ion-label>\n                  <h3>{{username}}</h3>\n                  <p>{{loginId}}</p>\n                </ion-label>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"container-fluid mt--6 dashboard-analysis-container\">\n      <div class=\"row\">\n        <div class=\"col-12\">\n          <div class=\"card\" >\n            <div class=\"card-body\">\n              <div class=\"row header no-gutters\">\n                <div class=\"col\">\n                  <h5 class=\"card-title text-uppercase text-muted mb-0 font-weight-bold\">Test Overview</h5>\n                </div>\n                <div class=\"col-auto\">\n                  <div class=\"icon icon-shape bg-gradient-red rounded-circle shadow\">\n                    <ion-icon src=\"./assets/icon/analysisicon.svg\"></ion-icon>\n                  </div>\n                </div>\n              </div>\n              <div class=\"chart-container-card\" [ngClass]=\"chartCardCustomStyle\">\n                <ion-row class=\"no-gutters\">\n                  <ion-col size=\"4\" size-md>\n                    <div class=\"chart-card\">\n                      <canvas #doughnutCanvas></canvas>\n                    </div>\n                   \n                  </ion-col>\n                  <ion-col size=\"8\" size-md>\n                    <ion-item class=\"progress-container\" lines=\"none\" *ngFor=\"let item of pdata;let i = index;\" >\n                      <ion-thumbnail slot=\"start\">\n                        <i class=\"badge text-white\"  [style.background-color]=\"item.color\">{{item.value}}</i>\n                      </ion-thumbnail>\n                      <ion-label>\n                        <h6>{{item.label}}</h6>\n                        <div class=\"progress\">\n                          <div class=\"progress-bar\" role=\"progressbar\" style=\"width: 25%\" \n                          [style.background-color]=\"item.color\"\n                          aria-valuenow=\"25\"\n                            aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                        </div>\n                      </ion-label>\n                    </ion-item>\n                   \n                  \n                  </ion-col>\n                </ion-row>\n               \n              </div>\n              <ion-chip color=\"secondary-outline\" class=\"pull-right\" (click)=\"showMore(showLess)\">\n                <ion-text>{{showMoreBtn}}</ion-text>\n                <ion-icon [name]=\"'chevron-'+showMoreBtnIcon +'-outline'\"></ion-icon>\n              </ion-chip>\n             \n            </div>\n           \n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n</ion-content>");

/***/ }),

/***/ "Fql8":
/*!**************************************************************!*\
  !*** ./src/app/nExamHall-Services/theme-switcher.service.ts ***!
  \**************************************************************/
/*! exports provided: ThemeSwitcherService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeSwitcherService", function() { return ThemeSwitcherService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../nExamHall-Services/local-storage.service */ "2eCX");





let ThemeSwitcherService = class ThemeSwitcherService {
    constructor(domCtrl, document, localStorage) {
        this.domCtrl = domCtrl;
        this.document = document;
        this.localStorage = localStorage;
        this.themeModes = [];
        this.currentMode = 0;
        this.theme = '';
        this.themes = [];
        this.themes = [
            {
                id: 1,
                name: 'Yellow',
                color: 'yellow',
                icon: 'color-palette',
                cssclass: 'theme-yellow',
                colorHex: '#FFB64D',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "243, 205, 116",
                    ionCcolorPrimaryContrast: "#000000",
                    ionCcolorPrimaryContrastrgb: " 0,0,0",
                    ionCcolorPrimaryShade: "#d8aa41",
                    ionCcolorPrimaryTint: "#f7c75c",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            },
            {
                id: 2,
                name: 'Rich Green',
                color: 'richgreen',
                icon: 'color-palette-outline',
                cssclass: 'theme-richgreen',
                colorHex: '#18B6B7',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "80,220,221",
                    ionCcolorPrimaryContrast: "#000000",
                    ionCcolorPrimaryContrastrgb: " 0,0,0",
                    ionCcolorPrimaryShade: "#15a0a1",
                    ionCcolorPrimaryTint: "#2fbdbe",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            },
            {
                id: 3,
                name: 'Pink',
                color: 'dark',
                icon: 'color-palette',
                cssclass: 'theme-pink',
                colorHex: '#C886D0',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "243, 181, 211",
                    ionCcolorPrimaryContrast: "#000000",
                    ionCcolorPrimaryContrastrgb: " 0,0,0",
                    ionCcolorPrimaryShade: "#b076b7",
                    ionCcolorPrimaryTint: "#ce92d5",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            },
            {
                id: 4,
                name: 'Blue',
                color: 'blue',
                icon: 'color-palette',
                cssclass: 'theme-blue',
                colorHex: '#344BDC',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "5,65,147",
                    ionCcolorPrimaryContrast: "#ffffff",
                    ionCcolorPrimaryContrastrgb: "255,255,255",
                    ionCcolorPrimaryShade: "#043981",
                    ionCcolorPrimaryTint: "#1e549e",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            },
            {
                id: 5,
                name: 'Orange',
                color: 'orange',
                icon: 'color-palette',
                cssclass: 'theme-orange',
                colorHex: '#ff6269',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "255,123,84",
                    ionCcolorPrimaryContrast: "#000000",
                    ionCcolorPrimaryContrastrgb: " 0,0,0",
                    ionCcolorPrimaryShade: "#e0565c",
                    ionCcolorPrimaryTint: "#ff7278",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            },
            {
                id: 6,
                name: 'Green',
                color: 'green',
                icon: 'color-palette',
                cssclass: 'theme-green',
                colorHex: '#85E5A8',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "191,239,177",
                    ionCcolorPrimaryContrast: "#000000",
                    ionCcolorPrimaryContrastrgb: " 0,0,0",
                    ionCcolorPrimaryShade: "#75ca94",
                    ionCcolorPrimaryTint: "#91e8b1",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            },
            {
                id: 7,
                name: 'Purple',
                color: 'purple',
                icon: 'color-palette',
                cssclass: 'theme-purple',
                colorHex: '#2E306E',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "76,75,155",
                    ionCcolorPrimaryContrast: "#ffffff",
                    ionCcolorPrimaryContrastrgb: "255,255,255",
                    ionCcolorPrimaryShade: "#282a61",
                    ionCcolorPrimaryTint: "#43457d",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            },
            {
                id: 8,
                name: 'Seablue',
                color: 'seablue',
                icon: 'color-palette',
                cssclass: 'theme-seablue',
                colorHex: '#4099ff',
                themeStyles: {
                    ionColorPrimary: "#1D2237",
                    ionColorPrimaryrgb: "64,153,255",
                    ionCcolorPrimaryContrast: "#000000",
                    ionCcolorPrimaryContrastrgb: "0,0,0",
                    ionCcolorPrimaryShade: "#3887e0",
                    ionCcolorPrimaryTint: "#53a3ff",
                    ionItemIosCackgroundColor: "",
                    ionItemMdBackgroundColor: "",
                    ionTabbarBackgroundColor: "",
                    ionTabbarIosTextColorActive: "",
                    ionTabbarMdTextColorActive: "",
                    ionBackgroundColor: ""
                }
            }
        ];
        this.themeModes = [
            {
                name: 'day',
                styles: [
                    // { themeModeVariable: '--ion-color-primary', value: '#D5D7DA'},
                    { themeModeVariable: '--ion-color-primary-rgb', value: '48,46,68' },
                    { themeModeVariable: '--ion-color-primary-contrast', value: '#ffffff' },
                    { themeModeVariable: '--ion-color-primary-contrast-rgb', value: '255,255,255' },
                    { themeModeVariable: '--ion-color-primary-shade', value: '#1e2023' },
                    { themeModeVariable: '--ion-color-primary-tint', value: '#383a3e' },
                    { themeModeVariable: '--ion-item-ios-background-color', value: '#717171' },
                    { themeModeVariable: '--ion-item-md-background-color', value: '#717171' },
                    { themeModeVariable: '--ion-tabbar-background-color', value: '#302E44' },
                    { themeModeVariable: '--ion-tabbar-ios-text-color-active', value: '#ffffff' },
                    { themeModeVariable: '--ion-tabbar-md-text-color-active', value: '#ffffff' },
                    { themeModeVariable: '--ion-background-color', value: '#f1f4f6' }
                ]
            },
            {
                name: 'night',
                styles: [
                    // { themeModeVariable: '--ion-color-primary', value: '#302E44'},
                    { themeModeVariable: '--ion-color-primary-rgb', value: '48,46,68' },
                    { themeModeVariable: '--ion-color-primary-contrast', value: '#ffffff' },
                    { themeModeVariable: '--ion-color-primary-contrast-rgb', value: '255,255,255' },
                    { themeModeVariable: '--ion-color-primary-shade', value: '#1e2023' },
                    { themeModeVariable: '--ion-color-primary-tint', value: '#383a3e' },
                    { themeModeVariable: '--ion-item-ios-background-color', value: '#717171' },
                    { themeModeVariable: '--ion-item-md-background-color', value: '#717171' },
                    { themeModeVariable: '--ion-tabbar-background-color', value: '#302E44' },
                    { themeModeVariable: '--ion-tabbar-ios-text-color-active', value: '#ffffff' },
                    { themeModeVariable: '--ion-tabbar-md-text-color-active', value: '#ffffff' },
                    // { themeModeVariable: '--ion-background-color', value: '#383838'}
                    { themeModeVariable: '--ion-background-color', value: '#181937' }
                ]
            }
        ];
    }
    ;
    getThemes() {
        return this.themes;
    }
    canLoad() {
        return this.localStorage.get('theme').then(res => {
            if (res) {
                // this.router.navigate(['/app', 'tabs', 'schedule']);
                //  this.router.navigate(['/nexamhallcore/auth',]);
                this.localStorage.set('theme', res);
                this.setThemeProperty(res);
                // return true;
            }
            else {
                this.theme = 'theme-yellow';
                this.localStorage.set('theme', this.theme);
                this.setThemeProperty(this.theme);
            }
            return true;
        });
    }
    intitialTheme() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            debugger;
            this.theme = yield this.localStorage.get('theme');
            return this.theme;
        });
    }
    setTheme() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.localStorage.get('theme').then((res) => {
                if (res == null) {
                    this.theme = 'theme-richgreen';
                    this.localStorage.set('theme', this.theme);
                }
                else {
                    this.theme = res;
                }
            });
        });
    }
    setMode(name) {
        this.localStorage.get('theme').then((res) => {
            this.currentTheme = res;
            let mode = this.themeModes.find(x => x.name === name);
            let currentThemecolor = this.themes.find(x => x.cssclass === this.currentTheme);
            this.domCtrl.write(() => {
                mode.styles.forEach(style => {
                    if (style.themeModeVariable == '--ion-color-primary-rgb') {
                        style.value = this.hexToRgbA(currentThemecolor['colorHex']);
                    }
                    document.documentElement.style.setProperty(style.themeModeVariable, style.value);
                });
            });
        });
    }
    hex2rgba(hex, alpha = 1) {
        const [r, g, b] = hex.match(/\w\w/g).map(x => parseInt(x, 16));
        // return `rgba(${r},${g},${b},${alpha})`;
        return `${r},${g},${b}`;
    }
    ;
    setThemeProperty(currentTheme) {
        debugger;
        let currentThemecolor = this.themes.find(x => x.cssclass === currentTheme);
        this.domCtrl.write(() => {
            //  var rgba_color = this.hex2rgba(currentThemecolor['colorHex'],0.5);
            var rgba_color = currentThemecolor.themeStyles.ionColorPrimaryrgb;
            var primarycolor = currentThemecolor.colorHex;
            document.documentElement.style.setProperty('--ion-color-primary-rgb', rgba_color);
            document.documentElement.style.setProperty('--ion-color-primary', primarycolor);
        });
    }
    hexToRgbA(hex) {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split('');
            if (c.length == 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = '0x' + c.join('');
            return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ')';
        }
        console.log('error hex');
    }
};
ThemeSwitcherService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["DomController"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["DOCUMENT"],] }] },
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"] }
];
ThemeSwitcherService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ThemeSwitcherService);



/***/ }),

/***/ "MSrT":
/*!**********************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/side-menu-bar/side-menu-bar.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: SideMenuBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideMenuBarComponent", function() { return SideMenuBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_side_menu_bar_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./side-menu-bar.component.html */ "teMF");
/* harmony import */ var _side_menu_bar_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./side-menu-bar.component.scss */ "0O0W");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../nExamHall-Services/theme-switcher.service */ "Fql8");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../nExamHall-Services/local-storage.service */ "2eCX");
/* harmony import */ var _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../nExamHall-Services/common-services.service */ "FE8S");









let SideMenuBarComponent = class SideMenuBarComponent {
    constructor(menu, platform, router, themeSwitcherService, localStorage, commonService) {
        //debugger;
        this.menu = menu;
        this.platform = platform;
        this.router = router;
        this.themeSwitcherService = themeSwitcherService;
        this.localStorage = localStorage;
        this.commonService = commonService;
        this.appPages = [
            {
                title: 'Home',
                url: '/nexamhallcandidate/dashboard',
                icon: 'home'
            },
            {
                title: 'My Exams',
                url: '/nexamhallcandidate/myexams',
                icon: 'receipt'
            },
            {
                title: 'My Videoseries',
                url: '/nexamhallcandidate/videoseries',
                icon: 'person'
            },
            {
                title: 'My Result',
                url: '/nexamhallcandidate/completedresult',
                icon: 'information-circle'
            }
        ];
    }
    ngOnInit() {
        //   this.themeSwitcherService.intitialTheme().then((res) =>{
        //     this.theme = res;
        //  });
    }
    logout() {
        // this.userData.logout().then(() => {
        return this.router.navigateByUrl('/nexamhallcore/auth/login');
        //});
    }
    getTheme() {
        return this.localStorage.get('theme').then((res) => {
            return this.theme = res;
        });
    }
    initTheme() {
        //debugger;
        this.localStorage.get('theme').then((res) => {
            this.theme = res;
            this.themeSwitcherService.setThemeProperty(this.theme);
            this.localStorage
                .getObject("candidateUserdata")
                .then((result) => {
                if (result != null) {
                    this.userdata = result;
                    this.userimage = this.commonService.addImageUrl(this.userdata.userimage, this.userdata.firstName, 60);
                    this.username = this.userdata.firstName;
                    this.lastlogin_date = this.userdata.lastlogin_date;
                }
            });
        });
    }
};
SideMenuBarComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_6__["ThemeSwitcherService"] },
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"] },
    { type: _nExamHall_Services_common_services_service__WEBPACK_IMPORTED_MODULE_8__["CommonServicesService"] }
];
SideMenuBarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-side-menu-bar',
        template: _raw_loader_side_menu_bar_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_side_menu_bar_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SideMenuBarComponent);



/***/ }),

/***/ "OoTI":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Custom-Components/mockfilter-modal/mockfilter-modal.component.html ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n<ion-toolbar>\n  <ion-buttons slot=\"start\" (click)=\"dismiss()\">\n    <ion-button>\n      Cancel\n    </ion-button>\n  </ion-buttons>\n  <ion-buttons slot=\"end\">\n    <ion-button (click)=\"applyFilters()\">\n      Done\n    </ion-button>\n  </ion-buttons>\n\n  <ion-title class=\"text-center\">Filters</ion-title>\n</ion-toolbar>\n\n<ion-list>\n  <ion-item *ngFor=\"let item of filerList\">\n    <ion-label>{{item.name}}</ion-label>\n    <ion-checkbox [(ngModel)]=\"item.isChecked\" color=\"success\" mode=\"ios\"></ion-checkbox>\n  </ion-item>\n</ion-list>\n\n<ion-toolbar>\n  <ion-buttons slot=\"start\">\n    <ion-button (click)=\"selectAll(false)\">Deselect All</ion-button>\n  </ion-buttons>\n  <ion-buttons slot=\"end\">\n    <ion-button (click)=\"selectAll(true)\">Select All</ion-button>\n  </ion-buttons>\n</ion-toolbar>\n</ion-content>");

/***/ }),

/***/ "P/7j":
/*!***************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/analysis-chart/analysis-chart.component.ts ***!
  \***************************************************************************************/
/*! exports provided: AnalysisChartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalysisChartComponent", function() { return AnalysisChartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_analysis_chart_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./analysis-chart.component.html */ "YOHI");
/* harmony import */ var _analysis_chart_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./analysis-chart.component.scss */ "ReDX");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../nExamHall-Services/local-storage.service */ "2eCX");





let AnalysisChartComponent = class AnalysisChartComponent {
    constructor(localStorage) {
        this.localStorage = localStorage;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        //debugger;
        this.localStorage.get('theme').then((res) => {
            this.theme = res;
        });
    }
};
AnalysisChartComponent.ctorParameters = () => [
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"] }
];
AnalysisChartComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-analysis-chart',
        template: _raw_loader_analysis_chart_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_analysis_chart_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AnalysisChartComponent);



/***/ }),

/***/ "ReDX":
/*!*****************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/analysis-chart/analysis-chart.component.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL2FuYWx5c2lzLWNoYXJ0L2FuYWx5c2lzLWNoYXJ0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "RnhZ":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "K/tc",
	"./af.js": "K/tc",
	"./ar": "jnO4",
	"./ar-dz": "o1bE",
	"./ar-dz.js": "o1bE",
	"./ar-kw": "Qj4J",
	"./ar-kw.js": "Qj4J",
	"./ar-ly": "HP3h",
	"./ar-ly.js": "HP3h",
	"./ar-ma": "CoRJ",
	"./ar-ma.js": "CoRJ",
	"./ar-sa": "gjCT",
	"./ar-sa.js": "gjCT",
	"./ar-tn": "bYM6",
	"./ar-tn.js": "bYM6",
	"./ar.js": "jnO4",
	"./az": "SFxW",
	"./az.js": "SFxW",
	"./be": "H8ED",
	"./be.js": "H8ED",
	"./bg": "hKrs",
	"./bg.js": "hKrs",
	"./bm": "p/rL",
	"./bm.js": "p/rL",
	"./bn": "kEOa",
	"./bn-bd": "loYQ",
	"./bn-bd.js": "loYQ",
	"./bn.js": "kEOa",
	"./bo": "0mo+",
	"./bo.js": "0mo+",
	"./br": "aIdf",
	"./br.js": "aIdf",
	"./bs": "JVSJ",
	"./bs.js": "JVSJ",
	"./ca": "1xZ4",
	"./ca.js": "1xZ4",
	"./cs": "PA2r",
	"./cs.js": "PA2r",
	"./cv": "A+xa",
	"./cv.js": "A+xa",
	"./cy": "l5ep",
	"./cy.js": "l5ep",
	"./da": "DxQv",
	"./da.js": "DxQv",
	"./de": "tGlX",
	"./de-at": "s+uk",
	"./de-at.js": "s+uk",
	"./de-ch": "u3GI",
	"./de-ch.js": "u3GI",
	"./de.js": "tGlX",
	"./dv": "WYrj",
	"./dv.js": "WYrj",
	"./el": "jUeY",
	"./el.js": "jUeY",
	"./en-au": "Dmvi",
	"./en-au.js": "Dmvi",
	"./en-ca": "OIYi",
	"./en-ca.js": "OIYi",
	"./en-gb": "Oaa7",
	"./en-gb.js": "Oaa7",
	"./en-ie": "4dOw",
	"./en-ie.js": "4dOw",
	"./en-il": "czMo",
	"./en-il.js": "czMo",
	"./en-in": "7C5Q",
	"./en-in.js": "7C5Q",
	"./en-nz": "b1Dy",
	"./en-nz.js": "b1Dy",
	"./en-sg": "t+mt",
	"./en-sg.js": "t+mt",
	"./eo": "Zduo",
	"./eo.js": "Zduo",
	"./es": "iYuL",
	"./es-do": "CjzT",
	"./es-do.js": "CjzT",
	"./es-mx": "tbfe",
	"./es-mx.js": "tbfe",
	"./es-us": "Vclq",
	"./es-us.js": "Vclq",
	"./es.js": "iYuL",
	"./et": "7BjC",
	"./et.js": "7BjC",
	"./eu": "D/JM",
	"./eu.js": "D/JM",
	"./fa": "jfSC",
	"./fa.js": "jfSC",
	"./fi": "gekB",
	"./fi.js": "gekB",
	"./fil": "1ppg",
	"./fil.js": "1ppg",
	"./fo": "ByF4",
	"./fo.js": "ByF4",
	"./fr": "nyYc",
	"./fr-ca": "2fjn",
	"./fr-ca.js": "2fjn",
	"./fr-ch": "Dkky",
	"./fr-ch.js": "Dkky",
	"./fr.js": "nyYc",
	"./fy": "cRix",
	"./fy.js": "cRix",
	"./ga": "USCx",
	"./ga.js": "USCx",
	"./gd": "9rRi",
	"./gd.js": "9rRi",
	"./gl": "iEDd",
	"./gl.js": "iEDd",
	"./gom-deva": "qvJo",
	"./gom-deva.js": "qvJo",
	"./gom-latn": "DKr+",
	"./gom-latn.js": "DKr+",
	"./gu": "4MV3",
	"./gu.js": "4MV3",
	"./he": "x6pH",
	"./he.js": "x6pH",
	"./hi": "3E1r",
	"./hi.js": "3E1r",
	"./hr": "S6ln",
	"./hr.js": "S6ln",
	"./hu": "WxRl",
	"./hu.js": "WxRl",
	"./hy-am": "1rYy",
	"./hy-am.js": "1rYy",
	"./id": "UDhR",
	"./id.js": "UDhR",
	"./is": "BVg3",
	"./is.js": "BVg3",
	"./it": "bpih",
	"./it-ch": "bxKX",
	"./it-ch.js": "bxKX",
	"./it.js": "bpih",
	"./ja": "B55N",
	"./ja.js": "B55N",
	"./jv": "tUCv",
	"./jv.js": "tUCv",
	"./ka": "IBtZ",
	"./ka.js": "IBtZ",
	"./kk": "bXm7",
	"./kk.js": "bXm7",
	"./km": "6B0Y",
	"./km.js": "6B0Y",
	"./kn": "PpIw",
	"./kn.js": "PpIw",
	"./ko": "Ivi+",
	"./ko.js": "Ivi+",
	"./ku": "JCF/",
	"./ku.js": "JCF/",
	"./ky": "lgnt",
	"./ky.js": "lgnt",
	"./lb": "RAwQ",
	"./lb.js": "RAwQ",
	"./lo": "sp3z",
	"./lo.js": "sp3z",
	"./lt": "JvlW",
	"./lt.js": "JvlW",
	"./lv": "uXwI",
	"./lv.js": "uXwI",
	"./me": "KTz0",
	"./me.js": "KTz0",
	"./mi": "aIsn",
	"./mi.js": "aIsn",
	"./mk": "aQkU",
	"./mk.js": "aQkU",
	"./ml": "AvvY",
	"./ml.js": "AvvY",
	"./mn": "lYtQ",
	"./mn.js": "lYtQ",
	"./mr": "Ob0Z",
	"./mr.js": "Ob0Z",
	"./ms": "6+QB",
	"./ms-my": "ZAMP",
	"./ms-my.js": "ZAMP",
	"./ms.js": "6+QB",
	"./mt": "G0Uy",
	"./mt.js": "G0Uy",
	"./my": "honF",
	"./my.js": "honF",
	"./nb": "bOMt",
	"./nb.js": "bOMt",
	"./ne": "OjkT",
	"./ne.js": "OjkT",
	"./nl": "+s0g",
	"./nl-be": "2ykv",
	"./nl-be.js": "2ykv",
	"./nl.js": "+s0g",
	"./nn": "uEye",
	"./nn.js": "uEye",
	"./oc-lnc": "Fnuy",
	"./oc-lnc.js": "Fnuy",
	"./pa-in": "8/+R",
	"./pa-in.js": "8/+R",
	"./pl": "jVdC",
	"./pl.js": "jVdC",
	"./pt": "8mBD",
	"./pt-br": "0tRk",
	"./pt-br.js": "0tRk",
	"./pt.js": "8mBD",
	"./ro": "lyxo",
	"./ro.js": "lyxo",
	"./ru": "lXzo",
	"./ru.js": "lXzo",
	"./sd": "Z4QM",
	"./sd.js": "Z4QM",
	"./se": "//9w",
	"./se.js": "//9w",
	"./si": "7aV9",
	"./si.js": "7aV9",
	"./sk": "e+ae",
	"./sk.js": "e+ae",
	"./sl": "gVVK",
	"./sl.js": "gVVK",
	"./sq": "yPMs",
	"./sq.js": "yPMs",
	"./sr": "zx6S",
	"./sr-cyrl": "E+lV",
	"./sr-cyrl.js": "E+lV",
	"./sr.js": "zx6S",
	"./ss": "Ur1D",
	"./ss.js": "Ur1D",
	"./sv": "X709",
	"./sv.js": "X709",
	"./sw": "dNwA",
	"./sw.js": "dNwA",
	"./ta": "PeUW",
	"./ta.js": "PeUW",
	"./te": "XLvN",
	"./te.js": "XLvN",
	"./tet": "V2x9",
	"./tet.js": "V2x9",
	"./tg": "Oxv6",
	"./tg.js": "Oxv6",
	"./th": "EOgW",
	"./th.js": "EOgW",
	"./tk": "Wv91",
	"./tk.js": "Wv91",
	"./tl-ph": "Dzi0",
	"./tl-ph.js": "Dzi0",
	"./tlh": "z3Vd",
	"./tlh.js": "z3Vd",
	"./tr": "DoHr",
	"./tr.js": "DoHr",
	"./tzl": "z1FC",
	"./tzl.js": "z1FC",
	"./tzm": "wQk9",
	"./tzm-latn": "tT3J",
	"./tzm-latn.js": "tT3J",
	"./tzm.js": "wQk9",
	"./ug-cn": "YRex",
	"./ug-cn.js": "YRex",
	"./uk": "raLr",
	"./uk.js": "raLr",
	"./ur": "UpQW",
	"./ur.js": "UpQW",
	"./uz": "Loxo",
	"./uz-latn": "AQ68",
	"./uz-latn.js": "AQ68",
	"./uz.js": "Loxo",
	"./vi": "KSF8",
	"./vi.js": "KSF8",
	"./x-pseudo": "/X5v",
	"./x-pseudo.js": "/X5v",
	"./yo": "fzPg",
	"./yo.js": "fzPg",
	"./zh-cn": "XDpg",
	"./zh-cn.js": "XDpg",
	"./zh-hk": "SatO",
	"./zh-hk.js": "SatO",
	"./zh-mo": "OmwH",
	"./zh-mo.js": "OmwH",
	"./zh-tw": "kOpN",
	"./zh-tw.js": "kOpN"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "RnhZ";

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "54vc");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "VYYF");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./nExamHall-Services/local-storage.service */ "2eCX");
/* harmony import */ var _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./nExamHall-Services/theme-switcher.service */ "Fql8");









let AppComponent = class AppComponent {
    constructor(platform, splashScreen, statusBar, localStorage, themeSwitcherService) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.localStorage = localStorage;
        this.themeSwitcherService = themeSwitcherService;
        this.initializeApp();
    }
    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"] },
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_7__["LocalStorageService"] },
    { type: _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_8__["ThemeSwitcherService"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);



/***/ }),

/***/ "VpEx":
/*!***************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/profile/profile.component.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL3Byb2ZpbGUvcHJvZmlsZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n");

/***/ }),

/***/ "Xyc/":
/*!************************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.page.ts ***!
  \************************************************************************************************************************/
/*! exports provided: NExamHallCandidateCoreModulePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NExamHallCandidateCoreModulePage", function() { return NExamHallCandidateCoreModulePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_n_exam_hall_candidate_core_module_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./n-exam-hall-candidate-core-module.page.html */ "8mUz");
/* harmony import */ var _n_exam_hall_candidate_core_module_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./n-exam-hall-candidate-core-module.page.scss */ "qude");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let NExamHallCandidateCoreModulePage = class NExamHallCandidateCoreModulePage {
    constructor() { }
    ngOnInit() {
    }
};
NExamHallCandidateCoreModulePage.ctorParameters = () => [];
NExamHallCandidateCoreModulePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-n-exam-hall-candidate-core-module',
        template: _raw_loader_n_exam_hall_candidate_core_module_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_n_exam_hall_candidate_core_module_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NExamHallCandidateCoreModulePage);



/***/ }),

/***/ "YOHI":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/analysis-chart/analysis-chart.component.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"secondary\">\n      <ion-button>\n        <ion-icon name=\"notifications-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"text-center\"> Analysis</ion-title>\n  </ion-toolbar>\n</ion-header>-->\n<ion-content>\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n      <ion-menu-button autoHide=\"false\" class=\"headerButton\"></ion-menu-button>\n      \n    </div>\n    <div class=\"pageTitle\">\n      Analysis\n    </div>\n    <div class=\"right\">\n        <button  class=\"headerButton\">\n            <ion-icon class=\"icon\" name=\"notifications-outline\"></ion-icon>\n            <span class=\"badge badge-danger\"></span>\n        </button>\n    </div>\n</div>\n<div class=\"main\">\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "54vc");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "VYYF");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var _nExamHall_Providers_interceptor_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./nExamHall-Providers/interceptor.service */ "jlwF");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./nExamHall-Services/theme-switcher.service */ "Fql8");
/* harmony import */ var _nExamHall_Custom_Components_nExamhall_custom_component_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./nExamHall-Custom-Components/nExamhall-custom-component.module */ "FBZy");
/* harmony import */ var _nExamHall_Candidate_Module_n_exam_hall_candidate_core_module_n_exam_hall_candidate_core_module_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.module */ "vW0O");















let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_9__["IonicStorageModule"].forRoot(),
            _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"],
            _nExamHall_Candidate_Module_n_exam_hall_candidate_core_module_n_exam_hall_candidate_core_module_module__WEBPACK_IMPORTED_MODULE_14__["NExamHallCandidateCoreModulePageModule"],
            _nExamHall_Custom_Components_nExamhall_custom_component_module__WEBPACK_IMPORTED_MODULE_13__["nExamHallcustomComponentsModule"]
        ],
        providers: [
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
            _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_12__["ThemeSwitcherService"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] },
            {
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HTTP_INTERCEPTORS"], useClass: _nExamHall_Providers_interceptor_service__WEBPACK_IMPORTED_MODULE_10__["InterceptorService"], multi: true
            }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "ZDsB":
/*!*****************************************************************!*\
  !*** ./src/app/nExamHall-Providers/onboarding-check.service.ts ***!
  \*****************************************************************/
/*! exports provided: OnboardingCheckService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnboardingCheckService", function() { return OnboardingCheckService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "e8h1");




let OnboardingCheckService = class OnboardingCheckService {
    constructor(storage, router) {
        this.storage = storage;
        this.router = router;
    }
    canLoad() {
        return this.storage.get('ion_did_onboarding').then(res => {
            if (res) {
                // this.router.navigate(['/app', 'tabs', 'schedule']);
                this.router.navigate(['/nexamhallcore/auth',]);
                return false;
            }
            else {
                return true;
            }
        });
    }
};
OnboardingCheckService.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
OnboardingCheckService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], OnboardingCheckService);



/***/ }),

/***/ "ZVIx":
/*!**********************************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module-routing.module.ts ***!
  \**********************************************************************************************************************************/
/*! exports provided: NExamHallCandidateCoreModulePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NExamHallCandidateCoreModulePageRoutingModule", function() { return NExamHallCandidateCoreModulePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _candidate_dashboard_candidate_dashboard_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../candidate-dashboard/candidate-dashboard.page */ "23nY");
/* harmony import */ var _myexams_myexams_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../myexams/myexams.page */ "8VBk");
/* harmony import */ var _analysis_chart_analysis_chart_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../analysis-chart/analysis-chart.component */ "P/7j");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../profile/profile.component */ "bwnV");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../settings/settings.component */ "phol");








const routes = [
    {
        path: 'dashboard',
        component: _candidate_dashboard_candidate_dashboard_page__WEBPACK_IMPORTED_MODULE_3__["CandidateDashboardPage"]
    },
    {
        path: 'myexams',
        //component: MyexamsPage,
        children: [
            {
                path: '',
                component: _myexams_myexams_page__WEBPACK_IMPORTED_MODULE_4__["MyexamsPage"],
            },
            {
                path: 'mockexamlist/:examid/:testseriesid',
                //component:MocklistComponent,
                loadChildren: () => __webpack_require__.e(/*! import() | mockexamlist-mockexamlist-module */ "mockexamlist-mockexamlist-module").then(__webpack_require__.bind(null, /*! ../mockexamlist/mockexamlist.module */ "xZir")).then(m => m.MockexamlistPageModule)
            },
            {
                path: 'testseries/:examid',
                loadChildren: () => __webpack_require__.e(/*! import() | testseries-testseries-module */ "testseries-testseries-module").then(__webpack_require__.bind(null, /*! ../testseries/testseries.module */ "AG1y")).then(m => m.TestseriesPageModule)
            },
            {
                path: 'intructionpage/:mockid/:examstatus/:examseq',
                loadChildren: () => __webpack_require__.e(/*! import() | examinstruction-examinstruction-module */ "examinstruction-examinstruction-module").then(__webpack_require__.bind(null, /*! ../examinstruction/examinstruction.module */ "zy6S")).then(m => m.ExaminstructionPageModule)
            },
            {
                path: 'examconductor/:mockid/:examstatus/:examseq',
                loadChildren: () => Promise.all(/*! import() | examconductor-examconductor-module */[__webpack_require__.e("common"), __webpack_require__.e("examconductor-examconductor-module")]).then(__webpack_require__.bind(null, /*! ../examconductor/examconductor.module */ "uipB")).then(m => m.ExamconductorPageModule)
            }
        ]
    },
    {
        path: 'analysischart',
        component: _analysis_chart_analysis_chart_component__WEBPACK_IMPORTED_MODULE_5__["AnalysisChartComponent"]
    },
    {
        path: 'profile',
        component: _profile_profile_component__WEBPACK_IMPORTED_MODULE_6__["ProfileComponent"]
    },
    {
        path: 'settings',
        component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_7__["SettingsComponent"]
    },
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    }
];
let NExamHallCandidateCoreModulePageRoutingModule = class NExamHallCandidateCoreModulePageRoutingModule {
};
NExamHallCandidateCoreModulePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NExamHallCandidateCoreModulePageRoutingModule);



/***/ }),

/***/ "Zfcf":
/*!**************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/bottom-menu-bar/bottom-menu-bar.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: BottomMenuBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BottomMenuBarComponent", function() { return BottomMenuBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_bottom_menu_bar_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./bottom-menu-bar.component.html */ "9lZg");
/* harmony import */ var _bottom_menu_bar_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./bottom-menu-bar.component.scss */ "2LUc");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let BottomMenuBarComponent = class BottomMenuBarComponent {
    constructor() { }
    ngOnInit() { }
};
BottomMenuBarComponent.ctorParameters = () => [];
BottomMenuBarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-bottom-menu-bar',
        template: _raw_loader_bottom_menu_bar_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_bottom_menu_bar_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], BottomMenuBarComponent);



/***/ }),

/***/ "aodY":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/myexams/myexams.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button autoHide=\"false\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons slot=\"secondary\">\n      <ion-button>\n        <ion-icon name=\"notifications-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"text-center\">My Exams </ion-title>\n  </ion-toolbar>\n</ion-header>-->\n<ion-header class=\"ion-no-border\">\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n      <ion-menu-button autoHide=\"false\" class=\"headerButton\"></ion-menu-button>\n      \n    </div>\n    <div class=\"pageTitle\">\n      My Exams\n    </div>\n    <div class=\"right\">\n        <button  class=\"headerButton\">\n            <ion-icon class=\"icon\" name=\"notifications-outline\"></ion-icon>\n            <span class=\"badge badge-danger\"></span>\n        </button>\n    </div>\n</div>\n</ion-header>\n\n<ion-content>\n  \n<div class=\"main\">\n  <div class=\"section wallet-card-section pt-1\">\n    <div class=\"section-header\">\n      <ion-searchbar\n        mode=\"ios\"\n        [(ngModel)]=\"queryText\"\n        (ionChange)=\"filterExams()\"\n        placeholder=\"Search\"\n      ></ion-searchbar>\n    </div>\n  </div>\n  <ion-grid class=\"my-3\" fixed>\n    <ion-row>\n      <ion-col size=\"6\" size-md=\"6\" *ngFor=\"let data of selected_Exam\">\n        <div\n          class=\"card exam-list-card\" mode=\"ios\"\n          [ngStyle]=\"{ 'background-image': data.background_color }\"\n        >\n          <div class=\"py-2 text-center\">\n            <img\n              class=\"d-block mx-auto mb-2 img-circle\"\n              [src]=\"data.examimage\"\n              alt=\"{{ data.examname }}\"\n              (error)=\"setDefaultPic(data.examname, 60, 1)\"\n              width=\"60\"\n              height=\"60\"\n            />\n            <h6 class=\"card-title\">{{ data.examname }}</h6>\n          </div>\n          <div class=\"card-content\">\n            <ul>\n              <li\n                class=\"list-group-item d-flex justify-content-between lh-condensed border-none\"\n                (click)=\"gomocklist(data.examid, data.mockexamslength)\"\n              >\n                <div>\n                  <p class=\"my-0\">Mock Test</p>\n                  <small class=\"text-muted\"></small>\n                </div>\n                <span class=\"text-muted number-palate\">\n                  {{ data.mockexamslength }} Tests\n                  <i class=\"ep-icon fa fa-angle-double-right\"></i>\n                </span>\n              </li>\n              <li\n                class=\"list-group-item d-flex justify-content-between lh-condensed border-none\"\n                (click)=\"goTestSeries(data.examid, data.testserieslength)\"\n                ng-if=\"data.testseriessupport\"\n              >\n                <div>\n                  <p class=\"my-0\">Test Series</p>\n                  <small class=\"text-muted\"></small>\n                </div>\n                <span class=\"text-muted\"\n                  >{{ data.testserieslength }} Series\n                  <i class=\"ep-icon fa fa-angle-double-right\"></i>\n                </span>\n              </li>\n              <li\n              class=\"list-group-item d-flex justify-content-between lh-condensed border-none\"\n              (click)=\"goLiveTest(data.examid,data.livemocktestlength)\"\n              ng-if=\"data.livemocktestsupport\"\n            >\n              <div>\n                <p class=\"my-0\">Live Test</p>\n                <small class=\"text-muted\"></small>\n              </div>\n              <span class=\"text-muted\"\n                >{{ data.livemocktestlength }} Tests\n                <i class=\"ep-icon fa fa-angle-double-right\"></i>\n              </span>\n            </li>\n            </ul>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n \n</div>\n\n</ion-content>\n");

/***/ }),

/***/ "bFVc":
/*!*****************************************************!*\
  !*** ./src/app/nExamHall-Services/alert.service.ts ***!
  \*****************************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");



let AlertService = class AlertService {
    constructor(alertController) {
        this.alertController = alertController;
    }
    presentAlert(msg) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Warning',
                message: msg,
                buttons: ['OK'],
                mode: 'ios'
            });
            yield alert.present();
        });
    }
    presentAlertMultipleButtons() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Alert',
                subHeader: 'Subtitle',
                message: 'This is an alert message.',
                buttons: ['Cancel', 'Open Modal', 'Delete'],
                mode: 'ios'
            });
            yield alert.present();
        });
    }
    presentAlertConfirm() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Confirm!',
                message: 'Message <strong>text</strong>!!!',
                mode: 'ios',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            console.log('Confirm Cancel: blah');
                        }
                    }, {
                        text: 'Okay',
                        handler: () => {
                            console.log('Confirm Okay');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentAlertPrompt() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Prompt!',
                mode: 'ios',
                inputs: [
                    {
                        name: 'name1',
                        type: 'text',
                        placeholder: 'Placeholder 1'
                    },
                    {
                        name: 'name2',
                        type: 'text',
                        id: 'name2-id',
                        value: 'hello',
                        placeholder: 'Placeholder 2'
                    },
                    {
                        name: 'name3',
                        value: 'http://ionicframework.com',
                        type: 'url',
                        placeholder: 'Favorite site ever'
                    },
                    // input date with min & max
                    {
                        name: 'name4',
                        type: 'date',
                        min: '2017-03-01',
                        max: '2018-01-12'
                    },
                    // input date without min nor max
                    {
                        name: 'name5',
                        type: 'date'
                    },
                    {
                        name: 'name6',
                        type: 'number',
                        min: -5,
                        max: 10
                    },
                    {
                        name: 'name7',
                        type: 'number'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    }, {
                        text: 'Ok',
                        handler: () => {
                            console.log('Confirm Ok');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentAlertRadio() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Radio',
                mode: 'ios',
                inputs: [
                    {
                        name: 'radio1',
                        type: 'radio',
                        label: 'Radio 1',
                        value: 'value1',
                        checked: true
                    },
                    {
                        name: 'radio2',
                        type: 'radio',
                        label: 'Radio 2',
                        value: 'value2'
                    },
                    {
                        name: 'radio3',
                        type: 'radio',
                        label: 'Radio 3',
                        value: 'value3'
                    },
                    {
                        name: 'radio4',
                        type: 'radio',
                        label: 'Radio 4',
                        value: 'value4'
                    },
                    {
                        name: 'radio5',
                        type: 'radio',
                        label: 'Radio 5',
                        value: 'value5'
                    },
                    {
                        name: 'radio6',
                        type: 'radio',
                        label: 'Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 ',
                        value: 'value6'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    }, {
                        text: 'Ok',
                        handler: () => {
                            console.log('Confirm Ok');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentAlertCheckbox() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Checkbox',
                mode: 'ios',
                inputs: [
                    {
                        name: 'checkbox1',
                        type: 'checkbox',
                        label: 'Checkbox 1',
                        value: 'value1',
                        checked: true
                    },
                    {
                        name: 'checkbox2',
                        type: 'checkbox',
                        label: 'Checkbox 2',
                        value: 'value2'
                    },
                    {
                        name: 'checkbox3',
                        type: 'checkbox',
                        label: 'Checkbox 3',
                        value: 'value3'
                    },
                    {
                        name: 'checkbox4',
                        type: 'checkbox',
                        label: 'Checkbox 4',
                        value: 'value4'
                    },
                    {
                        name: 'checkbox5',
                        type: 'checkbox',
                        label: 'Checkbox 5',
                        value: 'value5'
                    },
                    {
                        name: 'checkbox6',
                        type: 'checkbox',
                        label: 'Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6',
                        value: 'value6'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    }, {
                        text: 'Ok',
                        handler: () => {
                            console.log('Confirm Ok');
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
};
AlertService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
AlertService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AlertService);



/***/ }),

/***/ "bsB4":
/*!****************************************************************************************!*\
  !*** ./src/app/nExamHall-Custom-Components/otp-pin-screen/otp-pin-screen.component.ts ***!
  \****************************************************************************************/
/*! exports provided: OtpPinScreenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OtpPinScreenComponent", function() { return OtpPinScreenComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_otp_pin_screen_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./otp-pin-screen.component.html */ "mxE/");
/* harmony import */ var _otp_pin_screen_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./otp-pin-screen.component.scss */ "snt7");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let OtpPinScreenComponent = class OtpPinScreenComponent {
    constructor() {
        this.pagetitle = "Enter OTP";
        this.pin = "";
        this.change = new _angular_core__WEBPACK_IMPORTED_MODULE_3__["EventEmitter"]();
    }
    emitEvent() {
        this.change.emit(this.pin);
    }
    handleInput(pin) {
        if (pin === "clear") {
            this.pin = "";
            return;
        }
        if (this.pin.length === 4) {
            return;
        }
        this.pin += pin;
    }
    ngOnInit() { }
};
OtpPinScreenComponent.ctorParameters = () => [];
OtpPinScreenComponent.propDecorators = {
    pagetitle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    change: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Output"] }]
};
OtpPinScreenComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-otp-pin-screen',
        template: _raw_loader_otp_pin_screen_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_otp_pin_screen_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], OtpPinScreenComponent);



/***/ }),

/***/ "bwnV":
/*!*************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/profile/profile.component.ts ***!
  \*************************************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./profile.component.html */ "oDtP");
/* harmony import */ var _profile_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile.component.scss */ "VpEx");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../nExamHall-Services/local-storage.service */ "2eCX");





let ProfileComponent = class ProfileComponent {
    constructor(localStorage) {
        this.localStorage = localStorage;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        //debugger;
        this.localStorage.get('theme').then((res) => {
            this.theme = res;
        });
    }
};
ProfileComponent.ctorParameters = () => [
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"] }
];
ProfileComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-profile',
        template: _raw_loader_profile_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_profile_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ProfileComponent);



/***/ }),

/***/ "fdAZ":
/*!******************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/mainviewcontainer/mainviewcontainer.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: MainviewcontainerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainviewcontainerComponent", function() { return MainviewcontainerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_mainviewcontainer_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./mainviewcontainer.component.html */ "ptTE");
/* harmony import */ var _mainviewcontainer_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mainviewcontainer.component.scss */ "npcT");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../nExamHall-Services/theme-switcher.service */ "Fql8");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../nExamHall-Services/local-storage.service */ "2eCX");






let MainviewcontainerComponent = class MainviewcontainerComponent {
    constructor(themeSwitcherService, localStorage) {
        this.themeSwitcherService = themeSwitcherService;
        this.localStorage = localStorage;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.localStorage.get('theme').then((res) => {
            this.theme = res;
        });
    }
};
MainviewcontainerComponent.ctorParameters = () => [
    { type: _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_4__["ThemeSwitcherService"] },
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__["LocalStorageService"] }
];
MainviewcontainerComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-mainviewcontainer',
        template: _raw_loader_mainviewcontainer_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_mainviewcontainer_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MainviewcontainerComponent);



/***/ }),

/***/ "fyTe":
/*!*****************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/settings/settings.component.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL3NldHRpbmdzL3NldHRpbmdzLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "jlwF":
/*!************************************************************!*\
  !*** ./src/app/nExamHall-Providers/interceptor.service.ts ***!
  \************************************************************/
/*! exports provided: InterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterceptorService", function() { return InterceptorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "kU1M");








let InterceptorService = class InterceptorService {
    constructor(alertController, storage) {
        this.alertController = alertController;
        this.storage = storage;
    }
    intercept(request, next) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["from"])(this.storage.get('token'))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(token => {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
            request = request.clone({ url: request.url });
            return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((event) => {
                if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]) {
                    // do nothing for now
                }
                return event;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])((error) => {
                const status = error.status;
                const reason = error && error.error.reason ? error.error.reason : '';
                this.presentAlert(status, reason);
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["throwError"])(error);
            }));
        }));
    }
    presentAlert(status, reason) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: status + ' Error',
                subHeader: 'Subtitle',
                message: reason,
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
};
InterceptorService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] }
];
InterceptorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], InterceptorService);



/***/ }),

/***/ "kLfG":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		"dUtr",
		"common",
		0
	],
	"./ion-alert.entry.js": [
		"Q8AI",
		"common",
		1
	],
	"./ion-app_8.entry.js": [
		"hgI1",
		"common",
		2
	],
	"./ion-avatar_3.entry.js": [
		"CfoV",
		"common",
		3
	],
	"./ion-back-button.entry.js": [
		"Nt02",
		"common",
		4
	],
	"./ion-backdrop.entry.js": [
		"Q2Bp",
		5
	],
	"./ion-button_2.entry.js": [
		"0Pbj",
		"common",
		6
	],
	"./ion-card_5.entry.js": [
		"ydQj",
		"common",
		7
	],
	"./ion-checkbox.entry.js": [
		"4fMi",
		"common",
		8
	],
	"./ion-chip.entry.js": [
		"czK9",
		"common",
		9
	],
	"./ion-col_3.entry.js": [
		"/CAe",
		10
	],
	"./ion-datetime_3.entry.js": [
		"WgF3",
		"common",
		11
	],
	"./ion-fab_3.entry.js": [
		"uQcF",
		"common",
		12
	],
	"./ion-img.entry.js": [
		"wHD8",
		13
	],
	"./ion-infinite-scroll_2.entry.js": [
		"2lz6",
		14
	],
	"./ion-input.entry.js": [
		"ercB",
		"common",
		15
	],
	"./ion-item-option_3.entry.js": [
		"MGMP",
		"common",
		16
	],
	"./ion-item_8.entry.js": [
		"9bur",
		"common",
		17
	],
	"./ion-loading.entry.js": [
		"cABk",
		"common",
		18
	],
	"./ion-menu_3.entry.js": [
		"kyFE",
		"common",
		19
	],
	"./ion-modal.entry.js": [
		"TvZU",
		"common",
		20
	],
	"./ion-nav_2.entry.js": [
		"vnES",
		"common",
		21
	],
	"./ion-popover.entry.js": [
		"qCuA",
		"common",
		22
	],
	"./ion-progress-bar.entry.js": [
		"0tOe",
		"common",
		23
	],
	"./ion-radio_2.entry.js": [
		"h11V",
		"common",
		24
	],
	"./ion-range.entry.js": [
		"XGij",
		"common",
		25
	],
	"./ion-refresher_2.entry.js": [
		"nYbb",
		"common",
		26
	],
	"./ion-reorder_2.entry.js": [
		"smMY",
		"common",
		27
	],
	"./ion-ripple-effect.entry.js": [
		"STjf",
		28
	],
	"./ion-route_4.entry.js": [
		"k5eQ",
		"common",
		29
	],
	"./ion-searchbar.entry.js": [
		"OR5t",
		"common",
		30
	],
	"./ion-segment_2.entry.js": [
		"fSgp",
		"common",
		31
	],
	"./ion-select_3.entry.js": [
		"lfGF",
		"common",
		32
	],
	"./ion-slide_2.entry.js": [
		"5xYT",
		33
	],
	"./ion-spinner.entry.js": [
		"nI0H",
		"common",
		34
	],
	"./ion-split-pane.entry.js": [
		"NAQR",
		35
	],
	"./ion-tab-bar_2.entry.js": [
		"knkW",
		"common",
		36
	],
	"./ion-tab_2.entry.js": [
		"TpdJ",
		"common",
		37
	],
	"./ion-text.entry.js": [
		"ISmu",
		"common",
		38
	],
	"./ion-textarea.entry.js": [
		"U7LX",
		"common",
		39
	],
	"./ion-toast.entry.js": [
		"L3sA",
		"common",
		40
	],
	"./ion-toggle.entry.js": [
		"IUOf",
		"common",
		41
	],
	"./ion-virtual-scroll.entry.js": [
		"8Mb5",
		42
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "kLfG";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "mIMV":
/*!**********************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/myexams/myexams.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".exam-list-card {\n  background: linear-gradient(to bottom, rgba(10, 208, 244, 0.2) 0, rgba(255, 255, 255, 0.54) 30%, #fff 100%);\n  box-shadow: -1px 1px 4px 0 rgba(117, 138, 172, 0.12);\n  border: none;\n}\n\n.exam-list-card h6 {\n  font-weight: 600;\n  font-size: 0.9rem;\n  margin-bottom: 0px;\n}\n\n.exam-list-card ul {\n  padding: 0;\n  margin: 0;\n  list-style: none;\n  border-top: 1px solid #f1f4f6;\n  margin: 15px 0;\n  padding: 15px 0 0;\n  min-height: 100px;\n  flex: 1;\n}\n\n.exam-list-card ul .list-group-item {\n  padding: 0.5rem 0.5rem 0.5rem 0.5rem;\n  border: none;\n}\n\n.list-group-item:first-child {\n  border-top-left-radius: 0.25rem;\n  border-top-right-radius: 0.25rem;\n}\n\n.list-group-item:first-child {\n  border-top-left-radius: 0.25rem;\n  border-top-right-radius: 0.25rem;\n}\n\n.exam-list-card ul li {\n  padding-bottom: 3px;\n  cursor: pointer;\n  background: transparent;\n}\n\n.exam-list-card ul li p {\n  font-size: 0.7em;\n}\n\n.exam-list-card ul li .number-palate {\n  font-size: 0.9em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvbXlleGFtcy9teWV4YW1zLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdJLDJHQUFBO0VBRUEsb0RBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUU7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFRTtFQUNFLFVBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSw2QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBR0EsT0FBQTtBQUNKOztBQUFJO0VBQ0ksb0NBQUE7RUFDQSxZQUFBO0FBRVI7O0FBRUU7RUFDRSwrQkFBQTtFQUNBLGdDQUFBO0FBQ0o7O0FBRUU7RUFDRSwrQkFBQTtFQUNBLGdDQUFBO0FBQ0o7O0FBRUU7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUFJO0VBQ0ksZ0JBQUE7QUFFUjs7QUFBSTtFQUNJLGdCQUFBO0FBRVIiLCJmaWxlIjoic3JjL2FwcC9uRXhhbUhhbGwtQ2FuZGlkYXRlLU1vZHVsZS9teWV4YW1zL215ZXhhbXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW0tbGlzdC1jYXJkIHtcclxuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtZ3JhZGllbnQobGluZWFyLCBsZWZ0IHRvcCwgbGVmdCBib3R0b20sIGZyb20ocmdiYSgxMCwgMjA4LCAyNDQsIC4yKSksIGNvbG9yLXN0b3AoMzAlLCByZ2JhKDI1NSwgMjU1LCAyNTUsIC41NCkpLCB0byh3aGl0ZSkpO1xyXG4gICAgYmFja2dyb3VuZDogLW8tbGluZWFyLWdyYWRpZW50KHRvcCwgcmdiYSgxMCwgMjA4LCAyNDQsIC4yKSAwLCByZ2JhKDI1NSwgMjU1LCAyNTUsIC41NCkgMzAlLCAjZmZmIDEwMCUpO1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSgxMCwgMjA4LCAyNDQsIC4yKSAwLCByZ2JhKDI1NSwgMjU1LCAyNTUsIC41NCkgMzAlLCAjZmZmIDEwMCUpO1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAtMXB4IDFweCA0cHggMCByZ2JhKDExNywgMTM4LCAxNzIsIC4xMik7XHJcbiAgICBib3gtc2hhZG93OiAtMXB4IDFweCA0cHggMCByZ2JhKDExNywgMTM4LCAxNzIsIC4xMik7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtLWxpc3QtY2FyZCBoNiB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgZm9udC1zaXplOiAwLjlyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtLWxpc3QtY2FyZCB1bCB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZjFmNGY2O1xyXG4gICAgbWFyZ2luOiAxNXB4IDA7XHJcbiAgICBwYWRkaW5nOiAxNXB4IDAgMDtcclxuICAgIG1pbi1oZWlnaHQ6IDEwMHB4O1xyXG4gICAgLXdlYmtpdC1ib3gtZmxleDogMTtcclxuICAgIC1tcy1mbGV4OiAxO1xyXG4gICAgZmxleDogMTtcclxuICAgIC5saXN0LWdyb3VwLWl0ZW17XHJcbiAgICAgICAgcGFkZGluZzogMC41cmVtIDAuNXJlbSAwLjVyZW0gMC41cmVtO1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgIH1cclxuICB9XHJcbiAgXHJcbiAgLmxpc3QtZ3JvdXAtaXRlbTpmaXJzdC1jaGlsZCB7XHJcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAuMjVyZW07XHJcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogLjI1cmVtO1xyXG4gIH1cclxuICBcclxuICAubGlzdC1ncm91cC1pdGVtOmZpcnN0LWNoaWxkIHtcclxuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IC4yNXJlbTtcclxuICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAuMjVyZW07XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtLWxpc3QtY2FyZCB1bCBsaSB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogM3B4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMC43ZW07XHJcbiAgICB9XHJcbiAgICAubnVtYmVyLXBhbGF0ZXtcclxuICAgICAgICBmb250LXNpemU6IDAuOWVtO1xyXG4gICAgfVxyXG4gIH0iXX0= */");

/***/ }),

/***/ "mxE/":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Custom-Components/otp-pin-screen/otp-pin-screen.component.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content padding>\n  <h6 text-center margin-vertical>{{ pagetitle }}</h6>\n  <div class=\"text-center large margin-vertical\">\n     <ion-icon [name]=\"pin.length>0 ? 'radio-button-on' : 'radio-button-off'\"></ion-icon>\n    <ion-icon [name]=\"pin.length>1 ? 'radio-button-on' : 'radio-button-off'\"></ion-icon>\n    <ion-icon [name]=\"pin.length>2 ? 'radio-button-on' : 'radio-button-off'\"></ion-icon>\n    <ion-icon [name]=\"pin.length>3 ? 'radio-button-on' : 'radio-button-off'\"></ion-icon> \n  </div>\n\n  <div>\n    <ion-grid class=\"text-center\">\n      <ion-row>\n        <ion-col>\n          <ion-button  shape=\"round\" large outline (click)=\"handleInput('1')\">1</ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('2')\">2</ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('3')\">3</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('4')\">4</ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('5')\">5</ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('6')\">6</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('7')\">7</ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('8')\">8</ion-button>\n        </ion-col>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('9')\">9</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n\n        </ion-col>\n        <ion-col>\n          <ion-button  shape=\"round\"  outline (click)=\"handleInput('0')\">0</ion-button>\n        </ion-col>\n        <ion-col>\n\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button  no-padding (click)=\"handleInput('clear')\">Clear</ion-button>\n        </ion-col>\n        <ion-col>\n\n        </ion-col>\n        <ion-col>\n          <ion-button  (click)=\"emitEvent()\">OK</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-content>");

/***/ }),

/***/ "npcT":
/*!********************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/mainviewcontainer/mainviewcontainer.component.scss ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL25FeGFtSGFsbC1Db3JlLVV0aWxzL21haW52aWV3Y29udGFpbmVyL21haW52aWV3Y29udGFpbmVyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "oDtP":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/profile/profile.component.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div class=\"appHeader  text-light\" [ngClass]=\"theme\">\n    <div class=\"left\">\n      <ion-menu-button autoHide=\"false\" class=\"headerButton\"></ion-menu-button>\n      \n    </div>\n    <div class=\"pageTitle\">\n     Profile\n    </div>\n    <div class=\"right\">\n        <button  class=\"headerButton\">\n            <ion-icon class=\"icon\" name=\"notifications-outline\"></ion-icon>\n            <span class=\"badge badge-danger\"></span>\n        </button>\n    </div>\n</div>\n<div class=\"main\">\n  </div>\n\n</ion-content>");

/***/ }),

/***/ "pEEP":
/*!**********************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/candidate-dashboard/candidate-dashboard.page.scss ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  position: relative;\n}\n\n.pb-6, .py-6 {\n  padding-bottom: 4.5rem !important;\n}\n\n.cross-header {\n  transform: rotate(-10deg);\n  height: 30vh;\n  margin-left: -25px;\n  margin-right: -20px;\n  margin-top: -40px;\n}\n\n.cross-header .user-avatar {\n  transform: rotate(10deg);\n  margin: 8%;\n}\n\n.user-avatar ion-avatar {\n  width: 15vw;\n  height: 6vh;\n}\n\n.user-avatar ion-avatar img {\n  width: 12vw;\n  height: 7vh;\n}\n\n.user-avatar ion-label {\n  color: #fff;\n  margin-left: 0px;\n}\n\n.user-avatar ion-label h3 {\n  font-size: 80%;\n}\n\n.user-avatar ion-label p {\n  font-size: 70%;\n}\n\n.dashboard-analysis-container .card .card-body {\n  padding: 0;\n}\n\n.dashboard-analysis-container .card .card-body .header {\n  padding: 15px 15px 0px 15px;\n}\n\n.dashboard-analysis-container .card .card-body .header .card-title {\n  font-size: 90%;\n}\n\n.dashboard-analysis-container .card .card-body .card-title {\n  margin-top: 0px;\n}\n\n.dashboard-analysis-container .card .card-body .chart-card {\n  height: 18vh;\n  margin-top: -10%;\n}\n\n.dashboard-analysis-container .card .col-auto {\n  margin-top: -12%;\n}\n\n.dashboard-analysis-container .card .col-auto .icon-shape {\n  display: inline-flex;\n  padding: 0px;\n  text-align: center;\n  border-radius: 50%;\n  align-items: center;\n  justify-content: center;\n  width: 3rem;\n  height: 3rem;\n  background: #fff;\n  color: rgba(var(--ion-color-primary-rgb));\n  font-size: 23px;\n  font-weight: 800;\n}\n\n.dashboard-analysis-container ion-text {\n  color: gray;\n  font-size: small;\n}\n\n.showmore {\n  height: 17vh;\n  overflow: hidden;\n}\n\n.showless {\n  height: auto;\n  overflow: hidden;\n}\n\n.progress-container ion-thumbnail {\n  width: 3vw;\n  height: 3vh;\n}\n\n.progress-container ion-thumbnail i {\n  font-size: 80%;\n  padding: 10px 5px;\n  min-width: 8vw;\n}\n\n.progress-container ion-label {\n  margin-left: 10px;\n}\n\n.progress-container ion-label h6 {\n  font-size: 70%;\n}\n\n.progress-container ion-label .progress {\n  height: 1vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvY2FuZGlkYXRlLWRhc2hib2FyZC9jYW5kaWRhdGUtZGFzaGJvYXJkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxpQ0FBQTtBQUVKOztBQUFBO0VBQ0kseUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FBR0o7O0FBREE7RUFDSSx3QkFBQTtFQUNBLFVBQUE7QUFJSjs7QUFGQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0FBS0o7O0FBSkk7RUFDSSxXQUFBO0VBQ0EsV0FBQTtBQU1SOztBQUhBO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0FBTUo7O0FBTEk7RUFDSSxjQUFBO0FBT1I7O0FBTEk7RUFDSSxjQUFBO0FBT1I7O0FBRVE7RUFRSSxVQUFBO0FBTlo7O0FBRFk7RUFDSSwyQkFBQTtBQUdoQjs7QUFEZ0I7RUFDSSxjQUFBO0FBR3BCOztBQUNZO0VBQ0ksZUFBQTtBQUNoQjs7QUFNWTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtBQUpoQjs7QUFPUTtFQUNJLGdCQUFBO0FBTFo7O0FBTVk7RUFDSSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5Q0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUpoQjs7QUFTSTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtBQVBSOztBQVVBO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0FBUEo7O0FBU0E7RUFDSSxZQUFBO0VBQ0EsZ0JBQUE7QUFOSjs7QUFVSTtFQUNJLFVBQUE7RUFDQSxXQUFBO0FBUFI7O0FBU1E7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBUFo7O0FBVUk7RUFDSSxpQkFBQTtBQVJSOztBQVNRO0VBQ0ksY0FBQTtBQVBaOztBQVNRO0VBQ0ksV0FBQTtBQVBaIiwiZmlsZSI6InNyYy9hcHAvbkV4YW1IYWxsLUNhbmRpZGF0ZS1Nb2R1bGUvY2FuZGlkYXRlLWRhc2hib2FyZC9jYW5kaWRhdGUtZGFzaGJvYXJkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXIge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5wYi02LCAucHktNiB7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogNC41cmVtIWltcG9ydGFudDtcclxufVxyXG4uY3Jvc3MtaGVhZGVye1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTEwZGVnKTtcclxuICAgIGhlaWdodDogMzB2aDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMjVweDtcclxuICAgIG1hcmdpbi1yaWdodDogLTIwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtNDBweDtcclxufVxyXG4uY3Jvc3MtaGVhZGVyIC51c2VyLWF2YXRhcntcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDEwZGVnKTtcclxuICAgIG1hcmdpbjogOCU7XHJcbn1cclxuLnVzZXItYXZhdGFyIGlvbi1hdmF0YXJ7XHJcbiAgICB3aWR0aDogMTV2dztcclxuICAgIGhlaWdodDogNnZoO1xyXG4gICAgaW1ne1xyXG4gICAgICAgIHdpZHRoOiAxMnZ3O1xyXG4gICAgICAgIGhlaWdodDogN3ZoO1xyXG4gICAgfVxyXG59XHJcbi51c2VyLWF2YXRhciBpb24tbGFiZWwge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgaDN7XHJcbiAgICAgICAgZm9udC1zaXplOiA4MCU7XHJcbiAgICB9XHJcbiAgICBwe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNzAlO1xyXG4gICAgfVxyXG4gICBcclxuICAgXHJcbn1cclxuLmRhc2hib2FyZC1hbmFseXNpcy1jb250YWluZXJ7XHJcbiAgICAgXHJcbiAgICAuY2FyZHtcclxuICAgICAgICBcclxuICAgICAgICAuY2FyZC1ib2R5e1xyXG4gICAgICAgICAgICAuaGVhZGVye1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMTVweCAxNXB4IDBweCAxNXB4O1xyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgLmNhcmQtdGl0bGV7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiA5MCU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICAgICAgLmNhcmQtdGl0bGV7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gLmNoYXJ0LWNvbnRhaW5lci1jYXJke1xyXG4gICAgICAgICAgICAvLyAgICAgLy8gaGVpZ2h0OiAxOHZoO1xyXG4gICAgICAgICAgICAvLyAgICAgLy8gb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLmNoYXJ0LWNhcmR7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDE4dmg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtMTAlO1xyXG4gICAgICAgICAgICB9IFxyXG4gICAgICAgIH1cclxuICAgICAgICAuY29sLWF1dG97XHJcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IC0xMiU7XHJcbiAgICAgICAgICAgIC5pY29uLXNoYXBlIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDNyZW07XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDNyZW07XHJcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHJnYmEodmFyKC0taW9uLWNvbG9yLXByaW1hcnktcmdiKSk7XHJcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogODAwO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGlvbi10ZXh0e1xyXG4gICAgICAgIGNvbG9yOmdyYXk7XHJcbiAgICAgICAgZm9udC1zaXplOiBzbWFsbDtcclxuICAgIH1cclxufVxyXG4uc2hvd21vcmV7XHJcbiAgICBoZWlnaHQ6IDE3dmg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcbi5zaG93bGVzc3tcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcbi5wcm9ncmVzcy1jb250YWluZXJ7XHJcbiAgICBpb24tdGh1bWJuYWlse1xyXG4gICAgICAgIHdpZHRoOiAzdnc7XHJcbiAgICAgICAgaGVpZ2h0OiAzdmg7XHJcblxyXG4gICAgICAgIGl7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogODAlO1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAxMHB4IDVweDtcclxuICAgICAgICAgICAgbWluLXdpZHRoOiA4dnc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgaW9uLWxhYmVse1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIGg2e1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDcwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnByb2dyZXNze1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDF2aDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbiJdfQ== */");

/***/ }),

/***/ "phol":
/*!***************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/settings/settings.component.ts ***!
  \***************************************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_settings_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./settings.component.html */ "6aOO");
/* harmony import */ var _settings_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./settings.component.scss */ "fyTe");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../nExamHall-Services/theme-switcher.service */ "Fql8");
/* harmony import */ var _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../nExamHall-Services/local-storage.service */ "2eCX");






let SettingsComponent = class SettingsComponent {
    constructor(themeSwitcherService, localStorage) {
        this.themeSwitcherService = themeSwitcherService;
        this.localStorage = localStorage;
        this.selectedcolor = 0;
        this.selectedMode = '1';
        this.theme = '';
        this.themeSwitcherService.intitialTheme().then((res) => {
            this.theme = res;
            this.themes = this.themeSwitcherService.getThemes();
        });
    }
    ngOnInit() { }
    themeColorSet(colorid) {
        debugger;
        let themeColor = this.themes.find(x => x.id == colorid);
        this.theme = themeColor.cssclass;
        console.log('themeColor', themeColor);
        this.localStorage.set('theme', this.theme);
    }
    colorMode(mode) {
        if (mode == 'auto') {
        }
        else {
            this.themeSwitcherService.setMode(mode);
        }
    }
};
SettingsComponent.ctorParameters = () => [
    { type: _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_4__["ThemeSwitcherService"] },
    { type: _nExamHall_Services_local_storage_service__WEBPACK_IMPORTED_MODULE_5__["LocalStorageService"] }
];
SettingsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-settings',
        template: _raw_loader_settings_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_settings_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SettingsComponent);



/***/ }),

/***/ "ptTE":
/*!**********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/mainviewcontainer/mainviewcontainer.component.html ***!
  \**********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-content>\n  <app-side-menu-bar></app-side-menu-bar>\n  <div>\n     \n  </div>\n  <app-bottom-menu-bar></app-bottom-menu-bar>\n \n</ion-content>");

/***/ }),

/***/ "qude":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.page.scss ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DYW5kaWRhdGUtTW9kdWxlL24tZXhhbS1oYWxsLWNhbmRpZGF0ZS1jb3JlLW1vZHVsZS9uLWV4YW0taGFsbC1jYW5kaWRhdGUtY29yZS1tb2R1bGUucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "rl9i":
/*!**********************************************************************************************!*\
  !*** ./src/app/nExamHall-Custom-Components/mockfilter-modal/mockfilter-modal.component.scss ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DdXN0b20tQ29tcG9uZW50cy9tb2NrZmlsdGVyLW1vZGFsL21vY2tmaWx0ZXItbW9kYWwuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "snt7":
/*!******************************************************************************************!*\
  !*** ./src/app/nExamHall-Custom-Components/otp-pin-screen/otp-pin-screen.component.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25FeGFtSGFsbC1DdXN0b20tQ29tcG9uZW50cy9vdHAtcGluLXNjcmVlbi9vdHAtcGluLXNjcmVlbi5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "teMF":
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/nExamHall-Candidate-Module/nExamHall-Core-Utils/side-menu-bar/side-menu-bar.component.html ***!
  \**************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu contentId=\"main-content\" (ionWillOpen)=\"initTheme()\">\n  <ion-content>\n    <div class=\"menu-header\" [ngClass]=\"theme\" ></div>\n    <div class=\"header-content\">\n      <img [src]=\"userimage\" alt=\"\" />\n      <ion-label>\n        <h2>{{username}}</h2>\n        <p>Last logged in -{{ lastlogin_date | date: mediumDate }}</p>\n      </ion-label>\n    </div>\n    <ion-list lines=\"none\">\n      <ion-list-header> Menu </ion-list-header>\n      <ion-menu-toggle\n        autoHide=\"false\"\n        *ngFor=\"let p of appPages; let i = index\"\n      >\n        <ion-item\n          [routerLink]=\"p.url\"\n          routerLinkActive=\"selected\"\n          routerDirection=\"root\"\n          detail=\"false\"\n        >\n          <ion-icon slot=\"start\" [name]=\"p.icon + '-outline'\"></ion-icon>\n          <ion-label>\n            {{ p.title }}\n          </ion-label>\n        </ion-item>\n      </ion-menu-toggle>\n    </ion-list>\n\n    <ion-list  lines=\"none\">\n      <ion-list-header> Account </ion-list-header>\n      <ion-menu-toggle autoHide=\"false\">\n        <ion-item\n          routerLink=\"/nexamhallcandidate/settings\"\n          routerLinkActive=\"selected\"\n          routerDirection=\"root\"\n          detail=\"false\"\n        >\n          <ion-icon slot=\"start\" name=\"cog-outline\"></ion-icon>\n          <ion-label> Settings </ion-label>\n        </ion-item>\n      </ion-menu-toggle>\n      <ion-menu-toggle autoHide=\"false\">\n        <ion-item\n          routerLink=\"/app/tabs/account\"\n          routerLinkActive=\"selected\"\n          routerDirection=\"root\"\n          detail=\"false\"\n        >\n          <ion-icon slot=\"start\" name=\"person\"></ion-icon>\n          <ion-label> Account </ion-label>\n        </ion-item>\n      </ion-menu-toggle>\n\n      <ion-menu-toggle autoHide=\"false\">\n        <ion-item button (click)=\"logout()\" detail=\"false\">\n          <ion-icon slot=\"start\" name=\"log-out\"></ion-icon>\n          <ion-label> Logout </ion-label>\n        </ion-item>\n      </ion-menu-toggle>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<ion-router-outlet id=\"main-content\"></ion-router-outlet>");

/***/ }),

/***/ "vW0O":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.module.ts ***!
  \**************************************************************************************************************************/
/*! exports provided: NExamHallCandidateCoreModulePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NExamHallCandidateCoreModulePageModule", function() { return NExamHallCandidateCoreModulePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _n_exam_hall_candidate_core_module_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./n-exam-hall-candidate-core-module-routing.module */ "ZVIx");
/* harmony import */ var _n_exam_hall_candidate_core_module_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./n-exam-hall-candidate-core-module.page */ "Xyc/");
/* harmony import */ var _nExamHall_Core_Utils_mainviewcontainer_mainviewcontainer_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../nExamHall-Core-Utils/mainviewcontainer/mainviewcontainer.component */ "fdAZ");
/* harmony import */ var _nExamHall_Core_Utils_bottom_menu_bar_bottom_menu_bar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../nExamHall-Core-Utils/bottom-menu-bar/bottom-menu-bar.component */ "Zfcf");
/* harmony import */ var _nExamHall_Core_Utils_side_menu_bar_side_menu_bar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../nExamHall-Core-Utils/side-menu-bar/side-menu-bar.component */ "MSrT");
/* harmony import */ var _candidate_dashboard_candidate_dashboard_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../candidate-dashboard/candidate-dashboard.page */ "23nY");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../settings/settings.component */ "phol");
/* harmony import */ var _myexams_myexams_page__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../myexams/myexams.page */ "8VBk");
/* harmony import */ var _analysis_chart_analysis_chart_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../analysis-chart/analysis-chart.component */ "P/7j");
/* harmony import */ var _profile_profile_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../profile/profile.component */ "bwnV");
/* harmony import */ var _nExamHall_Custom_Components_mockfilter_modal_mockfilter_modal_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../nExamHall-Custom-Components/mockfilter-modal/mockfilter-modal.component */ "5IGy");
















let NExamHallCandidateCoreModulePageModule = class NExamHallCandidateCoreModulePageModule {
};
NExamHallCandidateCoreModulePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _n_exam_hall_candidate_core_module_routing_module__WEBPACK_IMPORTED_MODULE_5__["NExamHallCandidateCoreModulePageRoutingModule"]
        ],
        declarations: [
            _n_exam_hall_candidate_core_module_page__WEBPACK_IMPORTED_MODULE_6__["NExamHallCandidateCoreModulePage"],
            _nExamHall_Core_Utils_mainviewcontainer_mainviewcontainer_component__WEBPACK_IMPORTED_MODULE_7__["MainviewcontainerComponent"],
            _nExamHall_Core_Utils_bottom_menu_bar_bottom_menu_bar_component__WEBPACK_IMPORTED_MODULE_8__["BottomMenuBarComponent"],
            _nExamHall_Core_Utils_side_menu_bar_side_menu_bar_component__WEBPACK_IMPORTED_MODULE_9__["SideMenuBarComponent"],
            _candidate_dashboard_candidate_dashboard_page__WEBPACK_IMPORTED_MODULE_10__["CandidateDashboardPage"],
            _settings_settings_component__WEBPACK_IMPORTED_MODULE_11__["SettingsComponent"],
            _myexams_myexams_page__WEBPACK_IMPORTED_MODULE_12__["MyexamsPage"],
            _analysis_chart_analysis_chart_component__WEBPACK_IMPORTED_MODULE_13__["AnalysisChartComponent"],
            _profile_profile_component__WEBPACK_IMPORTED_MODULE_14__["ProfileComponent"],
            _nExamHall_Custom_Components_mockfilter_modal_mockfilter_modal_component__WEBPACK_IMPORTED_MODULE_15__["MockfilterModalComponent"],
        ],
        exports: [
            _nExamHall_Core_Utils_mainviewcontainer_mainviewcontainer_component__WEBPACK_IMPORTED_MODULE_7__["MainviewcontainerComponent"],
            _nExamHall_Core_Utils_bottom_menu_bar_bottom_menu_bar_component__WEBPACK_IMPORTED_MODULE_8__["BottomMenuBarComponent"],
            _nExamHall_Core_Utils_side_menu_bar_side_menu_bar_component__WEBPACK_IMPORTED_MODULE_9__["SideMenuBarComponent"],
            _candidate_dashboard_candidate_dashboard_page__WEBPACK_IMPORTED_MODULE_10__["CandidateDashboardPage"],
            _settings_settings_component__WEBPACK_IMPORTED_MODULE_11__["SettingsComponent"],
            _myexams_myexams_page__WEBPACK_IMPORTED_MODULE_12__["MyexamsPage"],
            _analysis_chart_analysis_chart_component__WEBPACK_IMPORTED_MODULE_13__["AnalysisChartComponent"],
            _profile_profile_component__WEBPACK_IMPORTED_MODULE_14__["ProfileComponent"],
            _nExamHall_Custom_Components_mockfilter_modal_mockfilter_modal_component__WEBPACK_IMPORTED_MODULE_15__["MockfilterModalComponent"],
        ],
    })
], NExamHallCandidateCoreModulePageModule);



/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _nExamHall_Providers_onboarding_check_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nExamHall-Providers/onboarding-check.service */ "ZDsB");
/* harmony import */ var _nExamHall_Candidate_Module_nExamHall_Core_Utils_mainviewcontainer_mainviewcontainer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./nExamHall-Candidate-Module/nExamHall-Core-Utils/mainviewcontainer/mainviewcontainer.component */ "fdAZ");
/* harmony import */ var _nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./nExamHall-Services/theme-switcher.service */ "Fql8");






const routes = [
    {
        path: 'home',
        loadChildren: () => __webpack_require__.e(/*! import() | home-home-module */ "home-home-module").then(__webpack_require__.bind(null, /*! ./home/home.module */ "ct+p")).then(m => m.HomePageModule)
    },
    {
        path: '',
        redirectTo: 'onboardingscreen',
        pathMatch: 'full'
    },
    {
        path: 'nexamhallcandidate',
        component: _nExamHall_Candidate_Module_nExamHall_Core_Utils_mainviewcontainer_mainviewcontainer_component__WEBPACK_IMPORTED_MODULE_4__["MainviewcontainerComponent"],
        loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.module */ "vW0O")).then(m => m.NExamHallCandidateCoreModulePageModule),
        canLoad: [_nExamHall_Services_theme_switcher_service__WEBPACK_IMPORTED_MODULE_5__["ThemeSwitcherService"]]
    },
    {
        path: 'nexamhallcore',
        loadChildren: () => Promise.all(/*! import() | nExamHall-Core-Pages-auth-auth-module */[__webpack_require__.e("common"), __webpack_require__.e("nExamHall-Core-Pages-auth-auth-module")]).then(__webpack_require__.bind(null, /*! ./nExamHall-Core-Pages/auth/auth.module */ "B5mF")).then(m => m.AuthPageModule)
    },
    {
        path: 'onboardingscreen',
        loadChildren: () => __webpack_require__.e(/*! import() | nExamHall-Core-Pages-onboardingscreen-onboardingscreen-module */ "nExamHall-Core-Pages-onboardingscreen-onboardingscreen-module").then(__webpack_require__.bind(null, /*! ./nExamHall-Core-Pages/onboardingscreen/onboardingscreen.module */ "aJvx")).then(m => m.OnboardingscreenPageModule),
        canLoad: [_nExamHall_Providers_onboarding_check_service__WEBPACK_IMPORTED_MODULE_3__["OnboardingCheckService"]]
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "xHtS":
/*!***************************************************!*\
  !*** ./src/app/nExamHall-Services/api.service.ts ***!
  \***************************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../environments/environment */ "AytR");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "e8h1");






let ApiService = class ApiService {
    constructor(http, storage) {
        this.http = http;
        this.storage = storage;
        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiUrl;
    }
    handleError(error) {
        if (error.error instanceof ErrorEvent) {
            console.log("An error occured", error.error.message);
        }
        else {
            console.log(`Backend returned code ${error.status},` + `body was : ${error.error}`);
        }
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])("something bad happend ;please try again later");
    }
    ;
    loginauth(loginData) {
        return this.http.post(this.apiUrl + 'login', loginData);
    }
    checkCredentials(data) {
        return this.http.post(this.apiUrl + "checkcredentials", data);
    }
    sendAuthOTP(otpdata) {
        return this.http.post(this.apiUrl + 'sendOtp', otpdata);
    }
    getDashBoardDetails(loginId) {
        return this.http.post(this.apiUrl + "dashboardanlytics", {
            loginId: loginId,
        });
    }
    ;
    userEnrolled_exams(loginId) {
        return this.http.post(this.apiUrl + "userenroled", {
            loginId: loginId,
        });
    }
    ;
    getTestmaps(loginid, instituteid, examid) {
        //debugger;
        return this.http.post(this.apiUrl + "usermockexams", {
            loginId: loginid,
            instituteuserid: instituteid,
            examid: examid,
        });
    }
    ;
    uservalidityCheck(userValidityCheckData) {
        return this.http.post(this.apiUrl + "uservalidity", userValidityCheckData);
    }
    ;
    startTest(getExamdata) {
        return this.http.get("assets/data/question.json");
        // return this.http.post(this.base_path + "startexam", getExamdata);
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] }
];
ApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ApiService);



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map