import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MenuController,Platform} from '@ionic/angular';
import {ThemeSwitcherService} from '../../../nExamHall-Services/theme-switcher.service'
import {LocalStorageService} from '../../../nExamHall-Services/local-storage.service'
import {CommonServicesService} from '../../../nExamHall-Services/common-services.service'



@Component({
  selector: 'app-side-menu-bar',
  templateUrl: './side-menu-bar.component.html',
  styleUrls: ['./side-menu-bar.component.scss'],
})
export class SideMenuBarComponent implements OnInit {
  appPages = [
    {
      title: 'Home',
      url: '/nexamhallcandidate/dashboard',
      icon: 'home'
    },
    {
      title: 'My Exams',
      url: '/nexamhallcandidate/myexams',
      icon: 'receipt'
    },
    {
      title: 'My Videoseries',
      url: '/nexamhallcandidate/videoseries',
      icon: 'person'
    },
    {
      title: 'My Result',
      url: '/nexamhallcandidate/completedresult',
      icon: 'information-circle'
    }
    
  ];

  userimage : string;
  theme : string;
  userdata:any;
  loginId:string;
  username : string;
  lastlogin_date: any;
  constructor(
    private menu : MenuController,
    private platform : Platform,
    private router : Router,
    private themeSwitcherService : ThemeSwitcherService,
    private localStorage : LocalStorageService,
    private commonService :CommonServicesService

  ) {
     //debugger;
      
   }
  

  ngOnInit() {
  //   this.themeSwitcherService.intitialTheme().then((res) =>{
  //     this.theme = res;
  //  });
  }
 logout() {
    // this.userData.logout().then(() => {
      return this.router.navigateByUrl('/nexamhallcore/auth/login');
    //});
  }

  getTheme(){
      return this.localStorage.get('theme').then((res) =>{
          return this.theme = res;
       })
  } 
  initTheme(){
    //debugger;
    this.localStorage.get('theme').then((res) =>{
       this.theme = res;
       this.themeSwitcherService.setThemeProperty(this.theme);
       this.localStorage
       .getObject("candidateUserdata")
       .then((result) => {
         if (result != null) {
           this.userdata = result;
           this.userimage = this.commonService.addImageUrl(this.userdata.userimage,this.userdata.firstName,60)
            this.username = this.userdata.firstName;
            this.lastlogin_date = this.userdata.lastlogin_date;
         }
       })
   })
  } 
}
