import { Component, OnInit } from '@angular/core';
import {ThemeSwitcherService} from '../../../nExamHall-Services/theme-switcher.service';
import {LocalStorageService} from '../../../nExamHall-Services/local-storage.service'


@Component({
  selector: 'app-mainviewcontainer',
  templateUrl: './mainviewcontainer.component.html',
  styleUrls: ['./mainviewcontainer.component.scss'],
})
export class MainviewcontainerComponent implements OnInit {
   theme : string;
  constructor(
    private themeSwitcherService : ThemeSwitcherService,
    private localStorage : LocalStorageService
  ) { }

  ngOnInit() {}
  ionViewWillEnter(){
  
       this.localStorage.get('theme').then((res) =>{
            this.theme = res;
       })
        
   }
}
