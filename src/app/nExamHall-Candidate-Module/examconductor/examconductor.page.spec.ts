import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExamconductorPage } from './examconductor.page';

describe('ExamconductorPage', () => {
  let component: ExamconductorPage;
  let fixture: ComponentFixture<ExamconductorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamconductorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExamconductorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
