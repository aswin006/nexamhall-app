import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExamconductorPageRoutingModule } from './examconductor-routing.module';

import { ExamconductorPage } from './examconductor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExamconductorPageRoutingModule
  ],
  declarations: [ExamconductorPage]
})
export class ExamconductorPageModule {}
