import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExamconductorPage } from './examconductor.page';

const routes: Routes = [
  {
    path: '',
    component: ExamconductorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExamconductorPageRoutingModule {}
