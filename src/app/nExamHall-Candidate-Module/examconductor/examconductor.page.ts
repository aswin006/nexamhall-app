import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { IonSlides ,ActionSheetController} from "@ionic/angular";

import {LocalStorageService} from '../../nExamHall-Services/local-storage.service';
import {ApiService} from '../../nExamHall-Services/api.service';
import {ToastService} from '../../nExamHall-Services/toast.service';
import {CommonServicesService} from '../../nExamHall-Services/common-services.service'

import { BehaviorSubject } from "rxjs";

@Component({
  selector: 'app-examconductor',
  templateUrl: './examconductor.page.html',
  styleUrls: ['./examconductor.page.scss'],
})
export class ExamconductorPage implements OnInit {
  @ViewChild("slides", { static: true }) slides: IonSlides;

  theme: any;
  progress: number = 0;
  question: any;
  option1: string;
  getExamData: object = {};
  loginId: any;
  instituteid: any;
  examid: any;
  mockid: any;
  examstatus: any;
  examseq: any;
  defaultLanguage: any;
  startDate: any;
  testseriesid: any;
  languageSuport: any;

  hoursS: any = "00";
  minutesS: any = "00";
  secondsS: any = "00";
  showPauseBtn: any;
  totalquestion: number;
  examname: any;
  test_data: any;
  test_answer: any;
  counterdown: any;
  current: number;
  counter: number;
  index: number;
  timeup: any;
  filteredquestion: any;
  qno: any;
  subjectName: any;
  unitName: any;
  subjectid: any;
  unitid: any;
  ques_id: any;
  question_value: any;
  questiontype: any;
  parentid: any;
  filteredComp: any;
  filteredCompLength: any;
  compquesno: any;
  Options: {};
  option: any = {};
  answerstatus: any;
  not_visited: any;
  answered_count: any;
  markforreview_count: any;
  marked_ans_count: any;
  que: string;
  time_duration: string;
  optionkeys: any[];
  selected_option: any;
  qindex: any;
  nextqno: any;
  checkboxvalue: string = "A";
  nextBtnDisabled: boolean = false;
  prevBtnDisabled: boolean = false;
  interval;
  hours: number;
  minutes: number;
  seconds: number;
  time_spent: BehaviorSubject<string> = new BehaviorSubject("00:00:00");
  unanswered_count: any;
  modalDisplay: string = "none";
  isModalShow: boolean;
  isSummaryModalShow: boolean;
 
  reviewflag = false;
  constructor(
    private localStorage :LocalStorageService,
    private apiService : ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private actionSheetController : ActionSheetController,
    private toast : ToastService,
    private commonService : CommonServicesService
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.mockid = this.route.snapshot.paramMap.get("mockid");
    this.examstatus = this.route.snapshot.paramMap.get("examstatus");
    this.examseq = this.route.snapshot.paramMap.get("examseq");
    this.localStorage
      .get("currentExamStatus")
      .then((result) => {
        console.log("1", result);
      })
      .catch((e) => {
        console.log("error: " + e);
      });

    this.localStorage
      .getObject("candidateUserData")
      .then((result) => {
        if (result) {
          var userdata = result;
          this.loginId = userdata.loginId;
          this.instituteid = userdata.instituteuserid;
        }
      })
      .catch((e) => {
        console.log("error: " + e);
      });
    this.localStorage
      .getObject("exam_details")
      .then((result) => {
        if (result) {
          var exam_details = result;
          this.examid = exam_details.examid;
          this.languageSuport = exam_details.langsupport;
          this.testseriesid = exam_details.testseriesid;
          this.showPauseBtn = exam_details.showpausebtn;
          this.getExamData = {
            loginId: this.loginId,
            instituteid: this.instituteid,
            examid: this.examid,
            mockid: this.mockid,
            examstatus: this.examstatus,
            examseq: this.examseq,
            defaultlanguage: this.defaultLanguage,
            created: this.startDate,
            testseriesid: this.testseriesid,
          };
          this.loadData(this.getExamData);
        }
      })
      .catch((e) => {
        console.log("error: " + e);
      });
  }
ionViewDidEnter(){
  eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
  this.localStorage.get('theme').then((res) =>{
    this.theme = res;
    this.commonService.hideTabs();
  })
}
loadData(getExamData) {
  debugger;
  this.apiService.startTest(getExamData).subscribe(async (res) => {
    var response = res["questiondata"];
    this.question = response[0].questionsblock;
    this.totalquestion = this.question.length;
    this.examname = response[0].examname;
    this.test_data = response[0];
    this.test_answer = this.question;
    if (this.examstatus == 0 || this.examstatus == 2) {
      this.counterdown = this.test_data.examDuration * 60;
      this.current = 0;
      this.counter = 0;
      this.index = 0;
    } else {
      this.counterdown = this.test_data.totaltimespent;
    }
    this.Startcoundown();
    this.getfilterquestion("A");
    console.log(res);
  });
};

pad(n) {
  return (n < 10 ? "0" : "") + n;
}
// starttimer = function () {
//   this.timer = setInterval(() => {
//     this.counter = this.counter + 1;
//     this.hours = Math.floor(this.counter / 60 / 60);
//     this.minutes = Math.floor((this.counter / 60) % 60);
//     this.seconds = Math.floor(this.counter % 60);
//     this.time_spent =
//       this.pad(this.hours) +
//       ":" +
//       this.pad(this.minutes) +
//       ":" +
//       this.pad(this.seconds);
//   }, 1000);
// };
starttimer() {
  clearInterval(this.interval);
  this.updateTime();
  this.interval = setInterval(() => {
    this.updateTime();
  }, 1000);
}
stoptimer() {
  clearInterval(this.interval);
}
updateTime() {
  this.counter = this.counter + 1;
  this.hours = Math.floor(this.counter / 60 / 60);
  this.minutes = Math.floor((this.counter / 60) % 60);
  this.seconds = Math.floor(this.counter % 60);
  var text =
    this.pad(this.hours) +
    ":" +
    this.pad(this.minutes) +
    ":" +
    this.pad(this.seconds);
  this.time_spent.next(text);
}
Startcoundown() {
  this.timeup = setInterval(() => {
    this.counterdown -= 1;
    this.hoursS = Math.floor(this.counterdown / 60 / 60);
    this.minutesS = Math.floor((this.counterdown / 60) % 60);
    this.secondsS = Math.floor(this.counterdown % 60);
    this.time_duration =
      this.pad(this.hoursS) +
      ":" +
      this.pad(this.minutesS) +
      ":" +
      this.pad(this.secondsS);
    if (this.counterdown < 0) {
      this.hoursS = "00";
      this.minutesS = "00";
      this.secondsS = "00";
      this.counterdown = 0;
    }
  }, 1000);
}
questionrefresh(checkboxvalue) {
  if (checkboxvalue == "A") {
    this.filteredquestion = this.question;
  } else {
    this.filteredquestion = this.filteredquestion.filter(
      (item) => item.answerstatus == checkboxvalue
    );
  }
}

getfilterquestion(checkboxvalue) {
  this.questionrefresh(checkboxvalue);
  this.show_question(0);
}





// slideChanged(qno) {
//   // let currentIndex = this.slides.getActiveIndex();
//   // console.log("Current index is", currentIndex);
//   this.stoptimer();
//   this.show_question(qno);
//   this.slides.getActiveIndex().then((index) => {
//     console.log(index);
//   });
// }
check_status(reviewflag) {
  debugger;
  if (this.option.selected_option != undefined &&this.option.selected_option != "0") {
      if (reviewflag) {
        this.answerstatus = "4";
      } else {
        this.answerstatus = "1";
      }
  } else {
      if (reviewflag) {
        this.answerstatus = "3";
         
      } else {
        this.answerstatus = "2";
      }

  }
};
check_status2(statusupdate, currentstatus) {
  if (currentstatus == "0" || currentstatus == '') {
      this.answerstatus = "2";
  } else {
      this.answerstatus = currentstatus;
  }
};
status_count(oldstatus, newstatus) {
  //debugger;

  if (oldstatus != newstatus) {
      if (oldstatus != 0) {
          if (oldstatus == "1") {
              this.answered_count = this.answered_count  - 1;
          } else if (oldstatus == "2") {
            this.unanswered_count =  this.unanswered_count - 1;
          } else if (oldstatus == "3") {
            this.markforreview_count = this.markforreview_count - 1;
          } else if (oldstatus == "4") {
            this.marked_ans_count =  this.marked_ans_count - 1;
          }

      }
      /******************* */
      if (newstatus == "1") {
        this.answered_count = this.answered_count + 1;
      } else if (newstatus == "2") {
        this.unanswered_count =  this.unanswered_count + 1;
      } else if (newstatus == "3") {
        this.markforreview_count = this.markforreview_count + 1;
      } else if (newstatus == "4") {
        this.marked_ans_count =  this.marked_ans_count + 1;
      }
  }
}
save_answer(currentqno, ansupdate) {
  this.index = currentqno;
 this.status_count(this.test_answer[this.index].answerstatus, this.answerstatus);

  if (ansupdate == "Y") {
    this.test_answer[this.index].selectoption = this.selected_option;
  }
  this.test_answer[this.index].seqid = this.test_data._id; 
  this.test_answer[this.index].ques_id = this.test_data.questionsblock[this.index].questionid;
  this.test_answer[this.index].answerstatus = this.answerstatus;
  this.test_answer[this.index].time_spent = this.counter;
  this.test_answer[this.index].totaltimespent = this.counterdown;
  if (this.counterdown <= 0) {
    this.test_answer[this.index].examstatus = "2";
  } else {
    this.test_answer[this.index].examstatus = "1";
  }
  this.test_answer[this.index].answered_count = this.answered_count;
  this.test_answer[this.index].unanswered_count = this.unanswered_count;
  this.test_answer[this.index].markforreview_count = this.markforreview_count;
  this.test_answer[this.index].marked_ans_count = this.marked_ans_count;
  this.test_answer[this.index].not_visited = this.totalquestion -(this.answered_count + this.unanswered_count + this.markforreview_count + this.marked_ans_count);
  this.test_answer[this.index].lastquestionvist = currentqno;

  ///****save local and finaly submit btn clicked send to backend */
  this.localStorage.setObject('testanswer',this.test_answer);
};

iterate_object(o) {
  var keys = Object.keys(o);
  for (var i = 0; i < keys.length; i++) {
      return [keys[i], o[keys[i]]];
  }
}
findKey(obj, langKeys) {
  // debugger;
  var findkey = null;
  for (var [key, val] of this.iterate_object(obj)) {
      if (key == langKeys) {
          if (obj[key].question) {
              findkey = true;
              break;
          } else {
              findkey = false;
              break;
          }


      }

  }
  return findkey;
};
getCurrentFilteredQuestion = function(question, qno) {
  var filteredques = this.filteredquestion.filter(
    (item) => item.questionno == qno
  );
  return filteredques
  // var filteredques = $filter('filter')(question, function(value, index) {
  //     return value.questionno == qno;
  // })[0];
  // return filteredques;
}
async checkquestionSupport(qno) {
  //  debugger;
  var questions = this.getCurrentFilteredQuestion(this.filteredquestion, qno); /**22-05-20-Aswin-mobile-view-sidebar-click-error*/
  var tamil = this.findKey(questions, 'ta');
  var english = this.findKey(questions, 'en');
  // if (tamil && english) {
  //     var tempLang = await this.localStorage.get('defaultLanguage');
  //     if (tempLang == "undefined" || tempLang == "null" || tempLang == undefined || tempLang == null) {
  //         this.currentLanguage = this.defaultLanguage;
  //         //this.setLang(this.currentLanguage);
  //         this.setlangNgmodel(this.currentLanguage);

  //     } else {
  //         this.currentLanguage = tempLang;
  //         this.languageSuport = getLocalStorageData('languageSupport');
  //         // this.setLang(this.currentLanguage);
  //         this.setlangNgmodel(this.currentLanguage);
  //     }

  // } else {
  //     if (tamil) {
  //         this.languageSuport = this.getDynamicLang('ta');
  //         this.currentLanguage = this.languageSuport[0].lngcode;
  //         // this.setLang(this.currentLanguage);
  //         this.setlangNgmodel(this.currentLanguage);
  //         //alert('tamil false')
  //     }
  //     if (english) {
  //         this.languageSuport = this.getDynamicLang('en');
  //         this.currentLanguage = this.languageSuport[0].lngcode;
  //         // this.setLang(this.currentLanguage);
  //         this.setlangNgmodel(this.currentLanguage);
  //         //alert('English false')
  //     }
  // }



}


show_question(nextqno) {
  debugger;
  this.index = nextqno;
  this.qno = this.filteredquestion[this.index].questionno;
  this.subjectName = this.filteredquestion[this.index].subject;
  this.unitName = this.filteredquestion[this.index].unit;
  this.subjectid = this.filteredquestion[this.index].subjectid;
  this.unitid = this.filteredquestion[this.index].unitid;
  this.ques_id = this.filteredquestion[this.index].questionid;

  this.question_value = this.filteredquestion[this.index]["en"].question;

  this.questiontype = this.filteredquestion[this.index].questiontype;
  this.parentid = this.filteredquestion[this.index].parentid;
  this.filteredComp = this.filteredquestion.filter(
    (item) => item.parentid == this.parentid
  );

  // (Que No. {{(test.sections[currentQuestion.sSNo - 1].questions[currentQuestion.qSNo - 1].startQno + 1) + " - " + (test.sections[currentQuestion.sSNo - 1].questions[currentQuestion.qSNo - 1].endQno + 1)}})
  this.filteredCompLength = this.filteredComp.length || "";
  this.compquesno = this.filteredquestion[this.index].compquesno;
  //console.log('comp:' + this.filteredCompLength.length)
  this.Options = {};
  this.Options = this.filteredquestion[this.index]["en"].options;
  this.optionkeys = Object.keys(this.Options);
  this.selected_option = this.filteredquestion[this.index].selectoption;
  this.answerstatus = this.filteredquestion[this.index].answerstatus;
  if(this.answerstatus == '4' || this.answerstatus == '3'){
     this.reviewflag = true;
  }else{
    this.reviewflag = false;
  }
  this.counter = this.filteredquestion[this.index].time_spent;

  this.not_visited = this.totalquestion - (this.answered_count + this.answered_count +this.markforreview_count + this.marked_ans_count);

  this.starttimer(); //counter starttimer
  eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');

}

markreview(){
this.reviewflag = !this.reviewflag
}

next_btn(questionno) {
  debugger;
  this.stoptimer();
  this.check_status(this.reviewflag); //chack answer or unanswer 
  this.qindex = questionno - 1;
  this.save_answer(this.qindex, "Y"); //save answer or insert test answer
  var findex = this.filteredquestion.findIndex( (x) => x.questionno === questionno);
  this.nextqno = findex < this.filteredquestion.length - 1 ? findex + 1 : findex;

  if (findex == this.filteredquestion.length - 1) {
    this.toast.showToast("you are in last question",2000,'top', "dark");
    this.nextBtnDisabled = true;
    this.prevBtnDisabled = false;
  } else {
    this.show_question(this.nextqno);
    this.questionrefresh("A");
  }
}

prev_btn(questionno) {
  this.stoptimer();
  this.qindex = questionno - 1;
  var findex = this.filteredquestion.findIndex(
    (x) => x.questionno === questionno
  );
  this.save_answer(this.qindex, "Y");
  if (findex != 0) {
    this.prevBtnDisabled = false;
    this.nextBtnDisabled = false;
    this.qindex = questionno - 1; //question index this.current = this.index;
    //findex = find index
    var findex = this.filteredquestion.findIndex(
      (x) => x.questionno === questionno
    );
    // console.log(findex)
    // this.nextqno = findex < this.filteredquestion.length - 1 ? findex - 1 : findex;
    this.nextqno = findex - 1;

    if (findex == 0) {
      this.checkboxvalue = "A";
      this.getfilterquestion(this.checkboxvalue);
    } else {
      this.show_question(this.nextqno);
      this.questionrefresh(this.checkboxvalue);
    }
  } else {
    //alert('First question')
    this.prevBtnDisabled = true;
    this.nextBtnDisabled = false;
  }
}
openModalDialog() {
  this.modalDisplay = "block"; //Set block css
  this.isModalShow = true;
}

closeModalDialog() {
  debugger;
  this.modalDisplay = "none"; //set none css after close dialog
  this.isModalShow = false;
}

filterquestion(checkboxvalue, currentqno) {
  //debugger;
  this.closeModalDialog();

  if (checkboxvalue == undefined) {
      // var checkboxvalue = "A";aswin summary to filter not working 08-03-19
      this.checkboxvalue = "A";
  } else {
    this.checkboxvalue = checkboxvalue;

  }
  this.stoptimer();
 
  this.save_answer(currentqno - 1, "N");

  this.getfilterquestion(this.checkboxvalue);
  
}



setActiveQuestion(currentqno, nextqno) {
  // debugger;
  this.qindex = currentqno - 1;
  this.stoptimer();
  // this.check_status2("N", this.answerstatus); //check answer or unanswer
  this.save_answer(this.qindex, "N"); //save answer or insert test answer
  var findex = this.filteredquestion.findIndex(
    (x) => x.questionno === nextqno
  );
  this.nextqno = findex;
  //  this.questionrefresh(this.checkboxvalue);

  this.slides.slideTo(this.nextqno, 1000, false);
  this.show_question(this.nextqno); //question values show
  this.closeModalDialog();
}

slideChangeBack(qno) {
  this.prev_btn(qno);
}
slideChangeNext(qno) {
  this.next_btn(qno);
}
storeOption(qno, value) {
  if (qno && value) {
    this.filteredquestion[qno - 1].selectoption = value;
  } else {
    this.filteredquestion[qno - 1].selectoption = 0;
  }
}

next() {
  //this.next_btn(qno);
  this.slides.slideNext();
}

prev() {
  // this.prev_btn(qno);
  this.slides.slidePrev();
}
goToExam() {
  this.router.navigateByUrl("/candidate/myexams");
}

async presentActionSheet() {
  const actionSheet = await this.actionSheetController.create({
    header: 'Albums',
    cssClass: 'my-custom-class',
    buttons: [{
      text: 'Delete',
      role: 'destructive',
      icon: 'trash',
      handler: () => {
        console.log('Delete clicked');
      }
    }, {
      text: 'Share',
      icon: 'share',
      handler: () => {
        console.log('Share clicked');
      }
    }, {
      text: 'Play (open modal)',
      icon: 'caret-forward-circle',
      handler: () => {
        console.log('Play clicked');
      }
    }, {
      text: 'Favorite',
      icon: 'heart',
      handler: () => {
        console.log('Favorite clicked');
      }
    }, {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();
};

submit(){
  debugger;
   this.presentActionSheet()
}
}
