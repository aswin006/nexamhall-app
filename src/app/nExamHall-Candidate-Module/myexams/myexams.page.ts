import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import { LocalStorageService } from '../../nExamHall-Services/local-storage.service';
import { ApiService } from '../../nExamHall-Services/api.service';
import { CommonServicesService } from '../../nExamHall-Services/common-services.service';
import{AlertService} from '../../nExamHall-Services/alert.service'
import {LoaderService} from '../../nExamHall-Services/loader.service';


@Component({
  selector: 'app-myexams',
  templateUrl: './myexams.page.html',
  styleUrls: ['./myexams.page.scss'],
})
export class MyexamsPage implements OnInit {
  theme : string;
  loginId: any;
  bgcolors: string[];
  selected_Exam: any[];
  allocate_exams: any;
  constructor(
    private localStorage : LocalStorageService,
    private apiService : ApiService,
    private commonService : CommonServicesService,
    private alertService :AlertService,
    private loader :LoaderService,
    private router : Router
  ) { 

  }

  ngOnInit() {
  }
  ionViewWillEnter(){
    debugger;
    this.loader.showImgLoader();
    this.localStorage.get('theme').then((res) =>{
      this.theme = res;
      this.localStorage
      .getObject("candidateUserdata")
      .then((result) => {
        if (result != null) {
          var _userdetails = result
          this.loginId = _userdetails.loginId;
          console.log("token :" + _userdetails);
          this.apiService
            .userEnrolled_exams(this.loginId)
            .subscribe(async (res) => {
              //debugger;
              this.bgcolors = [
                "linear-gradient(rgba(255, 250, 238, 1) , white)",
                "linear-gradient(rgba(255, 243, 243, 1) , white)",
                "linear-gradient(rgba(251, 252, 236, 1) , white)",
              ];

              var curr = 0;

              this.allocate_exams = res;
              this.selected_Exam = [];
              // console.log("this.allocate_exams :", this.allocate_exams);
              for (let i in this.allocate_exams) {
                this.selected_Exam.push({
                  examid: this.allocate_exams[i]._id,
                  examname: this.allocate_exams[i].examname,
                  examimage: this.commonService.addImageUrl(this.allocate_exams[i].images,
                    this.allocate_exams[i].examname,60),
                  mockexamslength: this.allocate_exams[i].mockexamscount,
                  testserieslength: this.allocate_exams[i].testseriescount,
                  testseriessupport: this.allocate_exams[i].testseries,
                  livemocktestlength: this.allocate_exams[i].livemockcount,
                  livemocktestsupport: _userdetails.features.livetest,
                  background_color: this.bgcolors[curr],
                });
                curr++;
                if (curr == this.bgcolors.length) {
                  curr = 0;
                }
              }

              console.log(this.selected_Exam);
            });
        }
        this.loader.hideLoader();
      })
      .catch((e) => {
        console.log("error: " + e);
        this.loader.hideLoader();
      });
    });
  };

  gomocklist(examid, mockexamslength) {
    //debugger;
    if (mockexamslength > 0) {

       // $location.path('examlist/' + examid + '/' + 0);
       this.router.navigateByUrl(`/nexamhallcandidate/myexams/mockexamlist/${examid}/0`)
    } else {
        var alertMsg = 'This Exam have no more mock tests';
        this.alertService.presentAlert(alertMsg);
    }

};



goTestSeries(examid, testserieslength) {
    if (testserieslength > 0) {

      this.router.navigateByUrl(`/nexamhallcandidate/myexams/testseries/${examid}`)
       // $location.path('testseries/' + examid)
    } else {
       var alertMsg = 'This Exam have no more test series';
        this.alertService.presentAlert(alertMsg);
    }
}


goLiveTest(examid, livetestlength) {
    if (livetestlength > 0) {
        // setLocalStorage('livetest', _userdetails.features.livetest)
        // $location.path('examlist/' + examid + '/' + 0)
    } else {
        var alertMsg = 'This Exam have no more live test';
        this.alertService.presentAlert(alertMsg);
    }
}

  // alert1(){
  //   this.alertService.presentAlert();
  // }
  // alert2(){
  //   this.alertService.presentAlertCheckbox();
  // }
  // alert3(){
  //   this.alertService.presentAlertConfirm();
  // }
  // alert4(){
  //   this.alertService.presentAlertMultipleButtons();
  // }
  // alert5(){
  //   this.alertService.presentAlertPrompt();
  // }
  // alert6(){
  //   this.alertService.presentAlertRadio();
  // }

}
