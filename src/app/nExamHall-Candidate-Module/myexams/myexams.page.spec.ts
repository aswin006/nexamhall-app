import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyexamsPage } from './myexams.page';

describe('MyexamsPage', () => {
  let component: MyexamsPage;
  let fixture: ComponentFixture<MyexamsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyexamsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyexamsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
