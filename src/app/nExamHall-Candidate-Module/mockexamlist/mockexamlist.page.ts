import { Component, OnInit } from '@angular/core';
import {
  NavController,
  IonList,
  IonRouterOutlet,
  ModalController,
} from "@ionic/angular";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "../../nExamHall-Services/api.service";
import { LocalStorageService } from "../../nExamHall-Services/local-storage.service";
import { LoaderService } from "../../nExamHall-Services/loader.service";
import {MockfilterModalComponent} from '../../nExamHall-Custom-Components/mockfilter-modal/mockfilter-modal.component';
import {AlertService} from '../../nExamHall-Services/alert.service';
import {CommonServicesService} from '../../nExamHall-Services/common-services.service'

@Component({
  selector: 'app-mockexamlist',
  templateUrl: './mockexamlist.page.html',
  styleUrls: ['./mockexamlist.page.scss'],
})
export class MockexamlistPage implements OnInit {
  defaultHref = "";
  userdata: any;
  loginId: any;
  instituteuserid: any;
  testseriesid: string;
  examid: string;
  student_map: any;
  mockexams: any[];
  mockExams: object;
  mockexamdetails: any = [];
  color: string;
  userExpired: any;
  getMockexams: Object;
  examname: any;
  allmockExams: any = [];
  excludedFilter: any = [];
  filtermockexam: any;
  mockid: any;
  examstatus: any;
  examseq: any;
  queryText: string;
  theme: any;
  constructor(
    public loader: LoaderService,
    public modalCtrl: ModalController,
    public router: Router,
    public routerOutlet: IonRouterOutlet,
    private route: ActivatedRoute,
    private apiService: ApiService,
    private localStorage: LocalStorageService,
    private nav: NavController,
    private alertService : AlertService,
    private commonService : CommonServicesService
   
  ) { }

  ngOnInit() {
  }

  constructMockList(data) {
    debugger;

    this.student_map = data[0];
    this.mockexams = this.student_map.mockexams;
    // console.log(this.student_map)
    var l = this.mockexams.length;
    this.mockexamdetails = [];
  
    for (let i = 0; i < l; i++) {
      this.mockExams = {
        _id: this.mockexams[i]._id,
        s_no: i + 1,
        examstatus: this.mockexams[i].examstatus,
        mockname: this.mockexams[i].mockname,
        examid: this.student_map._id,
        examseq: this.mockexams[i].examseq,
        exam_retake_limit: this.mockexams[i].examsretake,
        markPerQues: this.mockexams[i].markperquestions,
        negativeMark: this.mockexams[i].Negativemarks,
        examname: this.student_map.examname,
        timeduration: this.mockexams[i].timeduration,
        examshedule_date: this.student_map.examshedule_date,
        questioncount: this.mockexams[i].questioncount,
        buttonText: "",
        fa_icon: "",
        buttoncolor: "",
        langsupport: this.mockexams[i].langsupport,
        themesupport: this.student_map.themesupport,
        instructions: this.student_map.instructions,
        showviewsolution: this.mockexams[i].config.viewsolution,
        showpausebtn: this.mockexams[i].config.pause,
        showpdfdownload: this.mockexams[i].config.pdfviewsolution,
        testseriesid: this.testseriesid,
        testSeriesPaid: this.student_map.paid,
        startDate: this.mockexams[i].startDate,
        endDate: this.mockexams[i].endDate,
      };

      if (this.mockexams[i].examstatus == "0") {
        if (this.mockexams[i].freepaid == "1") {
          debugger;
          if (this.testseriesid != "0") {
            if (this.student_map.paid) {
              this.mockExams["buttonText"] = "Start Test";
              this.mockExams["fa_icon"] = "fa fa-hand-o-right";
              this.mockExams["buttoncolor"] = "success";
              this.color = "success";
            } else {
              this.mockExams["examstatus"] = "4";
              this.mockExams["buttonText"] = "Unlock Test";
              this.mockExams["fa_icon"] = "fa fa-lock";
              this.mockExams["buttoncolor"] = "success";
              this.color = "success";
            }
          } else {
            if (!this.userExpired) {
              this.mockExams["buttonText"] = "Start Test";
              this.mockExams["fa_icon"] = "fa fa-hand-o-right";
              this.mockExams["buttoncolor"] = "success";
              this.color = "success";
            } else {
              this.mockExams["examstatus"] = "4";
              this.mockExams["buttonText"] = "Unlock Test";
              this.mockExams["fa_icon"] = "fa fa-lock";
              this.mockExams["buttoncolor"] = "success";
              this.color = "success";
            }
          }
        } else {
          this.mockExams["buttonText"] = "Start Test";
          this.mockExams["fa_icon"] = "fa fa-hand-o-right";
          this.mockExams["buttoncolor"] = "success";
          this.color = "success";
        }

       
        this.mockexamdetails.push(this.mockExams);
      } else if (this.mockexams[i].examstatus == "1") {
        this.mockExams["buttonText"] = "Resume";
        this.mockExams["fa_icon"] = "fa fa-pause-circle-o";
        this.mockExams["buttoncolor"] = "info";
        this.color = "info";
       
        this.mockexamdetails.push(this.mockExams);
      } else if (this.mockexams[i].examstatus == "2") {
        this.mockExams["buttonText"] = "Re-Take";
        this.mockExams["fa_icon"] = "fa fa-undo";
        this.mockExams["buttoncolor"] = "warning";
        this.color = "warning";
        this.mockexamdetails.push(this.mockExams);
      }
    }
    console.log(this.mockexamdetails);
    this.allmockExams = this.mockexamdetails;
    this.loader.hideLoader();
  };

  ionViewWillEnter() {
    debugger;
    this.loader.showImgLoader();
    this.examid = this.route.snapshot.paramMap.get("examid");
    this.testseriesid = this.route.snapshot.paramMap.get("testseriesid");

    this.localStorage
      .getObject("candidateUserdata")
      .then((result) => {
        if (result != null) {
          this.userdata = result;
          this.loginId = this.userdata.loginId;
          this.instituteuserid = this.userdata.instituteuserid;
          if (this.testseriesid != "0") {
            console.log(this.testseriesid);
          } else {
            this.apiService
              .getTestmaps(this.loginId, this.instituteuserid, this.examid)
              .subscribe(async (res) => {
                this.getMockexams = res;
                this.examname = this.getMockexams[0].examname;
                this.constructMockList(this.getMockexams);
                console.log(res);
              });
          }
        }
      })
      .catch((e) => {
        console.log("error: " + e);
      });

    console.log("enter");
  }

  ionViewDidEnter() {
    this.localStorage.get('theme').then((res) =>{
      this.theme = res;
      this.commonService.showTabs();
    })
    this.defaultHref = 'nexamhallcandidate/myexams';
  };

  onSearchTerm(ev: CustomEvent) {
    this.mockexamdetails = this.allmockExams;
    const val = ev.detail.value;

    if (val && val.trim() !== "") {
      this.mockexamdetails = this.mockexamdetails.filter((term) => {
        return (
          term.mockname.toLowerCase().indexOf(val.trim().toLowerCase()) > -1
        );
      });
    }
  };

  async presentFilter() {
    const modal = await this.modalCtrl.create({
      component: MockfilterModalComponent,
     
      swipeToClose: true,
      cssClass:'half-modal',
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: { excludedFilter: this.excludedFilter },
    });

    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data) {
      this.excludedFilter = data;
      this.updateFilter(this.excludedFilter);
    }
  }

  updateFilter(excludedFilter) {
    this.mockexamdetails = this.allmockExams;
    console.log("gghh", excludedFilter);
    if (excludedFilter) {
      for (let i = 0; i < excludedFilter.length; i++) {
        this.mockexamdetails = this.mockexamdetails.filter(
          (item) => item.examstatus !== excludedFilter[i]
        );
      }
    }
  };


  intructpage(mockid, examstatus, examname, examseq, language) {
    debugger;
    if (examstatus == "4") {
      if (this.testseriesid != "0") {
      } else {
      }
    } else {
      var validityCheckData = {
        loginId: this.loginId,
        examid: this.examid,
        mockid: mockid,
        testseriesid: this.testseriesid,
      };
      this.apiService
        .uservalidityCheck(validityCheckData)
        .subscribe(async (res) => {
          console.log(res);
          if (res["status"]) {
            this.filtermockexam = this.mockexamdetails.filter(
              (item) => item._id == mockid
            );

            this.mockid = mockid;
            this.examstatus = examstatus;
            this.examseq = examseq;
            this.localStorage.setObject("exam_details", this.filtermockexam[0]);

            switch (examstatus) {
              case "0":
                this.localStorage.set("currentExamStatus", this.examstatus);
                this.router.navigateByUrl(
                  "/nexamhallcandidate/myexams/intructionpage/" +
                    this.mockid +
                    "/" +
                    this.examstatus +
                    "/" +
                    this.examseq
                );
                break;
              case "1":
                this.localStorage.set("currentExamStatus", this.examstatus);
                this.router.navigateByUrl(
                  "/nexamhallcandidate/myexams/examconductor/" +
                    this.mockid +
                    "/" +
                    this.examstatus +
                    "/" +
                    this.examseq
                );
                break;
              case "2":
                this.examseq = this.examseq + 1;
                this.localStorage.set("currentExamStatus", this.examstatus);
                this.router.navigateByUrl(
                  "/nexamhallcandidate/myexams/intructionpage/" +
                    this.mockid +
                    "/" +
                    this.examstatus +
                    "/" +
                    this.examseq
                );
                break;
              case "4":
                this.router.navigateByUrl("pricing");
                break;
            }
          } else {
            var msg = "Sorry, your plan is expired";
            this.alertService.presentAlert(msg);
          }
        });
    }
  }

}
