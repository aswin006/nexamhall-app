import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MockexamlistPage } from './mockexamlist.page';

describe('MockexamlistPage', () => {
  let component: MockexamlistPage;
  let fixture: ComponentFixture<MockexamlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MockexamlistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MockexamlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
