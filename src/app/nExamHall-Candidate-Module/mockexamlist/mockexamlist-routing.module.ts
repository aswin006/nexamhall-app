import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MockexamlistPage } from './mockexamlist.page';

const routes: Routes = [
  {
    path: '',
    component: MockexamlistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MockexamlistPageRoutingModule {}
