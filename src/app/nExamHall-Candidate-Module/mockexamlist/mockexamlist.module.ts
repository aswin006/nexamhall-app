import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MockexamlistPageRoutingModule } from './mockexamlist-routing.module';

import { MockexamlistPage } from './mockexamlist.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MockexamlistPageRoutingModule
  ],
  declarations: [MockexamlistPage]
})
export class MockexamlistPageModule {}
