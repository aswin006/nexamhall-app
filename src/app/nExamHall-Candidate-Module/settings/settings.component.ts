import { Component, OnInit } from '@angular/core';
import {ThemeSwitcherService} from '../../nExamHall-Services/theme-switcher.service'
import {LocalStorageService} from '../../nExamHall-Services/local-storage.service'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {


  // themes= [
  //   {
  //     id:1,
  //     name:'Yellow',
  //     color:'yellow',
  //     icon:'color-palette',
  //     cssclass:'theme-yellow'
  //   },
  //   {
  //     id:2,
  //     name:'Rich Green',
  //     color:'richgreen',
  //     icon:'color-palette-outline',
  //     cssclass:'theme-richgreen'
  //   },
  //   {
  //     id:3,
  //     name:'Pink',
  //     color:'dark',
  //     icon:'color-palette',
  //     cssclass:'theme-pink'
  //   },
  //   {
  //     id:4,
  //     name:'Blue',
  //     color:'blue',
  //     icon:'color-palette',
  //     cssclass:'theme-blue'
  //   },
  //   {
  //     id:5,
  //     name:'Orange',
  //     color:'orange',
  //     icon:'color-palette',
  //     cssclass:'theme-orange'
  //   },
  //   {
  //     id:6,
  //     name:'Green',
  //     color:'green',
  //     icon:'color-palette',
  //     cssclass:'theme-green'
  //   },
  //   {
  //     id:7,
  //     name:'Purple',
  //     color:'purple',
  //     icon:'color-palette',
  //     cssclass:'theme-purple'
  //   }
  // ]
  themes:any;
  selectedcolor =  0;
  selectedMode = '1';
  theme = '';
  constructor(
    public themeSwitcherService : ThemeSwitcherService,
    public localStorage : LocalStorageService
  ) { 
     this.themeSwitcherService.intitialTheme().then((res) =>{
         this.theme = res;
         this.themes = this.themeSwitcherService.getThemes()
    });
  }

  ngOnInit() {}

  themeColorSet(colorid){
    debugger;
    let themeColor = this.themes.find(x => x.id == colorid);
    this.theme = themeColor.cssclass;
    console.log('themeColor',themeColor);
    this.localStorage.set('theme',this.theme)
  }
  
  colorMode(mode){

    if(mode == 'auto'){

    }else{
      this.themeSwitcherService.setMode(mode);
    }
    
  }


}
