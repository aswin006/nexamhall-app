import { Component, OnInit, ViewChild,ElementRef  } from '@angular/core';
import {ThemeSwitcherService} from '../../nExamHall-Services/theme-switcher.service'
import {LocalStorageService} from '../../nExamHall-Services/local-storage.service';
import { Chart } from 'chart.js';
import { ApiService } from '../../nExamHall-Services/api.service';
import { LoaderService } from '../../nExamHall-Services/loader.service';
import { CommonServicesService } from '../../nExamHall-Services/common-services.service';







@Component({
  selector: 'app-candidate-dashboard',
  templateUrl: './candidate-dashboard.page.html',
  styleUrls: ['./candidate-dashboard.page.scss'],
})
export class CandidateDashboardPage implements OnInit {
 
  @ViewChild("doughnutCanvas") doughnutCanvas: ElementRef;
  private doughnutChart: Chart;

  userdata: any;
  dashboardData: any;

  theme : string;
  pdata: any[];
  showLess = true;
  showMoreBtn = 'Show more';
  showMoreBtnIcon =  'down';
  chartCardCustomStyle = 'showmore';
  loginId: any;
  userimage:string;
  username: any;
  lastlogin_date: any;
  constructor(
    private themeSwitcherService : ThemeSwitcherService,
    private localStorage : LocalStorageService,
    private apiService : ApiService,
    private loader : LoaderService,
    private commonService : CommonServicesService

  ) {
   
  }
   

  ngOnInit() {
    
  }
 
  ionViewDidEnter(){
    
  }
  
  ionViewWillEnter(){
    debugger;
    this.loader.showImgLoader();
    this.localStorage.get('theme').then((res) =>{
      this.theme = res;
      this.localStorage
      .getObject("candidateUserdata")
      .then((result) => {
        if (result != null) {
          this.userdata = result;
          this.loginId = this.userdata.loginId;
          this.userimage = this.commonService.addImageUrl(this.userdata.userimage,this.userdata.firstName,60);
          this.username = this.userdata.firstName;
          this.lastlogin_date = this.userdata.lastlogin_date;
          console.log("token :" + result);
          this.apiService.getDashBoardDetails(this.loginId).subscribe((res) => {
            // debugger;
            this.dashboardData = res;
            this.dashBoardGraphGenerate(this.dashboardData);
            this.loader.hideLoader();
            console.log(res);
          });
        }
      })
      .catch((e) => {
        console.log("error: " + e);
      });
    });
  }

  showMore(value){
   // debugger;
    this.showLess = !this.showLess;
    this.showMoreBtn = this.showLess == true ? 'Show more' : 'Show less';
    this.showMoreBtnIcon = this.showLess == true ? 'down' : 'up'; 

    this.chartCardCustomStyle = this.showLess == true ? 'showmore' : 'showless';
    // for(let item of this.pdata){
       
    // }
  }

  dashBoardGraphGenerate(data){
    this.pdata = [
      {
        value: data.totalexamsmapped,
        color: '#EA347E',
        highlight: "#EA347E",
        label: 'Exams Mapped',
        isChart:false,
        show:true
      },
      {
        value: data.attended,
        color: '#2AC96D',
        highlight: "#2AC96D",
        label: 'Exams Attended',
        isChart:false,
        show:true
      },
      {
        value: data.incompleteexams,
        color: '#F49251',
        highlight: "#F49251",
        label: 'Exams Incompleted',
        isChart:true,
        show:false
      },
      {
        value: data.passedexams,
        color: '#2AC96D',
        highlight: "#2AC96D",
        label: 'Exam Passed',
        isChart:true,
        show:false
      },
      {
        value: data.failedexams,
        color: '#2AC8C7',
        highlight: "#2AC8C7",
        label: 'Exam Failed',
        isChart:true,
        show:false
      },
      {
        value: data.yettoteaken,
        color: '#FBD12C',
        highlight: "#FBD12C",
        label: 'Exams Yet to be taken',
        isChart:true,
        show:false
      },

    ];

    var doughnutDataValues = [];
    var doughnutBackgroundColor = [];
    var doughnutLabel =[];
     for(let value of this.pdata){
           if(value.isChart){
            doughnutDataValues.push(value.value)
            doughnutBackgroundColor.push(value.color);
            doughnutLabel.push(value.label)
           }
     }

   

   var ctxp = this.doughnutCanvas.nativeElement;
    /***Doughnut Chart */
    var doughnutChart = new Chart(ctxp, {
      type: 'doughnut',
      data: {
        labels: doughnutLabel,
        datasets: [{
          data:doughnutDataValues,
          backgroundColor:doughnutBackgroundColor,
          hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
          hoverBorderColor: "rgba(234, 236, 244, 1)",
        }],
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 10,
          yPadding: 10,
          displayColors: false,
          caretPadding: 10,
        },
        legend: {
          display: false
        },
        animation :{
          animateRotate: true
        },
        title: {
          display: false,
          text: 'Status Based'
        },
        cutoutPercentage: 60,
      },
    });
   

  }
  

}
