import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CandidateDashboardPageRoutingModule } from './candidate-dashboard-routing.module';

import { CandidateDashboardPage } from './candidate-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CandidateDashboardPageRoutingModule
  ],
  declarations: [CandidateDashboardPage]
})
export class CandidateDashboardPageModule {}
