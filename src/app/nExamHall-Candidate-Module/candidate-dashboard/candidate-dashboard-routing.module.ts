import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CandidateDashboardPage } from './candidate-dashboard.page';

const routes: Routes = [
  {
    path: '',
    component: CandidateDashboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CandidateDashboardPageRoutingModule {}
