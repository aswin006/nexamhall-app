import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from '../../nExamHall-Services/local-storage.service'
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  theme:string;
  constructor(
    private localStorage : LocalStorageService,
  ) { }

  ngOnInit() {}
  ionViewWillEnter(){
    //debugger;
    
    this.localStorage.get('theme').then((res) =>{
      this.theme = res;
    });
  }

}
