import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NExamHallCandidateCoreModulePageRoutingModule } from './n-exam-hall-candidate-core-module-routing.module';

import { NExamHallCandidateCoreModulePage } from './n-exam-hall-candidate-core-module.page';

import {MainviewcontainerComponent} from '../nExamHall-Core-Utils/mainviewcontainer/mainviewcontainer.component';
import {BottomMenuBarComponent} from '../nExamHall-Core-Utils/bottom-menu-bar/bottom-menu-bar.component';
import {SideMenuBarComponent} from '../nExamHall-Core-Utils/side-menu-bar/side-menu-bar.component';
import {CandidateDashboardPage} from '../candidate-dashboard/candidate-dashboard.page'
import {SettingsComponent} from '../settings/settings.component';
import {MyexamsPage} from '../myexams/myexams.page'
import {AnalysisChartComponent} from '../analysis-chart/analysis-chart.component'
import {ProfileComponent} from '../profile/profile.component';
import {MockfilterModalComponent} from '../../nExamHall-Custom-Components/mockfilter-modal/mockfilter-modal.component'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NExamHallCandidateCoreModulePageRoutingModule
  ],
  declarations: [
    NExamHallCandidateCoreModulePage,
    MainviewcontainerComponent,
    BottomMenuBarComponent,
    SideMenuBarComponent,
    CandidateDashboardPage,
    SettingsComponent,
    MyexamsPage,
    AnalysisChartComponent,
    ProfileComponent,
    MockfilterModalComponent,
   
  ],
  exports: [
    MainviewcontainerComponent,
    BottomMenuBarComponent,
    SideMenuBarComponent,
    CandidateDashboardPage,
    SettingsComponent,
    MyexamsPage,
    AnalysisChartComponent,
    ProfileComponent,
    MockfilterModalComponent,
  
  ],
})
export class NExamHallCandidateCoreModulePageModule {}
