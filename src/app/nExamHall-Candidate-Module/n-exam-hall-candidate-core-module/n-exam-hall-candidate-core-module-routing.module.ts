import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NExamHallCandidateCoreModulePage } from './n-exam-hall-candidate-core-module.page';
import {CandidateDashboardPage} from '../candidate-dashboard/candidate-dashboard.page';
import {MyexamsPage} from '../myexams/myexams.page'
import {AnalysisChartComponent} from '../analysis-chart/analysis-chart.component';
import {ProfileComponent} from '../profile/profile.component'
import {SettingsComponent} from '../settings/settings.component';


const routes: Routes = [
  {
    path: 'dashboard',
    component: CandidateDashboardPage
  },
  {
    path: 'myexams',
    //component: MyexamsPage,
    children: [
      {
        path: '',
        component: MyexamsPage,
      },
      {
        path: 'mockexamlist/:examid/:testseriesid',
        //component:MocklistComponent,
      loadChildren: () => import('../mockexamlist/mockexamlist.module').then(m => m.MockexamlistPageModule)
      },
      {
        path: 'testseries/:examid',
        loadChildren: () => import('../testseries/testseries.module').then(m => m.TestseriesPageModule)
      },
      {
        path: 'intructionpage/:mockid/:examstatus/:examseq',
        loadChildren: () => import('../examinstruction/examinstruction.module').then(m => m.ExaminstructionPageModule)
      },
      {
        path: 'examconductor/:mockid/:examstatus/:examseq',
        loadChildren: () => import('../examconductor/examconductor.module').then(m => m.ExamconductorPageModule)
      }
    ]
  },
   
  {
    path: 'analysischart',
    component: AnalysisChartComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'settings',
    component: SettingsComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NExamHallCandidateCoreModulePageRoutingModule {}
