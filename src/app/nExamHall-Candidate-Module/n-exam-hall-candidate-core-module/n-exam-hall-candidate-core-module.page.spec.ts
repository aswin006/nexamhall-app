import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NExamHallCandidateCoreModulePage } from './n-exam-hall-candidate-core-module.page';

describe('NExamHallCandidateCoreModulePage', () => {
  let component: NExamHallCandidateCoreModulePage;
  let fixture: ComponentFixture<NExamHallCandidateCoreModulePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NExamHallCandidateCoreModulePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NExamHallCandidateCoreModulePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
