import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompletedresultsPage } from './completedresults.page';

const routes: Routes = [
  {
    path: '',
    component: CompletedresultsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompletedresultsPageRoutingModule {}
