import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompletedresultsPageRoutingModule } from './completedresults-routing.module';

import { CompletedresultsPage } from './completedresults.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CompletedresultsPageRoutingModule
  ],
  declarations: [CompletedresultsPage]
})
export class CompletedresultsPageModule {}
