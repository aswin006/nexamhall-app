import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddexamsPageRoutingModule } from './addexams-routing.module';

import { AddexamsPage } from './addexams.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddexamsPageRoutingModule
  ],
  declarations: [AddexamsPage]
})
export class AddexamsPageModule {}
