import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddexamsPage } from './addexams.page';

describe('AddexamsPage', () => {
  let component: AddexamsPage;
  let fixture: ComponentFixture<AddexamsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddexamsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddexamsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
