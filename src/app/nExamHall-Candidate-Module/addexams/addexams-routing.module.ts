import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddexamsPage } from './addexams.page';

const routes: Routes = [
  {
    path: '',
    component: AddexamsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddexamsPageRoutingModule {}
