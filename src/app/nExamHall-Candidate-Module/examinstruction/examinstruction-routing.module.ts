import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExaminstructionPage } from './examinstruction.page';

const routes: Routes = [
  {
    path: '',
    component: ExaminstructionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExaminstructionPageRoutingModule {}
