import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExaminstructionPage } from './examinstruction.page';

describe('ExaminstructionPage', () => {
  let component: ExaminstructionPage;
  let fixture: ComponentFixture<ExaminstructionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExaminstructionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExaminstructionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
