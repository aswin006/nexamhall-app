import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExaminstructionPageRoutingModule } from './examinstruction-routing.module';

import { ExaminstructionPage } from './examinstruction.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExaminstructionPageRoutingModule
  ],
  declarations: [ExaminstructionPage]
})
export class ExaminstructionPageModule {}
