import { Component, OnInit ,ViewChild, ElementRef} from '@angular/core';
import {ApiService} from '../../nExamHall-Services/api.service';
import {AlertService} from '../../nExamHall-Services/alert.service';
import {CommonServicesService} from '../../nExamHall-Services/common-services.service';
import {LocalStorageService} from '../../nExamHall-Services/local-storage.service'
import { IonSlides } from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";
import {LoaderService} from '../../nExamHall-Services/loader.service'

@Component({
  selector: 'app-examinstruction',
  templateUrl: './examinstruction.page.html',
  styleUrls: ['./examinstruction.page.scss'],
})
export class ExaminstructionPage implements OnInit {
//  @ViewChild("bottombar") bottomBar: ElementRef;

@ViewChild("slides", { static: true }) slides: IonSlides;

  defaultHref = "";
  theme: any;

  title: string;
  retrievedObject: any;
  loginId: any;
  mockid: any;
  examstatus: string;
  examseq: string;
  examid: any;
  testseriesid: any;
  examname: any;
  examDuration: any;
  totalQues: any;
  markPerQues: any;
  negativeMark: any;
  languageSuport: any;
  examinstruction: any;
  instructionData: object = {};
  generalInst: any;
  examInst: any;

  selectOptions = {
    header: "Select a Language",
  };
  Declaration: any;
  DeclarationTitle: any;
  isAccepted: boolean;
  currentLanguage: string;

  constructor(
    private apiService : ApiService,
    private alertService : AlertService,
    private localStorage : LocalStorageService,
    private commonService : CommonServicesService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private loader : LoaderService
  ) { 
    
   
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    debugger;
    this.mockid = this.activeRoute.snapshot.paramMap.get("mockid");
    this.examstatus = this.activeRoute.snapshot.paramMap.get("examstatus");
    this.examseq = this.activeRoute.snapshot.paramMap.get("examseq");

    this.title = "General Instructions";

    this.instructionData = this.commonService.instructionData();
    this.localStorage
      .getObject("exam_details")
      .then((result) => {
        if (result != null) {
          this.retrievedObject = result;
          this.examid = this.retrievedObject.examid;
          this.testseriesid = this.retrievedObject.testseriesid;
          this.examname = this.retrievedObject.examname;
          this.examDuration = this.retrievedObject.timeduration;
          this.totalQues = this.retrievedObject.questioncount;
          this.markPerQues = this.retrievedObject.markPerQues;
          this.negativeMark = this.retrievedObject.negativeMark;
          this.languageSuport = this.retrievedObject.langsupport;
          this.examinstruction = this.retrievedObject.instructions;
          //console.log("this.exam_details :" + this.retrievedObject);
          for (let i = 0; i < this.languageSuport.length; i++) {
            var langCode = this.languageSuport[i].lngcode;
            // var index = this.examinstruction.indexOf(langCode);
            var index = this.examinstruction.findIndex(function (item) {
              return item.langcode == langCode;
            });
            if (index > -1) {
              this.instructionData[langCode].examInst = this.examinstruction[
                index
              ].instructions;
            }
          }
          console.log(this.instructionData);

          this.generalInst = this.instructionData["en"].generalInst;
          this.examInst = this.instructionData["en"].examInst;
          this.Declaration = this.instructionData["en"].Declaration;
          this.DeclarationTitle = this.instructionData["en"].DeclarationTitle;
          this.currentLanguage = this.languageSuport[0].lngcode;
          this.isAccepted = false;
        }
      })
      .catch((e) => {
        console.log("error: " + e);
      });
  }

  ionViewDidEnter() {
    this.localStorage.get('theme').then((res) =>{
      this.theme = res;
      // const tabBar = this.bottomBar.nativeElement
      // tabBar.style.display = 'none';
      this.commonService.hideTabs();
      this.defaultHref = `nexamhallcandidate/myexams/mockexamlist/${this.examid}/${this.testseriesid}`;
    })
    
  };

  onSlideChangeStart(event) {}
  slideChanged(langCode) {
    // let currentIndex = this.slides.getActiveIndex();
    // console.log("Current index is", currentIndex);
    this.slides.getActiveIndex().then((index) => {
      console.log(index);
      if (index == 0) {
        if (langCode == "en") {
          this.title = this.instructionData[langCode].heading1;
        } else if (langCode == "ta") {
          this.title = this.instructionData[langCode].heading1;
        }
      } else if (index == 1) {
        if (langCode == "en") {
          this.title = this.instructionData[langCode].heading2;
        } else if (langCode == "ta") {
          this.title = this.instructionData[langCode].heading2;
        }
      }
    });
  }

  onCancel() {
    console.log("cancel");
  }
  onChange(currentLanguage) {
    this.currentLanguage = currentLanguage;
    this.slideChanged(this.currentLanguage);
    this.generalInst = this.instructionData[currentLanguage].generalInst;
    this.examInst = this.instructionData[currentLanguage].examInst;
    this.Declaration = this.instructionData[currentLanguage].Declaration;
    this.DeclarationTitle = this.instructionData[
      currentLanguage
    ].DeclarationTitle;
    console.log(currentLanguage);
  }

  checked() {}

  StartTestConfirmation(isAccepted) {
    if (isAccepted) {
      this.router.navigateByUrl(
        "/nexamhallcandidate/myexams/examconductor/" +
          this.mockid +
          "/" +
          this.examstatus +
          "/" +
          this.examseq
      );
    } else {
      var msg = "Please select declaration checkbox";
      this.alertService.presentAlert(msg);
    }
  }
}
