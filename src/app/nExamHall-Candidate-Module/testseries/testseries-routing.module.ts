import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestseriesPage } from './testseries.page';

const routes: Routes = [
  {
    path: '',
    component: TestseriesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestseriesPageRoutingModule {}
