import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestseriesPageRoutingModule } from './testseries-routing.module';

import { TestseriesPage } from './testseries.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestseriesPageRoutingModule
  ],
  declarations: [TestseriesPage]
})
export class TestseriesPageModule {}
