import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TestseriesPage } from './testseries.page';

describe('TestseriesPage', () => {
  let component: TestseriesPage;
  let fixture: ComponentFixture<TestseriesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestseriesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TestseriesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
