import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../../nExamHall-Services/local-storage.service';
@Component({
  selector: 'app-analysis-chart',
  templateUrl: './analysis-chart.component.html',
  styleUrls: ['./analysis-chart.component.scss'],
})
export class AnalysisChartComponent implements OnInit {
  theme: any;

  constructor(
    private localStorage : LocalStorageService,
  ) { }

  ngOnInit() {}
  ionViewWillEnter(){
    //debugger;
    
    this.localStorage.get('theme').then((res) =>{
      this.theme = res;
    });
  }
}
