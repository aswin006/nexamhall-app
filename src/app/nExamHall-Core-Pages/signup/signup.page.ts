import { Component, OnInit } from '@angular/core';
import {SignupData} from '../../nExamHall-Model/signup-data'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
 
   signUpData : SignupData 
  defaultHref = '';
  
  constructor() {
  
   } 

  ngOnInit() {
    
  }
  ionViewDidEnter(){
    this.defaultHref = '/nexamhallcore/auth/login';
    
  }

}
