import { Component, OnInit, } from '@angular/core';
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { LoginData } from "../../nExamHall-Model/login-data";
import {ToastService} from "../../nExamHall-Services/toast.service";
import {ApiService} from '../../nExamHall-Services/api.service'
import {LocalStorageService} from '../../nExamHall-Services/local-storage.service';
import {CommonServicesService} from '../../nExamHall-Services/common-services.service';
import {LoaderService} from '../../nExamHall-Services/loader.service'
import { Platform , } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginData : LoginData = {
    instituteuserid:"",
    loginId:"",
    password:"",
    subdomain:""
  }
  submitted = false;
  instituteData:any;
  errormsg:string;
  instituteLogo : string;
  defaultHref = "";
  publicsupport:boolean;
  enableOtpScreen = true;
  

  constructor(
    public router: Router,
    public apiService : ApiService,
    public storage :LocalStorageService,
    public toast : ToastService,
    public commonService : CommonServicesService,
    public loader : LoaderService,
    private platform: Platform,
  

  ) { 
    // this.platform.backButton.subscribeWithPriority(10, () => {
    //   console.log('Handler was called!');
    // });
  }
  

 
ionViewWillEnter(){
  this.loader.showImgLoader();
         this.storage.getObject('instituteData').then((data) =>{
           debugger;
              this.instituteData = data;
              this.instituteLogo = this.commonService.addImageUrl(this.instituteData.userimage,"","")
             this.loginData.instituteuserid = this.instituteData.instituteuserid;
             this.loginData.subdomain = this.instituteData.subdomain;

             this.publicsupport = this.instituteData.features.publicsupport
             console.log('instdata',this.instituteData);
            
             if (this.instituteData.features.mailotp == false && this.instituteData.features.mobileotp == false) {
                 this.enableOtpScreen = false;
             }
             this.loader.hideLoader();
         })
}
ionViewDidEnter(){
  this.defaultHref = '/nexamhallcore/auth';
}
  ngOnInit() {
  }

  routeProvider(value){
    var path = this.commonService.pathService(value)
    this.router.navigateByUrl(path);
  }
 

  onSignup(){
    this.routeProvider('signup')
  };
 goToForgotPwd = function () {
  this.routeProvider('otp')
};
directLogin() {
  this.storage.getObject('instituteData').then((res) =>{
    var directLoginData = res;
    if (this.enableOtpScreen) {
      var otpdata = {
        loginId:directLoginData.loginId.toLowerCase(),
        instituteuserid:this.instituteData.instituteuserid
      } 
      this.storage.setObject('otpData',otpdata).then((res) =>{
            this.apiService.sendAuthOTP(otpdata);
            this.routeProvider('otp')
      })
      
  } else {
     this.loginData.loginId = directLoginData.loginId.toLowerCase();
     this.loginData.password  = directLoginData.password
     this.loginData.instituteuserid = this.instituteData.instituteuserid;
     this.loginData.subdomain = this.instituteData.subdomain;
     this.loginAuth()
  }
  });
}
loginAuth(){
  this.apiService.loginauth(this.loginData).subscribe((response) => {
    this.loader.showImgLoader();
    console.log(response)
    if (response == null || response == undefined) {
      var errormsg = "Connectivity problem, please try later";
      this.toast.showToast(errormsg, 2000, 'top', 'danger')

    } else {
      if (response["statuscode"] == "NT-200") {
        this.submitted = true;
        var responseLogindata = response["userdata"];

        this.storage.set('token', responseLogindata.token);
        if (responseLogindata.status == 1) {

          switch (responseLogindata.userType) {
            case "1" || '3':
              this.toast.showToast('Available  candidates only', 2000, 'top', 'danger')
              break;
            case "2":
              this.toast.showToast(response["message"], 2000, 'top', 'success');
              this.storage.setObject('candidateUserdata', responseLogindata)
              this.storage.set('token', responseLogindata["token"]);
              this.routeProvider('candidatedashboard')
              //this.router.navigateByUrl("/nexamhallcandidate/dashboard");
              break;
            // case "3":
            //   this.toast.showToast('Available  candidates only', 2000, 'top', 'danger')

            //   break;
            default:
          }

        }

      } else {
        var statuscode = response["statuscode"];
        switch (statuscode) {
          case 'NT-202':
            this.errormsg = "Email Id not registered";
            this.toast.showToast(this.errormsg, 2000, 'top', 'danger')
            break;
          case 'NT-404':
            this.errormsg = "Password Incorrect";
            this.toast.showToast(this.errormsg, 2000, 'top', 'danger')
            break;
          case 'NT-203':
            this.errormsg = "Please Check your Mail to Activate your Account";
            this.toast.showToast(this.errormsg, 2000, 'top', 'danger')

            // $scope.directLogin();
            break;
          case 'NT-208':
            this.errormsg = response["message"];
            this.toast.showToast(this.errormsg, 2000, 'top', 'danger')
            break;
          case 'NT-212':
            this.errormsg = response["message"];
            this.toast.showToast(this.errormsg, 2000, 'top', 'danger')
            break;

          default:

        }
      }
    }
    this.loader.hideLoader();
    //this.router.navigateByUrl("/app/tabs/home");
  })
}
  onLogin(form: NgForm){
    if (form.valid) {
        if(!this.commonService.checkEmail(this.loginData.loginId)){
          var errormsg = "Please enter valid email";
          this.toast.showToast(errormsg,2000,'top','danger')
        }else{
          
        this.loginAuth();
         
       }
      
    }else{
      alert('error')
    }
  }

 
 

}
