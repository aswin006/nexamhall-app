import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../nExamHall-Services/api.service';
import {ToastService} from '../../nExamHall-Services/toast.service';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../nExamHall-Services/local-storage.service'
@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  institutecode:string;

  constructor(
    public apiService : ApiService,
    public toast : ToastService,
    public router : Router,
    public localStorage : LocalStorageService
  ) {
   
   }

  ngOnInit() {
  }
  checkInstCode(){
    var subdomain  = {
      "subdomain":this.institutecode
    }
    this.apiService.checkCredentials(subdomain).subscribe((res) =>{
      console.log(res['userdata'])
      var userdata = res['userdata'];
      if(userdata != null){
         this.localStorage.setObject('instituteData',userdata).then((res) =>{
          this.router.navigateByUrl("/nexamhallcore/auth/login", { replaceUrl: true });
         })
       
      }else{
        var errormsg = "Please enter valid institutecode";
        this.toast.showToast(errormsg,2000,'top','dark')
      }
    })
    
  }

}
