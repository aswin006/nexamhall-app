import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthPage } from './auth.page';
import {LoginPage} from '../login/login.page'

// const routes: Routes = [
//   {
//     path: 'auth',
//     component: AuthPage,
//     children: [
//       {
//         path: "instituteauth",
//         children: [
//           {
//             path: "",
//             loadChildren: () =>
//               import("../login/login.module").then((m) => m.LoginPageModule),
//           },
//         ],
//       },
//       {
//         path: "login",
//         children: [
//           {
//             path: "",
//             loadChildren: () =>
//               import("../login/login.module").then((m) => m.LoginPageModule),
//           },
//         ],
//       },
//       {
//         path: "signup",
//         children: [
//           {
//             path: "",
//             loadChildren: () =>
//               import("../signup/signup.module").then((m) => m.SignupPageModule),
//           },
//         ],
//       },
//       {
//         path: "forgotpassword",
//         children: [
//           {
//             path: "",
//             loadChildren: () =>
//               import("../forgotpassword/forgotpassword.module").then((m) => m.ForgotpasswordPageModule),
//           },
//         ],
//       },
//       {
//         path: "resetpassword",
//         children: [
//           {
//             path: "",
//             loadChildren: () =>
//               import("../resetpassword/resetpassword.module").then((m) => m.ResetpasswordPageModule),
//           },
//         ],
//       },
//       {
//         path: "otp",
//         children: [
//           {
//             path: "",
//             loadChildren: () =>
//               import("../otpscreen/otpscreen.module").then((m) => m.OtpscreenPageModule),
//           },
//         ],
//       },
//       {
//         path: "",
//         redirectTo: "/auth",
//         pathMatch: "full",
//       },
//    ]
//   }
// ];

const routes = [
  {
    path: 'auth',
    children: [
      {
        path: '',
        component: AuthPage,
      },
      {
        path: 'login',
        loadChildren: () => import('../login/login.module').then(m => m.LoginPageModule)
      },
      {
        path: 'signup',
        loadChildren: () => import('../signup/signup.module').then(m => m.SignupPageModule)
      },
      {
        path: 'forgot',
        loadChildren: () => import('../forgotpassword/forgotpassword.module').then(m => m.ForgotpasswordPageModule)
      },
      {
        path: 'resetpassword',
        loadChildren: () => import('../resetpassword/resetpassword.module').then(m => m.ResetpasswordPageModule)
      },
      {
        path: 'otp',
        loadChildren: () => import('../otpscreen/otpscreen.module').then(m => m.OtpscreenPageModule)
      }
    ]
  },
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthPageRoutingModule {}
