import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtpscreenPage } from './otpscreen.page';

describe('OtpscreenPage', () => {
  let component: OtpscreenPage;
  let fixture: ComponentFixture<OtpscreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpscreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtpscreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
