import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-otpscreen',
  templateUrl: './otpscreen.page.html',
  styleUrls: ['./otpscreen.page.scss'],
})
export class OtpscreenPage implements OnInit {

  defaultHref = '';
  Pin: String ="";
  ShowPin: Boolean = false;
  constructor() {
  
   } 

  ngOnInit() {
    
  }
  ionViewDidEnter(){
    this.defaultHref = '/nexamhallcore/auth/login';
    
  }

  
  eventCapture(event) {
    this.ShowPin = false;
    this.Pin=event;
  }

  showPin() {
    this.ShowPin = !this.ShowPin;
  }

}
