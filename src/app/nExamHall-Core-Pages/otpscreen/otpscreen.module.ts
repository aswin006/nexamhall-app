import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtpscreenPageRoutingModule } from './otpscreen-routing.module';

import { OtpscreenPage } from './otpscreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OtpscreenPageRoutingModule
  ],
  declarations: [OtpscreenPage]
})
export class OtpscreenPageModule {}
