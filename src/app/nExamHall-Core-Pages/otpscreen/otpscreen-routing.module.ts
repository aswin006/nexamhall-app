import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtpscreenPage } from './otpscreen.page';

const routes: Routes = [
  {
    path: '',
    component: OtpscreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtpscreenPageRoutingModule {}
