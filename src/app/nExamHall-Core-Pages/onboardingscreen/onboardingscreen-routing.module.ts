import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnboardingscreenPage } from './onboardingscreen.page';

const routes: Routes = [
  {
    path: '',
    component: OnboardingscreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnboardingscreenPageRoutingModule {}
