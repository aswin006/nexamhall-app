import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnboardingscreenPageRoutingModule } from './onboardingscreen-routing.module';

import { OnboardingscreenPage } from './onboardingscreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OnboardingscreenPageRoutingModule
  ],
  declarations: [OnboardingscreenPage]
})
export class OnboardingscreenPageModule {}
