import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { OnboardingCheckService } from './nExamHall-Providers/onboarding-check.service';
import {MainviewcontainerComponent} from './nExamHall-Candidate-Module/nExamHall-Core-Utils/mainviewcontainer/mainviewcontainer.component';
import {ThemeSwitcherService} from './nExamHall-Services/theme-switcher.service'
const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'onboardingscreen',
    pathMatch: 'full'
  },
  {
    path: 'nexamhallcandidate',
    component:MainviewcontainerComponent,
    loadChildren: () => import('./nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.module').then( m => m.NExamHallCandidateCoreModulePageModule),
    canLoad: [ThemeSwitcherService]
  },
  {
    path: 'nexamhallcore',
    loadChildren: () => import('./nExamHall-Core-Pages/auth/auth.module').then( m => m.AuthPageModule)
  },
  {
    path: 'onboardingscreen',
    loadChildren: () => import('./nExamHall-Core-Pages/onboardingscreen/onboardingscreen.module').then( m => m.OnboardingscreenPageModule),
    canLoad: [OnboardingCheckService]
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
