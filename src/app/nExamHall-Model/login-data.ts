export interface LoginData {
    instituteuserid: string;
    loginId: string;
    password:string;
    subdomain:string;
}
