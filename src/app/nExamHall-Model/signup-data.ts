export interface SignupData {
   instituteName: string;
   instituteuserid: string;
   loginId: string;
   firstName: string;
   lastName: string;
   fatherName: string;
   motherName: string;
   contactNumber: {
       Contact1:string;
       Contact2: string;
    };
   phone: {
       phoneno:number;
       whatsupno: number;
    };
   persons: {
       person1:string;
       person2: string
    };
   webaddress: string;
   dateOfbirth: string;
   stateName:string;
   password: string;
   repasswd: string;
   userType:"2";
   Address: {
       Line1: string;
       Line2:string;
       Line3: string;
    };
   city: string;
   pincode: string;
   Remarks: string;
   enteredBy:"System";
   enteredon:string;
   lastModifiedby: string;
   userimage: string;
    //status:Disable;//aswin login userstatus change 08-03-19
   status: 0; //aswin login userstatus change 25-07-19
   isadminuser: true;
   candidateusertype:"3"; //test series support changes 15-03-20
   resourcesupport: false;
   logintype:"N";
   social: {
       fb: false;
       gmail: false;
       twitter: false;
    };
   inst_details: {
       instuser:string;
       branch: string;
       rolenumber: string;
    };
   expiry: {
       startdate: string;
       enddate: string;
    }
}

