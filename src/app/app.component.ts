import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {LocalStorageService} from './nExamHall-Services/local-storage.service'
import {ThemeSwitcherService} from './nExamHall-Services/theme-switcher.service'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  theme: string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private localStorage : LocalStorageService,
    private themeSwitcherService : ThemeSwitcherService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
     
   
  }
  // ionViewWillEnter(){
  //   debugger;
    
  //   this.localStorage.get('theme').then((res) =>{
  //     if(res == null){
  //       this.theme = 'theme-richgreen';
        
  //     }else{
  //       this.theme = res;
  //     }
  //     this.localStorage.set('theme',this.theme).then((res) =>{
  //       this.themeSwitcherService.setThemeProperty(this.theme)
  //     })
      
  //   });
  // }
  
}
