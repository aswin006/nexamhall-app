import { Injectable } from '@angular/core';
import {ToastController} from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class ToastService {
 private myToast: any;

  constructor(
    public toast : ToastController,
  ) { }

  showToast(msg,duration,position,color){
    this.myToast = this.toast.create({
        message : msg,
        duration:duration,
        position:position,
        color:color,
        animated:true,
        mode:'ios'
    }).then((toastData)=>{
       toastData.present();
    })
  }
  hideToast(){
    this.myToast = this.toast.dismiss();
  }
}
