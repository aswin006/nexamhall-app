import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {AlertController} from "@ionic/angular"

@Injectable({
  providedIn: 'root'
})
export class CommonServicesService {

  commonImgUrl = environment.commonImgUrl;
  apiUrl =  environment.apiUrl;
  returnPath :string;
  constructor(
    public alertCtrl: AlertController
  ) { }

  addImageUrl(img,name,size) {
    debugger;
    var first_part = img.substring(0, 8);

    if (first_part === 'http://' || first_part === 'https://') {
        var image:any = img
    } else {
      if(img == "" || img == null || img == undefined){
        var image:any = this.letterLogo(name,size,1)
      }else{
        var image:any = this.commonImgUrl + img
      }
       
    }
    return image
   }

   showExitConfirm() {
    this.alertCtrl.create({
      header: 'Exit App',
      message: 'Do you want to close the app?',
      backdropDismiss: false,
      mode:'ios',
      buttons: [{
        text: 'Stay',
        role: 'cancel',
        handler: () => {
          console.log('Application exit prevented!');
        }
      }, {
        text: 'Exit',
        handler: () => {
          navigator['app'].exitApp();
        }
      }]
    })
      .then(alert => {
        alert.present();
      });
  }



  checkEmail(email){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
if(!re.test(email)) {
  // Invalid Email
  return false;
}else{
  return true;
}
  }

 
  pathService(pathname){
    switch (pathname) {
      case "auth":
        this.returnPath = '/nexamhallcore/auth';
        break;
      case "login":
        this.returnPath = '/nexamhallcore/auth/login';
        break;
      case "signup":
        this.returnPath = '/nexamhallcore/auth/signup';
        break;
      case "forgot":
        this.returnPath = '/nexamhallcore/auth/forgot';
        break;
      case "resetpassword":
        this.returnPath = '/nexamhallcore/auth/resetpassword';
        break;
      case "otp":
        this.returnPath = '/nexamhallcore/auth/otp';
        break;
        case "candidatedashboard":
        this.returnPath = '/nexamhallcandidate/dashboard';
        break;
      default:
    }
    return this.returnPath;
  };




  letterLogo(name, size, wordcount) {
    name = name || "";
    size = size || 60;
    wordcount = wordcount || 1;

    var colours = [
        "#1abc9c",
        "#2ecc71",
        "#3498db",
        "#9b59b6",
        "#34495e",
        "#16a085",
        "#27ae60",
        "#2980b9",
        "#8e44ad",
        "#2c3e50",
        "#f1c40f",
        "#e67e22",
        "#e74c3c",
        "#ecf0f1",
        "#95a5a6",
        "#f39c12",
        "#d35400",
        "#c0392b",
        "#bdc3c7",
        "#7f8c8d",
      ],
      nameSplit = String(name).toUpperCase().split(" "),
      initials,
      charIndex,
      colourIndex,
      canvas,
      context,
      dataURI;

    // if (nameSplit.length == 1) {
    //     initials = nameSplit[0] ? nameSplit[0].charAt(0) : '?';
    // } else {
    //     initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
    // }
    if (wordcount == 1) {
      initials = nameSplit[0] ? nameSplit[0].charAt(0) : "?";
    } else {
      initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
    }

    if (window.devicePixelRatio) {
      size = size * window.devicePixelRatio;
    }

    charIndex = (initials == "?" ? 72 : initials.charCodeAt(0)) - 64;
    colourIndex = charIndex % 10;
    canvas = document.createElement("canvas");
    canvas.width = size;
    canvas.height = size;
    context = canvas.getContext("2d");

    context.fillStyle = colours[colourIndex - 1];
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.font = Math.round(canvas.width / 2) + "px Arial";
    context.textAlign = "center";
    context.fillStyle = "#FFF";
    context.fillText(initials, size / 2, size / 1.5);

    dataURI = canvas.toDataURL();
    canvas = null;

    return dataURI;
  }
    
  public hideTabs() {
    const tabBar = document.getElementById('bottombar');
    if (tabBar.style.display !== 'none') tabBar.style.display = 'none';
  }
  public showTabs() {
    const tabBar = document.getElementById('bottombar');
    if (tabBar.style.display !== 'flex') tabBar.style.display = 'flex';
  };


  instructionData() {
    var data = [
      {
        ta: {
          heading1: "பொது வழிமுறை",
          heading2: "தேர்வு அறிவுறுத்தல்",
          generalInst:
            '<div><ol start="1"><li><p class="text-left">திரையின் மேல் வலது மூலையில் உள்ள கவுண்டவுன் டைமர் நீங்கள் தேர்வை முடிக்க மீதமுள்ள நேரத்தைக் காண்பிக்கும். டைமர் பூஜ்ஜியத்தை அடையும் போது, தேர்வு தானாகவே முடிவடையும். நீங்கள் தேர்வை நிறுத்தவோ அல்லது உங்கள் தாளைச் சமர்ப்பிக்கவோ தேவையில்லை.</p></li></ol><ol start="2"><li><p class="text-left">திரையின் வலது பக்கத்தில் காட்டப்படும் கேள்வி தட்டு பின்வரும் கேள்விகளில் ஒன்றைப் பயன்படுத்தி ஒவ்வொரு கேள்வியின் நிலையையும் காண்பிக்கும்:</p><ul class="que-ans-states text-left"><li><button class="intruct_btn answer"></button> என்ற கேள்விக்கு நீங்கள் பதிலளித்துள்ளீர்கள்..</li><li><button class="intruct_btn notvist"></button> கேள்வியை நீங்கள் இதுவரை பார்வையிடவில்லை.</li><li><button class="intruct_btn not_ans"></button> என்ற கேள்விக்கு நீங்கள் பதிலளிக்கவில்லை.</li><li><button class="intruct_btn Markedans"></button> மதிப்பாய்வுக்கான கேள்வியைக் குறித்துள்ளீர்கள், ஆனால் பதிலளித்தீர்கள்.</li><li><button class="intruct_btn Markfor_review"></button> மதிப்பாய்வுக்கான கேள்வியை நீங்கள் குறித்துள்ளீர்கள், ஆனால் பதிலளிக்கவில்லை.</li></ul></li></ol><ol start="3"><li><p class="text-left">ஒரு கேள்விக்கான மதிப்பாய்வுக்கான நிலை நீங்கள் அந்த கேள்வியை மீண்டும் பார்க்க விரும்புகிறீர்கள் என்பதைக் குறிக்கிறது. ஒரு கேள்விக்கு பதிலளிக்கப்பட்டாலும், மதிப்பாய்வுக்காக குறிக்கப்பட்டால், வேட்பாளரால் அந்தஸ்தை மாற்றியமைக்காவிட்டால் பதில் மதிப்பீட்டிற்கு பரிசீலிக்கப்படும்.</p></li></ol><ol start="4"><li><p class="text-left">பல தேர்வு வகை கேள்விக்கு பதிலளிப்பதற்கான செயல்முறை:</p><ul class="text-left"><li>நீங்கள் தேர்ந்தெடுத்த பதிலைத் தேர்வுநீக்க, தேர்ந்தெடுக்கப்பட்ட விருப்பத்தின் குமிழியை மீண்டும் கிளிக் செய்யவும் அல்லது தெளிவான முடிவு பொத்தானைக் கிளிக் செய்யவும்.</li><li>நீங்கள் தேர்ந்தெடுத்த பதிலை மாற்ற, மற்றொரு விருப்பத்தின் குமிழியைக் கிளிக் செய்க.</li><li>உங்கள் பதிலைச் சேமிக்க, சேமி &amp; அடுத்து என்பதைக் கிளிக் செய்ய வேண்டும்.</li><li>அந்த கேள்விக்கு நேரடியாகச் செல்ல கேள்வி தட்டில் உள்ள கேள்வி எண்ணைக் கிளிக் செய்க.</li><li>4 தேர்வுகளுக்கு முன் வைக்கப்பட்டுள்ள குமிழியைக் கிளிக் செய்வதன் மூலம் பல தேர்வு வகை கேள்விக்கான பதிலைத் தேர்ந்தெடுக்கவும்.</li><li>தற்போதைய கேள்விக்கான உங்கள் பதிலைச் சேமிக்க சேமி &amp; அடுத்து என்பதைக் கிளிக் செய்து அடுத்த கேள்விக்குச் செல்லவும்.</li><li>தற்போதைய கேள்விக்கான உங்கள் பதிலைச் சேமிக்க மார்க் ஃபார் ரிவியூவைக் கிளிக் செய்து மதிப்பாய்வுக்காகக் குறிக்கவும், பின்னர் அடுத்த கேள்விக்குச் செல்லவும்.</li></ul></li></ol><ol start="5"><li><p class="text-left">ஏற்கனவே பதிலளிக்கப்பட்ட கேள்விக்கு உங்கள் பதிலை மாற்ற, முதலில் பதிலளிக்க அந்த கேள்வியைத் தேர்ந்தெடுத்து, பின்னர் அந்த வகை கேள்விக்கு பதிலளிப்பதற்கான நடைமுறையைப் பின்பற்றவும்.</p></li></ol><ol start="6"><li><p class="text-left">முந்தைய கேள்விக்கான பதிலைச் சேமிக்காமல் ஒரு கேள்வி எண்ணைக் கிளிக் செய்வதன் மூலம் நீங்கள் நேரடியாக மற்றொரு கேள்விக்குச் சென்றால், தற்போதைய கேள்விக்கான உங்கள் பதில் சேமிக்கப்படாது.</p></li></ol><ol start="7"><li><p class="text-left">ஒரு கேள்விக்கான மார்க் ஃபார் ரிவியூ நிலையை நீங்கள் மீண்டும் அந்த கேள்வியைப் பார்க்க விரும்புகிறீர்கள் என்பதைக் குறிக்கிறது. ஒரு கேள்விக்கு பதிலளிக்கப்பட்டாலும், மதிப்பாய்வுக்காக குறிக்கப்பட்டால், வேட்பாளரால் அந்தஸ்தை மாற்றியமைக்காவிட்டால் பதில் மதிப்பீட்டிற்கு பரிசீலிக்கப்படும்.</p></li></ol><ol start="8"><li><p class="text-left">பதில்களுக்குப் பிறகு சேமிக்கப்பட்ட அல்லது மதிப்பாய்வு செய்ய குறிக்கப்பட்ட கேள்விகள் மட்டுமே மதிப்பீட்டிற்கு பரிசீலிக்கப்படும் என்பதை நினைவில் கொள்க.</p></li></ol><ol start="9"><li><p class="text-left">ஒரு பிரிவின் கடைசி கேள்விக்கான சேமி &amp; அடுத்து பொத்தானைக் கிளிக் செய்த பிறகு, அடுத்த பிரிவின் முதல் கேள்விக்கு நீங்கள் தானாக அழைத்துச் செல்லப்படுவீர்கள்.</p></li></ol></div>',
          examInst: "",
          declareSelect:
            '<p><label for="language">உங்கள் இயல்புநிலை மொழியைத் தேர்வுசெய்க:</label> <select class="" ng-model="defaultlanguage" ng-change="select_change(defaultlanguage)" placeholder="select"  ng-options="language.lngcode as language.lngname for language in languageSuport" ><option value="" disabled selected hidden>Select</option></select></p>',
          DeclarationTitle:
            '<p class="dec_margin text-left"><strong class="ng-binding"> உறுதிமொழி: </strong></p>',
          Declaration:
            '<span class="res exam-instructions-declare_box">  எல்லா வழிமுறைகளையும் கவனமாகப் படித்து அவற்றைப் புரிந்துகொண்டேன். இந்த தேர்வில் ஏமாற்று அல்லது நியாயமற்ற வழிகளைப் பயன்படுத்த வேண்டாம் என்று ஒப்புக்கொள்கிறேன். எனது சொந்த அல்லது வேறு ஒருவருக்கு எந்தவொரு நியாயமற்ற வழிகளையும் பயன்படுத்துவது எனது உடனடி தகுதியிழப்புக்கு வழிவகுக்கும் என்பதை நான் புரிந்துகொள்கிறேன். இந்த விஷயங்களில் eParikshaa.com இன் முடிவு இறுதியானது மற்றும் மேல்முறையீடு செய்ய முடியாது. </span>',
        },
        en: {
          heading1: "General Instructions",
          heading2: "Exam Instructions",
          generalInst:
            '<div><ol start="1"><li><p class="text-left">The clock will be set at the server. The countdown timer at the top right corner of screen will display the remaining time available for you to complete the examination. When the timer reaches zero, the examination will end by itself. You need not terminate the examination or submit your paper.</p></li></ol><ol start="2"><li><p class="text-left">The Question Palette displayed on the right side of screen will show the status of each question using one of the following symbols:</p><ul class="que-ans-states text-left"><li><button class="intruct_btn answer"></button> You have answered the question.</li><li><button class="intruct_btn notvist"></button> You have not visited the question yet.</li><li><button class="intruct_btn not_ans"></button> You have not answered the question.</li><li><button class="intruct_btn Markedans"></button> You have marked the question for review but answered</li><li><button class="intruct_btn Markfor_review"></button> You have marked the question for review but NOT answered.</li></ul></li></ol><ol start="3"><li><p class="text-left">The <strong>Mark For Review </strong> status for a question simply indicates that you would like to look at that question again. If a question is answered, but marked for review, then the answer will be considered for evaluation unless the status is modified by the candidate.</p></li></ol><ol start="4"><li><p class="text-left">Procedure for answering a multiple choice type question:</p><ul class="text-left"><li>To deselect your chosen answer, click on the bubble of the chosen option again or click on the Clear Result button.</li><li>To change your chosen answer, click on the bubble of another option.</li><li>To save your answer, you MUST click on the Save &amp; Next.</li><li>Click on the question number in the Question Palette to go to that question directly.</li><li>Select an answer for a multiple choice type question by clicking on the bubble placed before the 4 choices.</li><li>Click on Save &amp; Next to save your answer for the current question and then go to the next question.</li><li>Click on Mark for Review to save your answer for the current question and also mark it for review, and then go to the next question.</li></ul></li></ol><ol start="5"><li><p class="text-left">To change your answer to a question that has already been answered, first select that question for answering and then follow the procedure for answering that type of question</p></li></ol><ol start="6"><li><p class="text-left">that your answer for the current question will not be saved, if you navigate to another question directly by clicking on a question number without saving the answer to the previous question.</p></li></ol><ol start="7"><li><p class="text-left">Note that Mark For Review status for a question simply indicates that you would like to look at that question again. If a question is answered, but marked for review, then the answer will be considered for evaluation unless the status is modified by the candidate</p></li></ol><ol start="8"><li><p class="text-left">Note that ONLY Questions for which answers are saved or marked for review after answering will be considered for evaluation.</p></li></ol><ol start="9"><li><p class="text-left">After clicking the Save &amp; Next button for the last question in a Section, you will automatically be taken to the first question of the next Section in sequence.</p></li></ol></div>',
          examInst: "",
          declareSelect:
            '<p><label for="language">Choose your default language:</label> <select class="" ng-model="defaultlanguage" ng-change="select_change(defaultlanguage)" placeholder="select"  ng-options="language.lngcode as language.lngname for language in languageSuport" ><option value="" disabled selected hidden>Select</option></select></p>',
          DeclarationTitle:
            '<p class="dec_margin text-left"><strong class="ng-binding"> Declaration </strong></p>',
          Declaration:
            '<span class="res exam-instructions-declare_box"> I have read all the instructions carefully and have understood them. I agree not to cheat or use unfair means in this examination. I understand that using unfair means of any sort for my own or someone else&rsquo;s advantage will lead to my immediate disqualification. The decision of eParikshaa.com will be final in these matters and cannot be appealed. </span>',
        },
      },
    ];

    return data[0];
  }


}
