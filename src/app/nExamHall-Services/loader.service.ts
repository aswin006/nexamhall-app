import { Injectable } from '@angular/core';
import {LoadingController} from '@ionic/angular'

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  isLoading = false;

  constructor(
    public loadingController : LoadingController
  ) { }


  // showImgLoader(){
  //   this.loadingController.create({
  //    // message:'Please wait ...',
  //     mode:'ios',
  //     message: '<div class="text-center"><img src="../assets/images/icons/book.gif"   /> <br />Please wait..</div>',
  //     spinner: null, 
    
  //     cssClass:'custom-loader-class',
     
  //   }).then((res) =>{
  //     res.present();
  //   })
  // };
  // showNormalLoader(){
   
  //   this.loadingController.create({
  //     message:'Please wait ...',
  //     mode:'ios',
     
  //   }).then((res) =>{
  //     res.present();
  //   })
  // };


  // async hideLoader(){
   
  //   let topLoader = await this.loadingController.getTop();
  // while (topLoader) {
  //   if (!(await topLoader.dismiss())) {
  //     console.log('Could not dismiss the topmost loader. Aborting...');
  //   }
  //   topLoader = await this.loadingController.getTop();
  // }
  // }

  async showNormalLoader() {
    this.isLoading = true;
    return await this.loadingController.create({
       message:'Please wait ...',
       mode:'ios',
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }


  async showImgLoader() {
    this.isLoading = true;
    return await this.loadingController.create({
      mode:'ios',
      message: '<div class="text-center"><img src="../assets/images/icons/book.gif"   /> <br />Please wait..</div>',
      spinner: null, 
    
      cssClass:'custom-loader-class',
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }
}
