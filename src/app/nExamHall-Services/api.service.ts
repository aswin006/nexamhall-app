import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders,HttpErrorResponse} from "@angular/common/http"
import {Observable,throwError} from "rxjs";
import { environment } from '../../environments/environment';
import {Storage} from '@ionic/storage'
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiUrl = environment.apiUrl;
  userdetails:any;
  httpOptions:any;
  headers:any;
  constructor(
    public http:HttpClient,
    public storage : Storage,
  ) { }

  handleError(error : HttpErrorResponse){
    if(error.error instanceof ErrorEvent){
      console.log("An error occured", error.error.message)
    }else{
      console.log(`Backend returned code ${error.status},` + `body was : ${error.error}`)
    }
    return throwError("something bad happend ;please try again later")
  };

  loginauth(loginData){
    return this.http.post(this.apiUrl + 'login',loginData)
  }

  checkCredentials(data) {
  return this.http.post(this.apiUrl+ "checkcredentials",data);
  }
  sendAuthOTP(otpdata){
    return this.http.post(this.apiUrl + 'sendOtp',otpdata)
  }

  getDashBoardDetails(loginId) {
    return this.http.post(this.apiUrl + "dashboardanlytics", {
      loginId: loginId,
    });
  };


  userEnrolled_exams(loginId) {
    return this.http.post(this.apiUrl + "userenroled", {
      loginId: loginId,
    });
  };

  getTestmaps(loginid, instituteid, examid) {
    //debugger;
    return this.http.post(this.apiUrl + "usermockexams", {
      loginId: loginid,
      instituteuserid: instituteid,
      examid: examid,
    });
  };

  uservalidityCheck(userValidityCheckData) {
    return this.http.post(
      this.apiUrl + "uservalidity",
      userValidityCheckData
    );
  };

  startTest(getExamdata) {
    return this.http.get("assets/data/question.json");
    // return this.http.post(this.base_path + "startexam", getExamdata);
  }
  
}
