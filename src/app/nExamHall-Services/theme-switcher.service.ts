import { Inject, Injectable } from '@angular/core';
import { DOCUMENT} from '@angular/common';
import {DomController} from '@ionic/angular';
import {LocalStorageService} from '../nExamHall-Services/local-storage.service'
import { CanLoad} from '@angular/router';

interface ThemeMode{
  name:string;
  styles:ThemeModeStyle[];
}
interface ThemeModeStyle{
  themeModeVariable : string;
  value: string;
}
@Injectable({
  providedIn: 'root'
})
export class ThemeSwitcherService implements CanLoad {
private themeModes : ThemeMode[] = [];
  private currentMode : number = 0;
  theme = '';
  themes = [];
  currentTheme: any;
  constructor(
    private domCtrl : DomController,
     @Inject(DOCUMENT) private document,
     private localStorage : LocalStorageService 
  ) { 
    this.themes= [
      {
        id:1,
        name:'Yellow',
        color:'yellow',
        icon:'color-palette',
        cssclass:'theme-yellow',
        colorHex:'#FFB64D',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"243, 205, 116",
          ionCcolorPrimaryContrast:"#000000",
          ionCcolorPrimaryContrastrgb:" 0,0,0",
          ionCcolorPrimaryShade:"#d8aa41",
          ionCcolorPrimaryTint:"#f7c75c",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      },
      {
        id:2,
        name:'Rich Green',
        color:'richgreen',
        icon:'color-palette-outline',
        cssclass:'theme-richgreen',
        colorHex:'#18B6B7',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"80,220,221",
          ionCcolorPrimaryContrast:"#000000",
          ionCcolorPrimaryContrastrgb:" 0,0,0",
          ionCcolorPrimaryShade:"#15a0a1",
          ionCcolorPrimaryTint:"#2fbdbe",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      },
      {
        id:3,
        name:'Pink',
        color:'dark',
        icon:'color-palette',
        cssclass:'theme-pink',
        colorHex:'#C886D0',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"243, 181, 211",
          ionCcolorPrimaryContrast:"#000000",
          ionCcolorPrimaryContrastrgb:" 0,0,0",
          ionCcolorPrimaryShade:"#b076b7",
          ionCcolorPrimaryTint:"#ce92d5",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      },
      {
        id:4,
        name:'Blue',
        color:'blue',
        icon:'color-palette',
        cssclass:'theme-blue',
        colorHex:'#344BDC',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"5,65,147",
          ionCcolorPrimaryContrast:"#ffffff",
          ionCcolorPrimaryContrastrgb:"255,255,255",
          ionCcolorPrimaryShade:"#043981",
          ionCcolorPrimaryTint:"#1e549e",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      },
      {
        id:5,
        name:'Orange',
        color:'orange',
        icon:'color-palette',
        cssclass:'theme-orange',
        colorHex:'#ff6269',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"255,123,84",
          ionCcolorPrimaryContrast:"#000000",
          ionCcolorPrimaryContrastrgb:" 0,0,0",
          ionCcolorPrimaryShade:"#e0565c",
          ionCcolorPrimaryTint:"#ff7278",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      },
      {
        id:6,
        name:'Green',
        color:'green',
        icon:'color-palette',
        cssclass:'theme-green',
        colorHex:'#85E5A8',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"191,239,177",
          ionCcolorPrimaryContrast:"#000000",
          ionCcolorPrimaryContrastrgb:" 0,0,0",
          ionCcolorPrimaryShade:"#75ca94",
          ionCcolorPrimaryTint:"#91e8b1",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      },
      {
        id:7,
        name:'Purple',
        color:'purple',
        icon:'color-palette',
        cssclass:'theme-purple',
        colorHex:'#2E306E',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"76,75,155",
          ionCcolorPrimaryContrast:"#ffffff",
          ionCcolorPrimaryContrastrgb:"255,255,255",
          ionCcolorPrimaryShade:"#282a61",
          ionCcolorPrimaryTint:"#43457d",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      },
      {
        id:8,
        name:'Seablue',
        color:'seablue',
        icon:'color-palette',
        cssclass:'theme-seablue',
        colorHex:'#4099ff',
        themeStyles:{
          ionColorPrimary:"#1D2237",
          ionColorPrimaryrgb:"64,153,255",
          ionCcolorPrimaryContrast:"#000000",
          ionCcolorPrimaryContrastrgb:"0,0,0",
          ionCcolorPrimaryShade:"#3887e0",
          ionCcolorPrimaryTint:"#53a3ff",
          ionItemIosCackgroundColor:"",
          ionItemMdBackgroundColor:"",
          ionTabbarBackgroundColor:"",
          ionTabbarIosTextColorActive:"",
          ionTabbarMdTextColorActive:"",
          ionBackgroundColor:""
        }
      }
     
    ]
    
    this.themeModes = [
      {
        
        name: 'day',
        styles: [
        
          // { themeModeVariable: '--ion-color-primary', value: '#D5D7DA'},
          { themeModeVariable: '--ion-color-primary-rgb', value: '48,46,68'},
          { themeModeVariable: '--ion-color-primary-contrast', value: '#ffffff'},
          { themeModeVariable: '--ion-color-primary-contrast-rgb', value: '255,255,255'},
          { themeModeVariable: '--ion-color-primary-shade', value: '#1e2023'},
          { themeModeVariable: '--ion-color-primary-tint', value: '#383a3e'},
          { themeModeVariable: '--ion-item-ios-background-color', value: '#717171'},
          { themeModeVariable: '--ion-item-md-background-color', value: '#717171'},
          { themeModeVariable: '--ion-tabbar-background-color', value: '#302E44'},
          { themeModeVariable: '--ion-tabbar-ios-text-color-active', value: '#ffffff'},
          { themeModeVariable: '--ion-tabbar-md-text-color-active', value: '#ffffff'},
          { themeModeVariable: '--ion-background-color', value: '#f1f4f6'}
        ]
      },
      {
        name: 'night',
        styles: [
          // { themeModeVariable: '--ion-color-primary', value: '#302E44'},
          { themeModeVariable: '--ion-color-primary-rgb', value: '48,46,68'},
          { themeModeVariable: '--ion-color-primary-contrast', value: '#ffffff'},
          { themeModeVariable: '--ion-color-primary-contrast-rgb', value: '255,255,255'},
          { themeModeVariable: '--ion-color-primary-shade', value: '#1e2023'},
          { themeModeVariable: '--ion-color-primary-tint', value: '#383a3e'},
          { themeModeVariable: '--ion-item-ios-background-color', value: '#717171'},
          { themeModeVariable: '--ion-item-md-background-color', value: '#717171'},
          { themeModeVariable: '--ion-tabbar-background-color', value: '#302E44'},
          { themeModeVariable: '--ion-tabbar-ios-text-color-active', value: '#ffffff'},
          { themeModeVariable: '--ion-tabbar-md-text-color-active', value: '#ffffff'},
          // { themeModeVariable: '--ion-background-color', value: '#383838'}
          { themeModeVariable: '--ion-background-color', value: '#181937'}
          
        ]
      }
    ];

    
  };

   getThemes(){
    return this.themes;
  }
  
  canLoad() {
    return this.localStorage.get('theme').then(res => {
      if (res) {
       // this.router.navigate(['/app', 'tabs', 'schedule']);
        //  this.router.navigate(['/nexamhallcore/auth',]);
        this.localStorage.set('theme', res);
        this.setThemeProperty(res)
        // return true;
      } else {
        this.theme = 'theme-yellow';
        this.localStorage.set('theme', this.theme);
        this.setThemeProperty(this.theme)
       
      }
      return true;
    });
  }

 async intitialTheme(){
       debugger;
        this.theme = await this.localStorage.get('theme');
        return this.theme;
  }
  async setTheme(){
    this.localStorage.get('theme').then((res) =>{
      if(res == null){
        this.theme = 'theme-richgreen';
        this.localStorage.set('theme', this.theme);
        
      }else{
        this.theme = res;
      }
    })
    
     
  }
  
  setMode(name):void{

    this.localStorage.get('theme').then((res)=>{
      this.currentTheme = res;
      let mode = this.themeModes.find(x => x.name === name);
      let currentThemecolor = this.themes.find(x => x.cssclass === this.currentTheme);
      this.domCtrl.write(() =>{
        mode.styles.forEach(style =>{
          if(style.themeModeVariable == '--ion-color-primary-rgb'){
                
                 style.value = this.hexToRgbA(currentThemecolor['colorHex'])
          }
          document.documentElement.style.setProperty(style.themeModeVariable,style.value)
        });
      });
   })
   
  }
  

   hex2rgba(hex, alpha = 1){
    const [r, g, b] = hex.match(/\w\w/g).map(x => parseInt(x, 16));
    // return `rgba(${r},${g},${b},${alpha})`;
    return `${r},${g},${b}`;
  };

  setThemeProperty(currentTheme){
    debugger;
    let currentThemecolor = this.themes.find(x => x.cssclass === currentTheme);
    this.domCtrl.write(() =>{
       //  var rgba_color = this.hex2rgba(currentThemecolor['colorHex'],0.5);
       var rgba_color = currentThemecolor.themeStyles.ionColorPrimaryrgb;
         var primarycolor = currentThemecolor.colorHex;
         document.documentElement.style.setProperty('--ion-color-primary-rgb',rgba_color)
        
         document.documentElement.style.setProperty('--ion-color-primary',primarycolor);
         
    });
  }
  hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+')';
    }
    console.log('error hex')
}


}
