import { Component, OnInit } from '@angular/core';
import { Config, ModalController, NavParams } from "@ionic/angular";

@Component({
  selector: 'app-mockfilter-modal',
  templateUrl: './mockfilter-modal.component.html',
  styleUrls: ['./mockfilter-modal.component.scss'],
})
export class MockfilterModalComponent implements OnInit {
  filerLists: any = [];
  filerList: any = [];
  constructor(
    public config: Config,
    private modalCtrl: ModalController,
    public navParams: NavParams
  ) {
    this.filerLists = [
      {
        name: "Start Test",
        value: "0",
        isChecked: false,
      },
      {
        name: "Resume Test",
        value: "1",
        isChecked: false,
      },
      {
        name: "Retake Test",
        value: "2",
        isChecked: false,
      },
    ];
   }

  ngOnInit() {}

  ionViewWillEnter() {
    debugger;
    const excludedFilter = this.navParams.get("excludedFilter");
    this.filerLists.forEach((item) => {
      this.filerList.push({
        name: item.name,
        value: item.value,
        isChecked: excludedFilter.indexOf(item.value) === -1,
      });
    });
    console.log(this.filerList);
  }

  selectAll(check: boolean) {
    this.filerList.forEach((item) => {
      item.isChecked = check;
    });
  }

  applyFilters() {
    const excludedFilter = this.filerList
      .filter((c) => !c.isChecked)
      .map((c) => c.value);
    console.log(excludedFilter);
    this.dismiss(excludedFilter);
  }

  dismiss(data?: any) {
    this.modalCtrl.dismiss(data);
  }

  

}
