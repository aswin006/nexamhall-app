import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { OtpPinScreenComponent } from './otp-pin-screen/otp-pin-screen.component';

@NgModule({
	declarations: [OtpPinScreenComponent],
	imports: [IonicModule],
	exports: [OtpPinScreenComponent]
})
export class nExamHallcustomComponentsModule {}