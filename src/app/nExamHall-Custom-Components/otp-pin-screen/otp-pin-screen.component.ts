import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: 'app-otp-pin-screen',
  templateUrl: './otp-pin-screen.component.html',
  styleUrls: ['./otp-pin-screen.component.scss'],
})
export class OtpPinScreenComponent  {

  @Input() pagetitle:string = "Enter OTP";
  pin:string="";
@Output() change: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  emitEvent(){
    this.change.emit(this.pin);
  }

  handleInput(pin : string){
    if(pin === "clear"){
      this.pin = "";
      return
    }


    if(this.pin.length === 4){
      return;
    }
    this.pin += pin;
  }

  ngOnInit() {}

}
