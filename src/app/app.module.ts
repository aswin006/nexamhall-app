import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage';
import {InterceptorService} from "./nExamHall-Providers/interceptor.service";

import { HttpClientModule ,HTTP_INTERCEPTORS} from '@angular/common/http';
import { ThemeSwitcherService } from './nExamHall-Services/theme-switcher.service';
import {nExamHallcustomComponentsModule} from './nExamHall-Custom-Components/nExamhall-custom-component.module';
import {NExamHallCandidateCoreModulePageModule} from './nExamHall-Candidate-Module/n-exam-hall-candidate-core-module/n-exam-hall-candidate-core-module.module'
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
     BrowserModule, 
     IonicModule.forRoot(),
     AppRoutingModule,
     IonicStorageModule.forRoot(),
     HttpClientModule,
     NExamHallCandidateCoreModulePageModule,
     nExamHallcustomComponentsModule
    ],
  providers: [
    StatusBar,
    SplashScreen,
    ThemeSwitcherService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { 
      provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true 
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
